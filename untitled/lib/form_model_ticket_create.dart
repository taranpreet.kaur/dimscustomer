
class FormModelTicketCreation {
  String assetId;
  String userName;
  String email;
  String customerName;
  String mobileNo;
  String assetType;
  String callMode;
  String callType;
  String address;
  String area;
  String city;
  String state;
  String pincode;
  String country;
  String issueDescription;
  String issueCategory;
  String issueSubCategory;
  String typeOfRequest;
  String department;
  String date;
  String time;


  FormModelTicketCreation({this.userName,this.assetId, this.email,this.customerName,this.mobileNo,this.assetType,this.callMode,this.callType,this.address,this.area,this.city,this.state,
  this.pincode,this.country,this.issueDescription,this.issueCategory,this.issueSubCategory,this.typeOfRequest,this.department,this.date,this.time});
}
