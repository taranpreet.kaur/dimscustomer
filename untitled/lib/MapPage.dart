// import 'dart:convert';
//
// import 'package:flutter/foundation.dart';
// import 'package:flutter_app_tabs/Details1.dart';
// import 'package:flutter_polyline_points/flutter_polyline_points.dart';
// import 'package:google_maps_flutter/google_maps_flutter.dart';
// import 'dart:async';
// import 'package:flutter/material.dart';
// import 'package:socket_io_client/socket_io_client.dart';
// import 'package:http/http.dart' as http;
//
//
//
// class MapPage extends StatefulWidget {
//   String id;
//   var ticket_lat,ticket_lng,eng_lat,eng_lng;
//   MapPage(this.id,this.ticket_lat,this.ticket_lng,this.eng_lat,this.eng_lng);
//   @override
//   MapPageState createState() => MapPageState(id,ticket_lat,ticket_lng,eng_lat,eng_lng);
// }
//
// class MapPageState extends State<MapPage> {
//   String id;
//   var ticket_lat,ticket_lng,eng_lat,eng_lng;
//   MapPageState(this.id,this.ticket_lat,this.ticket_lng,this.eng_lat,this.eng_lng);
//   double CAMERA_ZOOM = 13;
//    double CAMERA_TILT = 0;
//   double CAMERA_BEARING = 30;
//   var eta_controller = TextEditingController();
//   var timeTaken;
//
//   Completer<GoogleMapController> _controller = Completer();
//   // this set will hold my markers
//   Set<Marker> _markers = {};
//   // this will hold the generated polylines
//   Set<Polyline> _polylines = {};
//   // this will hold each polyline coordinate as Lat and Lng pairs
//   List<LatLng> polylineCoordinates = [];
//   // this is the key object - the PolylinePoints
//   // which generates every polyline between start and finish
//   PolylinePoints polylinePoints = PolylinePoints();
//   String googleAPIKey = "AIzaSyAWgUYmMPpD3mcpEBjz2FOB94PNqKGc-N0";
//   // for my custom icons
//   BitmapDescriptor sourceIcon;
//   BitmapDescriptor destinationIcon;
//   setPolylines() async {
//
//     List<PointLatLng> result = await polylinePoints?.getRouteBetweenCoordinates(
//         googleAPIKey,
//         ticket_lat,
//        ticket_lng,
//         eng_lat,
//         eng_lng);
//     if (result.isNotEmpty) {
//       // loop through all PointLatLng points and convert them
//       // to a list of LatLng, required by the Polyline
//       result.forEach((PointLatLng point) {
//         polylineCoordinates.add(LatLng(point.latitude, point.longitude));
//       });
//     }
//
//     setState(() {
//       // create a Polyline instance
//       // with an id, an RGB color and the list of LatLng pairs
//       Polyline polyline = Polyline(
//           polylineId: PolylineId("poly"),
//           color: Color.fromARGB(255, 40, 122, 198),
//           points: polylineCoordinates);
//
//       // add the constructed polyline as a set of points
//       // to the polyline set, which will eventually
//       // end up showing up on the map
//       _polylines.add(polyline);
//     });
//   }
//   @override
//   void initState() {
//     super.initState();
//     setSourceAndDestinationIcons();
//     socket();
//     getTime(eng_lat.toString(), eng_lng.toString(), ticket_lat.toString(), ticket_lng.toString());
//   }
//
//   void setSourceAndDestinationIcons() async {
//     sourceIcon = await BitmapDescriptor.fromAssetImage(
//         ImageConfiguration(devicePixelRatio: 2.5), 'assets/delivery1.png');
//     destinationIcon = await BitmapDescriptor.fromAssetImage(
//         ImageConfiguration(devicePixelRatio: 2.5),
//         'assets/success.png');
//   }
//   LatLng SOURCE_LOCATION;
//   LatLng DEST_LOCATION;
//
//   @override
//   Widget build(BuildContext context) {
//     SOURCE_LOCATION = LatLng(this.ticket_lat, ticket_lng);
//     CameraPosition initialLocation = CameraPosition(
//         zoom: CAMERA_ZOOM,
//         bearing: CAMERA_BEARING,
//         tilt: CAMERA_TILT,
//         target: SOURCE_LOCATION);
//
//
//      return MaterialApp(
//          home: Scaffold(
//            appBar: AppBar(title: Text("Tracking"),  flexibleSpace: Image(
//              image: AssetImage('assets/appbar_background.png'),
//              fit: BoxFit.cover,
//            ),
//              backgroundColor: Colors.transparent,),
//            body: Stack(
//                children: <Widget>[
//            Container(margin: EdgeInsets.fromLTRB(25, 30, 20, 30),
//            child: Text("Estimated Time of Arrival"),),
//          Container(
//            padding: EdgeInsets.all(20),
//            margin: EdgeInsets.fromLTRB(10, 20, 10, 5),
//            child: TextField(
//              controller: eta_controller??"Eta",
//              textAlign: TextAlign.left,
//              style: TextStyle(fontWeight: FontWeight.bold),
//              readOnly: true,
//
//            ),
//
//          ),
//
//
//        Container(margin: EdgeInsets.fromLTRB(10, 150, 10, 10),
//                child:
//      GoogleMap(
//         myLocationEnabled: true,
//         compassEnabled: true,
//         tiltGesturesEnabled: false,
//         markers: _markers,
//         polylines: _polylines,
//         mapType: MapType.normal,
//         initialCameraPosition: initialLocation,
//         onMapCreated: onMapCreated))
//     ] ),),);
//   }
//
//     void onMapCreated(GoogleMapController controller) {
//     controller.setMapStyle(Utils.mapStyles);
//     _controller.complete(controller);
//     setMapPins();
//     setPolylines();
//   }
//
//   void setMapPins() {
//     SOURCE_LOCATION = LatLng(this.ticket_lat, this.ticket_lng);
//     DEST_LOCATION=LatLng(this.eng_lat,this.eng_lng);
//
//     setState(() {
//       // source pin
//       _markers.add(Marker(
//           markerId: MarkerId('sourcePin'),
//           position: SOURCE_LOCATION,
//           icon: sourceIcon));
//       // destination pin
//       _markers.add(Marker(
//           markerId: MarkerId('destPin'),
//           position: DEST_LOCATION,
//           icon: destinationIcon));
//     });
//   }
//   @override
//   void debugFillProperties(DiagnosticPropertiesBuilder properties) {
//     super.debugFillProperties(properties);
//     properties.add(DoubleProperty('ticket_lat', ticket_lat));
//   }
//   double latitude,longitude;
//
//   socket() {
//     // 10.11.4.59:8080
//     Socket socket1 = io ('http://10.11.4.59:8080', <String, dynamic>{
//       'transports': ['websocket'],
//       'autoConnect': true,
//       // 'extraHeaders': {'foo': 'bar'} // optional
//     });
//     socket1.connect();
//     print("==hello server");
//
//     socket1.on(id, (data) {
//
//       print("socket"+data);
//       // compute(  latitude=data['long'],longitude=data['lat']);
//       var hjhj=jsonDecode(data);
//       latitude=hjhj['lat'];
//       longitude=hjhj['lng'];
//       if(latitude!=null && longitude!=null){
//         updateMarker (latitude, longitude);
//       }
//     });
//     getTime(latitude.toString(), longitude.toString(), ticket_lat.toString(), ticket_lng.toString());
//
//     //   socket1.on("tracker", (data)  {
//     //   print("==socket data "+data);
//     //
//     //   print("==bye server");
//     //   // await compute(latitude = data["lat"],
//     //   //     longitude = data["long"]);
//     //   latitude = data["lat"];
//     //   longitude = data["long"];
//     //    print("latitide"+longitude.toString()+"longitude"+latitude.toString());
//     // if(latitude!=null && longitude!=null){
//     //   updateMarker (latitude, longitude);
//     // }
//     // longitude=null;
//     // latitude=null;
//     //
//     // });
//   }
//
//
//
//   updateMarker(lat1, lng1) async {
//     print("==udate marker"+lat1.toString()+lng1.toString());
//     var pinPosition = LatLng(lat1, lng1);
//
//     final GoogleMapController controller = await _controller.future;
//     controller.animateCamera(CameraUpdate.newCameraPosition(CameraPosition(target: pinPosition, zoom: 16)),);
//     if (this.mounted) {
//       setState(() {
//         // compute( _addMarker(LatLng(lat,lng), "sourcePin",BitmapDescriptor.fromAsset('assets/delivery.png')),null);
//         // _addMarker(LatLng(lat1,lng1), "sourcePin",
//         //   BitmapDescriptor.fromAsset('assets/delivery1.png',),);
//
//
//         _markers.add(Marker(
//             markerId: MarkerId('sourcePin'),
//             position: LatLng(lat1, lng1),
//             icon: sourceIcon));
//
//
//         // getTime(lat1.toString(),lng1.toString(),latitude.toString(),longitude.toString());
//         // getTime(ticket_lat.toString(),ticket_lng.toString(),lat1.toString(),lng1.toString());
//
//       });
//     }
//     return;
//
//
//
//     // print("lat lng"+lat1.toString()+lng1.toString()+lat.toString()+lng.toString());
//     // double distanceInMeters = await Geolocator().distanceBetween(
//     //     lat1, lng1, lat, lng);
//     // print("distance"+distanceInMeters.toString());
//   }
//
//
//
//
//
//
//
//   void getTime(String cuttentlat, currentlog, destlat, destlong) async {
//     print("called"+cuttentlat+currentlog+destlat+destlong);
//     var urlString = "https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=" +cuttentlat + "," + currentlog + "&destinations=" +destlat+","+destlong + "&key=AIzaSyAWgUYmMPpD3mcpEBjz2FOB94PNqKGc-N0";
//     print("currenturl" + urlString);
//     // 'https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=29.4727,77.7085&destinations=28.9845,77.7064&key=AIzaSyAWgUYmMPpD3mcpEBjz2FOB94PNqKGc-N0'
//     final response =
//     await http.get(Uri.parse(urlString));
//     if (response.statusCode == 200) {
//       // var result = await http.Response.fromStream(response);
//       var jsonResponse = json.decode(response.body);
//       if (jsonResponse['rows'][0]['elements'][0]['status'] == "ZERO_RESULTS"){
//
//
//         setState(() {
//           timeTaken = "Undefined Time";
//         });
//       }else{
//         var respo = jsonResponse['rows'][0]['elements'][0]['duration']['text'];
//         print("respo " + respo);
//
//         setState(() {
//           timeTaken = respo;
//           eta_controller.text=timeTaken;
//         });
//
//       }
//
//       print("==result get APi" + response.body.toString());
//
//     } else {
//       print("==response error" + response.toString());
//     }
//     // if (response.statusCode == 200) {
//     //   // If the server did return a 200 OK response,
//     //   // then parse the JSON.
//     //   return Album.fromJson(jsonDecode(response.body));
//     // } else {
//     //   // If the server did not return a 200 OK response,
//     //   // then throw an exception.
//     //   throw Exception('Failed to load album');
//     // }
//     // var request = http.get(Uri.parse("https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=29.4727,77.7085&destinations=28.9845,77.7064&key=AIzaSyAWgUYmMPpD3mcpEBjz2FOB94PNqKGc-N0"));
//     //       print("request" + request.toString());
//
//     //
//     // var response = await request.send();
//
//
//   }
//
//
// }
//
// class Utils {
//   static String mapStyles = '''[
//   {
//     "elementType": "geometry",
//     "stylers": [
//       {
//         "color": "#f5f5f5"
//       }
//     ]
//   },
//   {
//     "elementType": "labels.icon",
//     "stylers": [
//       {
//         "visibility": "off"
//       }
//     ]
//   },
//   {
//     "elementType": "labels.text.fill",
//     "stylers": [
//       {
//         "color": "#616161"
//       }
//     ]
//   },
//   {
//     "elementType": "labels.text.stroke",
//     "stylers": [
//       {
//         "color": "#f5f5f5"
//       }
//     ]
//   },
//   {
//     "featureType": "administrative.land_parcel",
//     "elementType": "labels.text.fill",
//     "stylers": [
//       {
//         "color": "#bdbdbd"
//       }
//     ]
//   },
//   {
//     "featureType": "poi",
//     "elementType": "geometry",
//     "stylers": [
//       {
//         "color": "#eeeeee"
//       }
//     ]
//   },
//   {
//     "featureType": "poi",
//     "elementType": "labels.text.fill",
//     "stylers": [
//       {
//         "color": "#757575"
//       }
//     ]
//   },
//   {
//     "featureType": "poi.park",
//     "elementType": "geometry",
//     "stylers": [
//       {
//         "color": "#e5e5e5"
//       }
//     ]
//   },
//   {
//     "featureType": "poi.park",
//     "elementType": "labels.text.fill",
//     "stylers": [
//       {
//         "color": "#9e9e9e"
//       }
//     ]
//   },
//   {
//     "featureType": "road",
//     "elementType": "geometry",
//     "stylers": [
//       {
//         "color": "#ffffff"
//       }
//     ]
//   },
//   {
//     "featureType": "road.arterial",
//     "elementType": "labels.text.fill",
//     "stylers": [
//       {
//         "color": "#757575"
//       }
//     ]
//   },
//   {
//     "featureType": "road.highway",
//     "elementType": "geometry",
//     "stylers": [
//       {
//         "color": "#dadada"
//       }
//     ]
//   },
//   {
//     "featureType": "road.highway",
//     "elementType": "labels.text.fill",
//     "stylers": [
//       {
//         "color": "#616161"
//       }
//     ]
//   },
//   {
//     "featureType": "road.local",
//     "elementType": "labels.text.fill",
//     "stylers": [
//       {
//         "color": "#9e9e9e"
//       }
//     ]
//   },
//   {
//     "featureType": "transit.line",
//     "elementType": "geometry",
//     "stylers": [
//       {
//         "color": "#e5e5e5"
//       }
//     ]
//   },
//   {
//     "featureType": "transit.station",
//     "elementType": "geometry",
//     "stylers": [
//       {
//         "color": "#eeeeee"
//       }
//     ]
//   },
//   {
//     "featureType": "water",
//     "elementType": "geometry",
//     "stylers": [
//       {
//         "color": "#c9c9c9"
//       }
//     ]
//   },
//   {
//     "featureType": "water",
//     "elementType": "labels.text.fill",
//     "stylers": [
//       {
//         "color": "#9e9e9e"
//       }
//     ]
//   }
// ]''';
// }
