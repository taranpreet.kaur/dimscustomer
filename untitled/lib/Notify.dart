class Notify {

  String title;
  String body;
  String ticket_id;
  String date;


  Notify(
      {
        this.title,
        this.body,
        this.ticket_id,
        this.date
      });

  factory Notify.fromJson(Map<String, dynamic> json) {
    return Notify(

      title: json["title"],
      body: json["body"],
      ticket_id:json["ticket_id"],
      date: json["date"]
    );
  }

  static Map<String, dynamic> toMap(Notify music) => {
    'title': music.title,
    'body': music.body,
    'ticket_id':music.ticket_id,
    'date':music.date

  };


}


