import 'TicketModel.dart';

class TicketResponse{
  TicketModel notification_details;

  TicketResponse(
      {
        this.notification_details

      });

  factory TicketResponse. fromJson(Map<String, dynamic> json) {
    return TicketResponse(notification_details : TicketModel.fromJson(json));
  }

}

