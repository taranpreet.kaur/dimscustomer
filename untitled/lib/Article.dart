
class Article {
  String assetType;
  String assetId;
  String id;
  String status;
  String ticketSerialNo;
  String ticketDate;
  String lastRemark;
  String isPartRequested;


  Article({
    this.assetType,
    this.assetId,
    this.id,
    this.status,
    this.ticketSerialNo,
    this.ticketDate,
    this.lastRemark,
    this.isPartRequested

  });

  factory Article.fromJson(Map<String, dynamic> json) {
    return Article(
      assetType: json["assetType"] as String,
      assetId: json["assetId"] as String,
      id: json["_id"] as String,
      status: json["status"] as String,
      ticketSerialNo: json["ticketSerialNo"] as String,
      ticketDate: json["ticketDate"] as String,
      lastRemark: json["lastRemark"] as String,
       isPartRequested: json["isPartRequested"].toString() as String

    );
  }

  static Map<String, dynamic> toMap(Article music) =>
      {
        'assetType': music.assetType,
        'assetId': music.assetId,
        '_id': music.id,
        'status': music.status,
        'ticketSerialNo':music.ticketSerialNo,
        'ticketDate':music.ticketDate,
        'lastRemark':music.lastRemark,
        'isPartRequested':music.isPartRequested

      };
}
