import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'Deatils2.dart';
import 'Details1.dart';

class TicketDetails extends StatefulWidget {
 String id;
 TicketDetails(this.id);
 @override
 TicketDetailState createState() => TicketDetailState(id);
}
class TicketDetailState extends State<TicketDetails> {
  String id;
  TicketDetailState(this.id);
  @override
  Widget build(BuildContext context) {
    Color color1 = _colorFromHex("#00ABC5");
   print("Ticket id is: "+id);

    return MaterialApp(
        home: DefaultTabController(length: 2,child: Scaffold(appBar: AppBar(title: Text("Ticket Detail"),

          flexibleSpace: Image(
            image: AssetImage('assets/appbar_background.png'),
            fit: BoxFit.cover,
          ),

          bottom:TabBar(indicatorColor:Colors.white,tabs: [Tab(text: 'Status'),Tab(text: 'OtherDetail',)],),),
          body:
          TabBarView(children: [Details1(id),Deatils2()],),))
    );
  }
}
  Color _colorFromHex(String hexColor) {
    final hexCode = hexColor.replaceAll('#', '');
    return Color(int.parse('FF$hexCode', radix: 16));
  }

