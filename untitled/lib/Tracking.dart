import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:customer/Constant.dart';
import 'package:http/http.dart' as http;
import 'package:location/location.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_animarker/lat_lng_interpolation.dart';
import 'package:flutter_animarker/models/lat_lng_delta.dart';
import 'package:customer/pin_pill_info.dart';
import 'package:flutter_polyline_points/flutter_polyline_points.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location/location.dart';
import 'package:socket_io_client/socket_io_client.dart';
import 'SearchListViewExample.dart';
const double CAMERA_ZOOM = 16;
const double CAMERA_TILT = 80;
const double CAMERA_BEARING = 30;
PinInformation sourcePinInfo;
LatLngInterpolationStream _latLngStream = LatLngInterpolationStream();
StreamSubscription<LatLngDelta> subscription;
var _markerIdCounter=0;
PinInformation currentlySelectedPin = PinInformation(
    pinPath: '',
    avatarPath: '',
    location: LatLng(0, 0),
    locationName: '',
    labelColor: Colors.grey);


class Tracking extends StatefulWidget {
  String id;
  var ticket_lat,ticket_lng,eng_lat,eng_lng;
  Tracking(this.id,this.ticket_lat,this.ticket_lng,this.eng_lat,this.eng_lng):super();
  @override
  TrackingState createState() => TrackingState(id,ticket_lat,ticket_lng,eng_lat,eng_lng);
}
class TrackingState extends State<Tracking> {
  String id;
  var ticket_lat,ticket_lng,eng_lat,eng_lng;
  TrackingState(this.id,this.ticket_lat,this.ticket_lng,this.eng_lat,this.eng_lng);
  GoogleMapController myController;

  // double _destLatitude;
  // // = 28.6623,
  //     double _destLongitude ;
  // // = 77.1411;
  Map<MarkerId, Marker> markers = {};
  Map<PolylineId, Polyline> polylines = {};
  List<LatLng> polylineCoordinates = [];
  PolylinePoints polylinePoints = PolylinePoints();
  String apiKey = "AIzaSyBqU4wR7dFqFKT-QTCjAXVEGqvYUCu4jEQ";
  Completer<GoogleMapController> _controller = Completer();
  Completer<GoogleMapController> _mapController = Completer();
  // String testapiKey="AIzaSyCe-3C7Z-NzNoEIRzDqQXBUgEmomVArowQ";
  Set<Marker> _markers = Set<Marker>();
  LatLng SOURCE_LOCATION = LatLng(28.57061, 77.18792);
  double pinPillPosition = -100;
  var eta_controller = TextEditingController();
  var dis_km=TextEditingController();
  var timeTaken;
     double distanceKm;
  final LatLng _center = const LatLng(28.5679, 77.1881);
  Socket socket1;
  var _originLatitude, _originLongitude,dest_latitude,dest_longitude;

  void _onMapCreated(GoogleMapController controller) {
    _controller.complete(controller);
    // if ([SOURCE_LOCATION] != null) {
    MarkerId markerId = MarkerId(_markerIdVal());
    // LatLng position = [SOURCE_LOCATION] as LatLng;
    Marker marker = Marker(
      markerId: markerId,
      position: LatLng(ticket_lat,ticket_lng),
      draggable: false,
    );
    setState(() {
      markers[markerId] = marker;
    });

    Future.delayed(Duration(seconds: 1), () async {
      GoogleMapController controller = await _controller.future;
      controller.animateCamera(
        CameraUpdate.newCameraPosition(
          CameraPosition(
            target: LatLng(ticket_lat,ticket_lng),

            zoom: 12.0,
          ),
        ),
      );
    });
  }



  @override
  Widget build(BuildContext context) {
    CameraPosition initialCameraPosition = CameraPosition(
      zoom: CAMERA_ZOOM,
      tilt: CAMERA_TILT,
      bearing: CAMERA_BEARING,

      target: LatLng(eng_lat, eng_lng),);
    return WillPopScope(
    onWillPop: _onBackPressed,
    child:Scaffold(
        appBar: AppBar(
        title: Text("Tracking"),

    flexibleSpace: Image(
    image: AssetImage('assets/appbar_background.png'),
    fit: BoxFit.cover,
    ),
   ),
      body:
      Stack(
        children: <Widget>[
          Container(margin: EdgeInsets.fromLTRB(25, 30, 20, 30),
            child: Text("Estimated Time of Arrival"),),
          Container(
            padding: EdgeInsets.all(20),
            margin: EdgeInsets.fromLTRB(10, 20, 10, 5),
            child: TextField(
              controller: eta_controller??"Eta",
              textAlign: TextAlign.left,
              style: TextStyle(fontWeight: FontWeight.bold),
              readOnly: true,

            ),

          ),
          Container(margin: EdgeInsets.fromLTRB(25, 100, 20, 30),
            child: Text("Distance Km"),),
          Container(
            padding: EdgeInsets.all(20),
            margin: EdgeInsets.fromLTRB(10, 100, 10, 5),
            child: TextField(
              controller: dis_km??"Distance Km",
              textAlign: TextAlign.left,
              style: TextStyle(fontWeight: FontWeight.bold),
              readOnly: true,

            ),

          ),


          Container(margin: EdgeInsets.fromLTRB(10, 200, 10, 10),
            child: GoogleMap(
              myLocationEnabled: true,
              compassEnabled: true,
              tiltGesturesEnabled: false,
              markers: Set<Marker>.of(markers.values),
              polylines:Set<Polyline>.of(polylines.values),
              mapType: MapType.normal,
              onMapCreated: _onMapCreated,
              initialCameraPosition: initialCameraPosition,
              onTap: (LatLng loc) {
              },
            ),


          ),



          // Padding(
          //   padding: const EdgeInsets.all(14.0),
          //   child: Align(
          //     alignment: Alignment.topRight,
          //     child: FloatingActionButton(
          //       onPressed: () => print('You have pressed the button'),
          //       materialTapTargetSize: MaterialTapTargetSize.padded,
          //       backgroundColor: color1,
          //       child: const Icon(Icons.map, size: 30.0),
          //     ),
          //   ),
          // ),
        ],
      ),
   ),);
  }
  String _markerIdVal({bool increment = false}) {
    String val = 'marker_id_$_markerIdCounter';
    if (increment) _markerIdCounter++;
    return val;
  }

  Future<bool> _onBackPressed() {
    print("===trackkkkkkkkk");
   // socket1.off(id);
   // socket1.dispose();
   // socket1.disconnect();
    Navigator.pushAndRemoveUntil<dynamic>(context, MaterialPageRoute<dynamic>(builder: (BuildContext context) => UserFilterDemo(),),
          (route) => false,//if you want to disable back feature set to false
    );
  }
  Color _colorFromHex(String hexColor) {
    final hexCode = hexColor.replaceAll('#', '');
    return Color(int.parse('FF$hexCode', radix: 16));
  }

  void getTime(String cuttentlat, currentlog, destlat, destlong) async {
    print("called"+cuttentlat+currentlog+destlat+destlong);
    var urlString = "https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=" +cuttentlat + "," + currentlog + "&destinations=" +destlat+","+destlong + "&key=AIzaSyBqU4wR7dFqFKT-QTCjAXVEGqvYUCu4jEQ";
    print("currenturl" + urlString);

    // 'https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=29.4727,77.7085&destinations=28.9845,77.7064&key=AIzaSyAWgUYmMPpD3mcpEBjz2FOB94PNqKGc-N0'
    final response =
    await http.get(Uri.parse(urlString));
    if (response.statusCode == 200) {
      // var result = await http.Response.fromStream(response);
      var jsonResponse = json.decode(response.body);
      if (jsonResponse['rows'][0]['elements'][0]['status'] == "ZERO_RESULTS"){
        setState(() {
          timeTaken = "Undefined Time";
        });
      }
      else{
        var respo = jsonResponse['rows'][0]['elements'][0]['duration']['text'];
        var respo1 = jsonResponse['rows'][0]['elements'][0]['distance']['text'];
         var split=respo1.split(" ");
        var prefix = split[0].trim();
        print("prefix"+prefix.toString());
        var  distanceKm=(double.parse(prefix))*1.6;
        print("respo " + distanceKm.toString());
        setState(() {
          timeTaken = respo;
          // distanceKm=respo1;
          eta_controller.text=timeTaken;
          dis_km.text=distanceKm.toString();
        });
      }
      print("==result get APi" + response.body.toString());
    } else {
      print("==response error" + response.toString());
    }
    // if (response.statusCode == 200) {
    //   // If the server did return a 200 OK response,
    //   // then parse the JSON.
    //   return Album.fromJson(jsonDecode(response.body));
    // } else {
    //   // If the server did not return a 200 OK response,
    //   // then throw an exception.
    //   throw Exception('Failed to load album');
    // }
    // var request = http.get(Uri.parse("https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=29.4727,77.7085&destinations=28.9845,77.7064&key=AIzaSyAWgUYmMPpD3mcpEBjz2FOB94PNqKGc-N0"));
    //       print("request" + request.toString());

    //
    // var response = await request.send();
  }

  _addMarker(LatLng position, String id, BitmapDescriptor descriptor) {
    MarkerId markerId = MarkerId(id);
    Marker marker =
    Marker(markerId: markerId, icon: descriptor, position: position);
    markers[markerId] = marker;

    sourcePinInfo = PinInformation(
        locationName: "Start Location",
        location: SOURCE_LOCATION,
        pinPath: "assets/marker.png",
        avatarPath: "assets/marker.png",
        labelColor: Colors.blueAccent);
  }


  // updateMarker(id){
  //
  //   final marker = markers.values.toList().firstWhere((item) => item.markerId == id);
  //
  //   Marker _marker = Marker(
  //     markerId: marker.markerId,
  //     onTap: () {
  //       print("tapped");
  //     },
  //     position: LatLng(marker.position.latitude, marker.position.longitude),
  //     icon: marker.icon,
  //     infoWindow: InfoWindow(title: 'my new Title'),
  //   );
  //
  //   setState(() {
  //     //the marker is identified by the markerId and not with the index of the list
  //     markers[id] = _marker;
  //   });
  // }


  _addPolyLine() {

    PolylineId id = PolylineId("poly");

    Polyline polyline = Polyline(
        polylineId: id, color: Colors.blue, points: polylineCoordinates);
    polylines[id] = polyline;

    setState(() {
      // create a Polyline instance
      // with an id, an RGB color and the list of LatLng pairs
      // create a Polyline instance
      // with an id, an RGB color and the list of LatLng pairs
      // add the constructed polyline as a set of points
      // to the polyline set, which will eventually
      // end up showing up on the map
    });
  }

  _getPolyline() async {
    PolylineResult result = await polylinePoints.getRouteBetweenCoordinates(
      apiKey,
      PointLatLng(eng_lat, eng_lng),
      PointLatLng(ticket_lat, ticket_lng),
      travelMode: TravelMode.driving,

    );
    if (result.points.isNotEmpty) {
      print(result.points);
      result.points.forEach((PointLatLng point) {
        polylineCoordinates.add(LatLng(point.latitude, point.longitude));
      });
    }
    _addPolyLine();


    // Dio dio = new Dio();
    // Response response=await dio.get("https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=$eng_lat,$eng_lng&destinations=$lat%2C,$lng&key=AIzaSyAWgUYmMPpD3mcpEBjz2FOB94PNqKGc-N0");
    // print("==ETA"+response.data.toString());

  }

  @override
  void initState() {

    subscription= _latLngStream .getLatLngInterpolation().listen((LatLngDelta delta) {
      LatLng from = delta.from;
      LatLng to = delta.to;
    });
    super.initState();
    // initPlatformState();
    socket();
    getTime(eng_lat.toString(), eng_lng.toString(), ticket_lat.toString(), ticket_lng.toString());

    _addMarker(LatLng(eng_lat, eng_lng), "sourcePin",
      BitmapDescriptor.fromAsset('assets/marker.png',),);
    _addMarker(LatLng(ticket_lat, ticket_lng), "destination",
        BitmapDescriptor.defaultMarkerWithHue(90));
    _getPolyline();
  }

  double latitude;
  double longitude;

  updateMarker(lat1, lng1) async  {
    String id='sourcePin';
    print("==udate marker"+lat1.toString()+lng1.toString());
    final GoogleMapController controller = await _controller.future;
    setState(()  async {
      var pinPosition = LatLng(lat1, lng1);
      controller.moveCamera(CameraUpdate.newCameraPosition(CameraPosition(target: pinPosition, zoom: 16)),);
      _addMarker(LatLng(lat1,lng1), "sourcePin",
        BitmapDescriptor.fromAsset('assets/marker.png',),);
      // polylineCoordinates.clear();

      // var markerId1=MarkerId('sourcePin');
      // Marker(markerId: markerId1,rotation: pinPosition.heading);

      // Location location = new Location();
      // location.onLocationChanged.listen((LocationData currentLocation) {
      //   // Use current location
      //
      //
      //  var markerId1=MarkerId('sourcePin');
      //   Marker(markerId: markerId1,rotation: currentLocation.heading);
      // });
    });
  }

  socket() {
    // 10.11.4.59:8080
     socket1 = io (Constants.socketConnection,<String, dynamic>{
      'transports': ['websocket'],
      'autoConnect': true,

      // 'extraHeaders': {'foo': 'bar'} // optional
    });
    socket1.connect();
    print("==hello server");
    // getTime(latitude.toString(), longitude.toString(), ticket_lat.toString(), ticket_lng.toString());
    socket1.on(id, (data) {
    print("socket"+data);
      // compute(  latitude=data['long'],longitude=data['lat']);
      var hjhj=jsonDecode(data);
      var  latitude=hjhj['lat'];
      var  longitude=hjhj['lng'];
      if(latitude!=null && longitude!=null){
        updateMarker (double.parse(latitude), double.parse(longitude));
        getTime(latitude, longitude, ticket_lat.toString(), ticket_lng.toString());

      }
    });
  }

  // Future<void> initPlatformState() async {
  //   // Configure BackgroundFetch.
  //   BackgroundFetch.configure(BackgroundFetchConfig(
  //     minimumFetchInterval: 15,
  //     stopOnTerminate: false,
  //     enableHeadless: false,
  //     requiresBatteryNotLow: false,
  //     requiresCharging: false,
  //     requiresStorageNotLow: false,
  //     requiresDeviceIdle: false,
  //   ), (String taskId) async {
  //     socket();
  //     // getTime(eng_lat.toString(), eng_lng.toString(), ticket_lat.toString(), ticket_lng.toString());
  //     // This is the fetch-event callback.
  //     print("[BackgroundFetch] Event received $taskId");
  //     // IMPORTANT:  You must signal completion of your task or the OS can punish your app
  //     // for taking too long in the background.
  //     BackgroundFetch.finish(taskId);
  //   }).then((int status) {
  //     print('[BackgroundFetch] configure success: $status');
  //   }).catchError((e) {
  //     print('[BackgroundFetch] configure ERROR: $e');
  //   });
  // }

  @override
  void deactivate() {
    super.deactivate();
    //this method not called when user press android back button or quit
    print('deactivate');
    socket1.off(id);
    socket1.dispose();
    socket1.disconnect();
  }

  @override
  void dispose() {
    super.dispose();
    // socket1.off(id);
    // socket1.dispose();
    // socket1.disconnect();

    //this method not called when user press android back button or quit
    print('dispose');
  }



}


String _markerIdVal({bool increment = false}) {
  String val = 'marker_id_$_markerIdCounter';
  if (increment) _markerIdCounter++;
  return val;
}



class Utils {
  static String mapStyles = '''[
  {
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#f5f5f5"
      }
    ]
  },
  {
    "elementType": "labels.icon",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  },
  {
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#616161"
      }
    ]
  },
  {
    "elementType": "labels.text.stroke",
    "stylers": [
      {
        "color": "#f5f5f5"
      }
    ]
  },
  {
    "featureType": "administrative.land_parcel",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#bdbdbd"
      }
    ]
  },
  {
    "featureType": "poi",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#eeeeee"
      }
    ]
  },
  {
    "featureType": "poi",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#757575"
      }
    ]
  },
  {
    "featureType": "poi.park",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#e5e5e5"
      }
    ]
  },
  {
    "featureType": "poi.park",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#9e9e9e"
      }
    ]
  },
  {
    "featureType": "road",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#ffffff"
      }
    ]
  },
  {
    "featureType": "road.arterial",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#757575"
      }
    ]
  },
  {
    "featureType": "road.highway",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#dadada"
      }
    ]
  },
  {
    "featureType": "road.highway",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#616161"
      }
    ]
  },
  {
    "featureType": "road.local",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#9e9e9e"
      }
    ]
  },
  {
    "featureType": "transit.line",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#e5e5e5"
      }
    ]
  },
  {
    "featureType": "transit.station",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#eeeeee"
      }
    ]
  },
  {
    "featureType": "water",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#c9c9c9"
      }
    ]
  },
  {
    "featureType": "water",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#9e9e9e"
      }
    ]
  }
]''';

}



