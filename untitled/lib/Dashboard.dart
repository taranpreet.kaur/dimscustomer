import 'dart:io';
import 'package:badges/badges.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:customer/DashboardHome.dart';
import 'package:customer/MapScreen.dart';
import 'package:customer/NotificationList.dart';
import 'package:customer/Person.dart';
import 'package:customer/Profile.dart';
import 'package:customer/SearchListViewExample.dart';
import 'package:customer/TicketCreate.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:flutter/material.dart';
import 'FabBottomBarItem.dart';
import 'SearchListViewExample1.dart';
import 'fabicons.dart';
import 'layout.dart';

void main() => runApp(MyApp());

/// This is the main application widget.
class MyApp extends StatelessWidget {
  static const String _title = 'Flutter Code Sample';
  @override
  Widget build(BuildContext context) {


    return new WillPopScope(
      onWillPop: _onWillPop,
      child:  Scaffold(
        appBar: AppBar(title: Text(_title),

        ),

        body: Dashboard(),
      ),
    );

  }
  // Future<bool> _onBackPressed() {
  //   return showDialog(
  //     context: context,
  //     builder: (context) => new AlertDialog(
  //       title: new Text('Are you sure?'),
  //       content: new Text('Do you want to exit an App'),
  //       actions: <Widget>[
  //         new GestureDetector(
  //           onTap: () => Navigator.of(context).pop(false),
  //           child: Text("NO"),
  //         ),
  //         SizedBox(height: 16),
  //         new GestureDetector(
  //           onTap: () => Navigator.of(context).pop(true),
  //           child: Text("YES"),
  //         ),
  //       ],
  //     ),
  //   ) ??
  //       false;
  // }

  BuildContext context;
  Future<bool> _onWillPop() async {
    return (await showDialog(
      builder: (context) => new AlertDialog(
        title: new Text('Are you sure?'),
        content: new Text('Do you want to exit an App'),
        actions: <Widget>[
          new FlatButton(
            onPressed: () => Navigator.of(context).pop(false),
            child: new Text('No'),
          ),
          new FlatButton(
            onPressed: () => Navigator.of(context).pop(true),
            child: new Text('Yes'),
          ),
        ],
      ), context: context,
    )) ?? false;
  }



}
class Dashboard extends StatefulWidget{
  Dashboard({Key key}) : super(key: key);
  @override
  _MyStatefulWidgetState createState() => _MyStatefulWidgetState();
}

/// This is the private State class that goes with MyStatefulWidget.
class _MyStatefulWidgetState extends State<Dashboard> {
  FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin;
  String _message = '';
  int _selectedIndex = 0;
  int _selectedIndex1 = 0;


  var data_lat;
  static const TextStyle optionStyle =
  TextStyle(fontSize: 30, fontWeight: FontWeight.bold, color: Colors.black);



  String _lastSelected = 'TAB: 0';

  void _selectedTab(int index) {
    setState(() {
      _lastSelected = 'TAB: $index';
    });
  }

  void _selectedFab(int _selectedIndex1) {
    setState(() {
      //
      widgetOptions1.elementAt(_selectedIndex1);

    });
  }

  final widgetOptions1 = [
    TicketCreate()
  ];



  final widgetOptions = [
    DashboardHome(),
    UserFilterDemo(),

    NotificationList(),
    Profile(),

  ];
  // static const List<Widget> _widgetOptions = <Widget>[
  //   Text(
  //     'Index 0: Home',
  //     style: optionStyle,
  //
  //   ),
  //   DashboardHome(),
  //   Text(
  //     'Index 1: Listings',
  //     style: optionStyle,
  //   ),
  //   Text(
  //     'Index 2: Add',
  //     style: optionStyle,
  //   ),
  //   Text(
  //     'Index 2: Notification',
  //     style: optionStyle,
  //   ),
  //   Text(
  //     'Index 2: Profile',
  //     style: optionStyle,
  //   ),
  //
  // ];

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
      print("==selectedIndex" + index.toString());
      if (index == 2) {
        // Navigator.push(
        //   context,
        //   MaterialPageRoute(builder: (context) => TicketCreate()),);
      }
      else if (index == 1) {
        // Navigator.push(context, MaterialPageRoute(builder: (context) => UserFilterDemo()),);
      }
      else if (index == 3) {
        // Navigator.push(context, MaterialPageRoute(builder: (context) => NotificationList()),);
        // socket();
      }
      // else if (index == 4) {
      //   // Navigator.push(context, MaterialPageRoute(builder: (context) => Profile()),);
      // }
    });
  }
  @override
  Widget build(BuildContext context) {
    // socket();
    Color color1 = _colorFromHex("#00ABC5");
    return Scaffold(
      // appBar: AppBar(actions: <Widget>[IconButton(onPressed:() {
      //   showSearch(context: context, delegate: Search());
      // }, icon: Icon(Icons.search),)],centerTitle: true,title: Text('Search Bar'),),
      body: Center(
        child: widgetOptions.elementAt(_selectedIndex),

        // child: _widgetOptions.elementAt(_selectedIndex),
      ),

      //
      bottomNavigationBar: FABBottomAppBar(

        centerItemText: '',
        color: Colors.grey,
        selectedColor:color1,
        notchedShape: CircularNotchedRectangle(),
        onTabSelected: _onItemTapped,
        selectedFontSize: 10,

        items: [

          FABBottomAppBarItem(iconData: Icons.home, text: ''),
          FABBottomAppBarItem(iconData: Icons.library_books, text: ''),
          FABBottomAppBarItem(iconData: Icons.notifications
              , text: ''),
          FABBottomAppBarItem(iconData: Icons.person, text: ''),
        ],
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      floatingActionButton: _buildFab(
          context), // This trailing comma makes auto-formatting nicer for build methods.
    );

  }

  Color _colorFromHex(String hexColor) {
    final hexCode = hexColor.replaceAll('#', '');
    return Color(int.parse('FF$hexCode', radix: 16));
  }

  Widget _buildFab(BuildContext context) {
    // final icons = [ Icons.sms, Icons.mail, Icons.phone ];
    // return AnchoredOverlay(
    //   showOverlay: true,
    //   overlayBuilder: (context, offset) {
    //     return CenterAbout(
    //      position: Offset(offset.dx, offset.dy - icons.length * 35.0),
    //       child: FabWithIcons(
    //        icons: icons,
    //         onIconTapped: _selectedFab,
    //       ),
    //     );
    //   },
    return FloatingActionButton(
      foregroundColor:Colors.white,
      backgroundColor:  _colorFromHex("#00ABC5"),
      onPressed: () {
        print("jjjj");
        Navigator.push(context, MaterialPageRoute(builder: (context) => TicketCreate()),);
      },
      tooltip: 'Increment',
      child: Icon(Icons.add),
      elevation: 2.0,
    );
  }
















  // bottomNavigationBar: BottomNavigationBar(
  //   items: const <BottomNavigationBarItem>[
  //     BottomNavigationBarItem(
  //       icon: Icon(Icons.home),
  //       title: Text('Home'),
  //       // backgroundColor: Colors.black
  //     ),
  //     BottomNavigationBarItem(
  //       icon: ImageIcon(
  //
  //     AssetImage('assets/ticket.png'),
  //     ),
  //       title: Text("Listings"),
  //       // backgroundColor: Colors.black
  //     ),
  //     // BottomNavigationBarItem(
  //     //   icon: ImageIcon(
  //     //
  //     //     AssetImage('assets/plus.png'),
  //     //   ),
  //     //   title: Text('Add'),
  //     //
  //     //
  //     // ),
  //     BottomNavigationBarItem(
  //      icon: Icon(Icons.notifications),
  //       title: Text('Notification'),
  //     ),
  //
  //     BottomNavigationBarItem(
  //       icon: Icon(Icons.supervised_user_circle),
  //       title: Text('Profile'),
  //       // backgroundColor: Colors.black
  //     ),
  //   ],
  //   currentIndex: _selectedIndex,
  //   selectedItemColor: color1,
  //   unselectedItemColor: Colors.black,
  //   showUnselectedLabels: true,
  //   onTap: _onItemTapped,
  // ),



  final _fab = FloatingActionButton(
    child: Icon(Icons.add),
    backgroundColor: Colors.black,
    onPressed: () {},
  );



  // socket() {
  //   Socket socket1 = io('http://10.11.4.59:8080', <String, dynamic>{
  //     'transports': ['websocket'],
  //     'autoConnect': false,
  //     // 'extraHeaders': {'foo': 'bar'} // optional
  //   });
  //   socket1.connect();
  //   socket1.on("track", (data) {
  //     print("engineer coOrdinate: "+data["lat"]);
  //   });
  // }
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getMessage();
    flutterLocalNotificationsPlugin = new FlutterLocalNotificationsPlugin();
    var android = new AndroidInitializationSettings('@mipmap/ic_launcher');
    var iOS = new IOSInitializationSettings();
    var initSetttings = new InitializationSettings(android: android, iOS: iOS);
    flutterLocalNotificationsPlugin.initialize(initSetttings,onSelectNotification: onSelectNotification);
  }

  void getMessage(){
    final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
    _firebaseMessaging.configure(onMessage: (Map<String, dynamic> message) async {
      print('==on message $message');
      _message = message["notification"]["title"];
      setState(() =>
          showNotification(_message));
    },
        // onBackgroundMessage: myBackgroundMessageHandler,
        onResume: (Map<String, dynamic> message) async {
          print('==on resume $message');
          _message = message["notification"]["title"];
          setState(() => showNotification(_message));
        },
        onLaunch: (Map<String, dynamic> message) async {
          print('==on launch $message');
          _message = message["notification"]["title"];
          setState(() => showNotification(_message));
        });
  }

  showNotification(String message) async {
    var android = new AndroidNotificationDetails(
        'channel id', 'channel NAME', 'CHANNEL DESCRIPTION',
        priority: Priority.high,importance: Importance.max
    );
    var iOS = new IOSNotificationDetails();
    var platform = new NotificationDetails(android: android, iOS: iOS);
    await flutterLocalNotificationsPlugin.show(
        0, message, "", platform,
        payload: 'AndroidCoding.in');
  }

  Future onSelectNotification(String payload) {
    debugPrint("payload : $payload");

    Navigator.pushAndRemoveUntil<dynamic>(
      context,
      MaterialPageRoute<dynamic>(
        builder: (BuildContext context) => NotificationList(),
      ),
          (route) => false,//if you want to disable back feature set to false
    );

  }

}
