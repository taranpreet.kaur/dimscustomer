
import 'Location.dart';

class TicketModel {
  Location location;
  String assetType;
  String assetId;
  String id;
  String status;

  TicketModel(
      {
        this.location,
        this.assetType,
        this.assetId,
        this.id,
        this.status,


      });

  factory TicketModel.fromJson(Map<String, dynamic> json) {
    return TicketModel(

      location : new Location.fromJson(json['location']),
      assetType : json['assetType'],
      assetId : json['assetId'],
      id:json['_id'],
      status : json['status'],
         );
  }

  static Map<String, dynamic> toMap(TicketModel music) => {
    'location':music.location,
    'assetType':music.assetType,
    'assetId':music.assetId,
    '_id': music.id,
    'status': music.status,
  };


}