import 'dart:convert';
import 'package:customer/Constant.dart';
import 'package:customer/myform.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'Article.dart';
import 'NotificationResponse.dart';
import 'Notify.dart';

class NotificationList extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _MyAppState();
  }
}

class _MyAppState extends State<NotificationList> {
  String _message = '';
  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getMessage();
  }

     void getMessage(){
    _firebaseMessaging.configure(
        onMessage: (Map<String, dynamic> message) async {
          print('on message $message');
          setState(() => _message = message["notification"]["title"]);
        },
        // onBackgroundMessage: myBackgroundMessageHandler,
        onResume: (Map<String, dynamic> message) async {
          print('on resume $message');
          setState(() => _message = message["notification"]["title"]);
        }, onLaunch: (Map<String, dynamic> message) async {
      print('on launch $message');
      setState(() => _message = message["notification"]["title"]);
    });
  }

  var tempList = List<String>();
  @override
  Widget build(BuildContext context) {
    Color color1 = _colorFromHex("#00ABC5");
    // TODO: implement build
    return FlutterEasyLoading( child:Scaffold(
      backgroundColor: Colors.transparent,
      appBar: AppBar(
        flexibleSpace: Image(
          image: AssetImage('assets/appbar_background.png'),
          fit: BoxFit.cover,
        ),
        title: Text("Notification List"),
      ),

        body: WillPopScope(
        onWillPop: _onBackPressed,
         child:
         FutureBuilder(
            future: getData(),
            builder: (context, snapshot) {
              return snapshot.data != null
                  ? listViewWidget1(snapshot.data)
              : Center(child: CircularProgressIndicator());
            }),
    ),  ), );
  }

       Widget listViewWidget(List<NotificationResponse> article) {
       return Container(
           child: ListView.builder(
          itemCount: article.length,
          padding: const EdgeInsets.all(2.0),
          itemBuilder: (context, position) {
            return Card(
              child: ListTile(
                title: Text(
                  '${article[position].notification_details.title}',
                  style: TextStyle(
                      fontSize: 18.0,
                      color: Colors.black,
                      fontWeight: FontWeight.normal),
                ),
                subtitle: Text('${article[position].notification_details.body}',style: TextStyle(fontSize: 16.0,color:Colors.black,fontWeight:FontWeight.normal),),


              ),
            );
          }),
    );
  }





  Widget listViewWidget1(List<NotificationResponse> article) {

    return Center(
        child: ListView.builder(
          // separatorBuilder: (context, index) => Divider(
          //   //color: Colors.white,
          // ),
            itemCount: article.length,
            itemBuilder: (context, index) => Padding(
                padding: EdgeInsets.fromLTRB(7, 1, 7, 4),
                child: InkWell(
                  onTap: () async {


                  },
//                           child: Container(
//                             height: 120,
//                             child: Card(
//
//                               // shape: RoundedRectangleBorder(
//                               //   borderRadius: BorderRadius.circular(1.0),
//                               // ),
//                               // color: Colors.white,
//                               //elevation: 3,
//                               child: Row(
//                                 mainAxisSize: MainAxisSize.min,
//                                 children: <Widget>[
//                                   Container(
//                                     height: 120,
//                                     width: 2 ,
// color: Colors.red,
//
//                                   ),
//                                   SizedBox(
//                                     width: 13,
//
//                                   ),
//                                   Container(
//                                     height: 40,
//                                     width: 60 ,
//
//                                     child: Text("26 Nov 2021"),
//                                   ),
//                                   SizedBox(
//                                     // width: ,
//
//                                   ),
//                                   Container(
//
//                                     height: 70,
//                                     width: 2,
//                                     color: Colors.black12,
//                                   ),
//                                   SizedBox(
//                                     width: 10,
//
//                                   ),
//                                   Container(
//                                     height: 100,
//
//                                     child: Column(
//                                         // mainAxisAlignment: MainAxisAlignment.start,
//                                         // mainAxisSize: MainAxisSize.min,
//                                         children: <Widget>[
//                                           // Align(
//                                           //
//                                           //     child: Text("Sector 18, Gurugram,\nHaryana 122022",
//                                           //       style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold,
//                                           //           color: Color(0xFF101010)),)
//                                           // ),
//                                           SizedBox(
//                                             height: 8,
//                                           ),
//
//
//                                          Padding(
//                                             padding: EdgeInsets.fromLTRB(3, 3, 0, 0),
//                                               child: Align(
//
//                                                  alignment: Alignment.bottomLeft,
//                                                child: Text(
//                                                  '${article[index].notification_details.title}',
//                                                  style: TextStyle(fontSize: 20.0,color: Colors.black),
//                                                textAlign: TextAlign.left ,
//                                                ),
//                                            ),
//                                             ),
//
//
//
//                                        SizedBox(
//                                          height: 10,
//                                        ),
//                                           Container(
//                                              width: 300,
//                                             child: Text(
//                                                 '${article[index].notification_details.body}'
//                                                 ,
//                                                 overflow: TextOverflow.ellipsis,
//                                                 maxLines: 5,
//
//                                                 style: TextStyle(fontSize: 15.0)
//                                             )
//                                           ),
//                                        ]
//                                     ),)
//
//
//
//                                 ],
//                               ),
//                             ),
//                           ),
                  child: Container(
                    height: 100,
                    child: Card(

                      // shape: RoundedRectangleBorder(
                      //   borderRadius: BorderRadius.circular(1.0),
                      // ),
                      // color: Colors.white,
                      //elevation: 3,
                      child: Row(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          Container(
                            height: 100,
                            width: 2 ,

                            color: Colors.red,
                          ),
                          SizedBox(
                            width: 13,

                          ),
                          Container(
                            height: 60,
                            width: 90 ,

                            child: Text('${article[index].notification_details.date}'),
                          ),
                          SizedBox(
                            width: 10 ,

                          ),
                          Container(

                            height: 70,
                            width: 2,
                            color: Colors.black12,
                          ),
                          SizedBox(
                            width: 10,

                          ),
                          Container(
                            height: 80,

                            child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                // mainAxisAlignment: MainAxisAlignment.start,
                                // mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  // Align(
                                  //
                                  //     child: Text("Sector 18, Gurugram,\nHaryana 122022",
                                  //       style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold,
                                  //           color: Color(0xFF101010)),)
                                  // ),
                                  SizedBox(
                                    height: 4,
                                  ),


                                  Align(
                                    alignment: Alignment.centerLeft,
                                    child: Container(
                                      //color: Colors.blue,
                                      child: Text(
                                        '${article[index].notification_details.title}',
                                        style: TextStyle(fontSize: 15.0, fontWeight: FontWeight.bold,
                                            color: Color(0xFF696969)),
                                        //  textAlign: TextAlign.left ,
                                      ),
                                    ),
                                  ),



                                  SizedBox(
                                    height: 2,
                                  ),
                                  Container(
                                      width: 260,
                                      height:  50,
                                      child: Text(
                                        // "nstance of 'TicketResponse', Instance of 'TicketResponse', Instance of 'TicketResponse', Instance of 'TicketResponse', Instance of 'TicketResponse', Instance of 'TicketResponse', Instance of 'TicketResponse', Instance of 'TicketResponse', Instance of 'TicketResponse']"
                                          '${article[index].notification_details.body}'
                                          ,
                                          overflow: TextOverflow.ellipsis,
                                          maxLines: 6,

                                          style: TextStyle(fontSize: 13.0)
                                      )
                                  ),
                                ]
                            ),)

                        ],
                      ),
                    ),
                  ),

                )
            )
        )
    );
  }




  Future<bool> _onBackPressed() {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text('Are you sure?'),
            content: Text('Do you want to exit from App!!'),
            actions: <Widget>[

              FlatButton(
                child: Text('YES'),
                onPressed: () {
                  Navigator.of(context).pop(true);
                },
              ),
              FlatButton(
                child: Text('NO'),
                onPressed: () {
                  Navigator.of(context).pop(false);
                },
              ),
            ],
          );
        });
  }

      Future<List<NotificationResponse>> getData() async {
      // EasyLoading.show(status: "Loading..");
      List<NotificationResponse> list;
      final prefs = await SharedPreferences.getInstance();
      var userId = prefs.getString("userId");
      tempList = List<String>();
      var url=Constants.base_url+"api/extranet/get/notification";

      var request =  http.MultipartRequest('POST',Uri.parse(url));
      print("request"+request.toString());
      request.fields['user_id']=userId;
      var response=await request.send();
      if (response.statusCode == 200) {
        EasyLoading.dismiss(animation: false);
      var result = await http.Response.fromStream(response);
      var data = json.decode(result.body);
      var rest=data["resp"] as List;
      print(rest);
      list = rest.map<NotificationResponse>((json) => NotificationResponse.fromJson(json)).toList();
     }
      else{
        // EasyLoading.showError("Error");

      }
     print("List Size: ${list.length}");
     return list;
    }

    Color _colorFromHex(String hexColor) {
    final hexCode = hexColor.replaceAll('#', '');
    return Color(int.parse('FF$hexCode', radix: 16));
  }
}
