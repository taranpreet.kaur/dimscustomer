import 'dart:convert';
import 'dart:core';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:customer/Constant.dart';
import 'package:customer/SearchListViewExample.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:responsive_widgets/responsive_widgets.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:step_progress_indicator/step_progress_indicator.dart';
import 'package:http/http.dart' as http;
import 'Coordinates.dart';
import 'package:intl/intl.dart';
import 'Article.dart';
import 'TicketResponse.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class DashboardHome extends StatefulWidget {
  DashboardHome():super();
  @override
  DashboardHomeState createState() => DashboardHomeState();
}
class DashboardHomeState extends State<DashboardHome>{
  LatLng currentPostion ;
  bool loading;
  double _originLatitude=28.605407, _originLongitude=77.2146617 ;
  double _destLatitude = 28.605407, _destLongitude = 77.2146617;
  ScrollController _scrollController = ScrollController();



  @override
  void initState() {
    super.initState();
    loading = false;
    getDetails();
    printLocation();
  }

  var isLoading =false;
  List<Article> users = List();
  String name,sentence;
  var count=1;
  var totalOpen;
  var todayCount;
  var sla;
  var count1;
  var unassigned,todayResolved,workInProgress,inJourney,onHold;


  Future<bool> _onBackPressed() {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text('Are you sure?'),
            content: Text('Do you want to exit from App!!'),
            actions: <Widget>[
              FlatButton(
                child: Text('YES'),
                onPressed: () {
                  Navigator.of(context).pop(true);
                },
              ),
              FlatButton(
                child: Text('NO'),
                onPressed: () {
                  Navigator.of(context).pop(false);
                },
              ),
            ],
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    ResponsiveWidgets.init(context,
      height: 1920, // Optional
      width: 1080, // Optional
      allowFontScaling: true, // Optional
   );
    // Sizer(
    //    builder: (context, orientation, deviceType) {
    return ResponsiveWidgets.builder(
        height: 1920, // Optional
        width: 1080, // Optional
        allowFontScaling: true, // Optional

        child:   FlutterEasyLoading(child:  MaterialApp(
          debugShowCheckedModeBanner: false,

          // theme: ThemeData(
          //   backgroundColor: _colorFromHex("#00ABC5"),
          // ),
          home: Scaffold(

            backgroundColor: Color(0xFFF8FCFF),
            appBar:PreferredSize(
              preferredSize: Size.fromHeight(140.0),
              child:  AppBar(
                flexibleSpace: Container(
                  padding: EdgeInsets.all(10),
                  decoration: BoxDecoration(image: DecorationImage(
                    image: AssetImage('assets/appbar_background.png'),
                    fit: BoxFit.cover,
                  )
                  ),

                  child:  Column(
                      children: <Widget>[
                        // TextField(
                        //     style: TextStyle(
                        //       fontSize: 15,
                        //       color: Colors.white,
                        //     )
                        //   ,
                        //     readOnly: true,
                        //     textAlign: TextAlign.left,
                        //    decoration: InputDecoration(
                        //    labelText: "Welcome,",
                        //      enabled: false,
                        //      labelStyle: TextStyle(
                        //         color: Colors.white,
                        //       ),
                        //       border: InputBorder.none,
                        //       focusedBorder: InputBorder.none,
                        //       enabledBorder: InputBorder.none,
                        //       errorBorder: InputBorder.none,
                        //       disabledBorder: InputBorder.none,
                        //        )),
                        Container(
                            margin: new EdgeInsets.fromLTRB(10,20,10,0),
                            child: Align(alignment: Alignment.centerLeft,

                              child: Text("Welcome,",textAlign: TextAlign.left,
                                style: TextStyle( color: Colors.white,fontSize: 14),),
                            )),

                        Container(
                            margin: new EdgeInsets.fromLTRB(10,5,10,10),
                            child: Align(alignment: Alignment.centerLeft,

                              child: Text(sentence ??="Name",textAlign: TextAlign.left,
                                style: TextStyle( color: Colors.white,fontSize: 18),),
                            )),


                        // TextField(
                        //
                        //     style: TextStyle(
                        //       color: Colors.white,
                        //         decorationColor: Colors.white,
                        //       fontSize: 15
                        //     )
                        //     ,
                        //     readOnly: true,
                        //     enabled: false,
                        //     maxLines: 1,
                        //     textAlign: TextAlign.left,
                        //     decoration: InputDecoration(
                        //       labelText: sentence ??="Name",
                        //       labelStyle: TextStyle(
                        //         color: Colors.white,
                        //       ),
                        //       border: InputBorder.none,
                        //       focusedBorder: InputBorder.none,
                        //       enabledBorder: InputBorder.none,
                        //       errorBorder: InputBorder.none,
                        //       disabledBorder: InputBorder.none,
                        //     )
                        // ),
                        //    InkWell(child:
                        GestureDetector(

                            child: new Container(height: 50,
                              margin: EdgeInsets.fromLTRB(10, 10, 0, 0),
                              decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(8)),
                              child: TextFormField(
                                onTap: () {
                                  print("---dhbh");
                                  Navigator.push(context, MaterialPageRoute(builder: (context) =>UserFilterDemo()));
                                },
                                cursorColor: Colors.black,
                                readOnly: true,
                                // keyboardType: TextInputType.,
                                decoration: InputDecoration(
                                  hintStyle: TextStyle(fontSize: 15),
                                  hintText: 'Search by Ticket Id & Asset Type',
                                  prefixIcon: Icon(Icons.search),
                                  border: InputBorder.none,
                                  contentPadding: EdgeInsets.all(16),
                                ),
                              ),)
                        ),
                      ]
                  ),),),),
            body:
            WillPopScope(
              onWillPop: _onBackPressed,
              child: SingleChildScrollView(
                child:
                isLoading ? Center(

                  child: CircularProgressIndicator(),
                )
                    :
                Column(
                  children: [
                    Padding(padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
                      child: Container(
                        alignment: Alignment.center,

                        decoration: BoxDecoration(
                          boxShadow: [BoxShadow(
                            color: Colors.grey,
                            blurRadius: 5.0,
                          ),],
                          color: Colors.white,
                          border: Border.all(
                            color: Colors.white,
                          ),
                          borderRadius: BorderRadius.circular(7.0),
                        ),


                        child: Padding(
                          padding: EdgeInsets.all(20),
                          child: Column(
                            children: [
                              Align(
                                  alignment: Alignment.centerLeft,
                                  child: Text("Total Open Ticket Status",
                                    style: TextStyle(fontSize: 13.0, fontWeight: FontWeight.w300,
                                        color: Color(0xFF919191)),)),
                              SizedBox(
                                height: 5,
                              ),
                              Align(
                                alignment: Alignment.centerLeft,
                                child:    TextField(
                                  controller: _controller,
                                  textAlign: TextAlign.left,
                                  decoration: InputDecoration( border: InputBorder.none,),

                                  style: TextStyle(fontWeight: FontWeight.bold),
                                  readOnly: true,

                                ),),
                              SizedBox(
                                height: 5,
                              ),
                              Container(
                                child:  StepProgressIndicator(

                                  totalSteps: count ,
                                  currentStep: todayCount ?? 0,
                                  size: 25,
                                  padding: 10,
                                  selectedColor: Colors.yellow,
                                  unselectedColor: Colors.cyan,
                                  roundedEdges: Radius.circular(12),
                                  selectedGradientColor: LinearGradient(
                                    begin: Alignment.topLeft,
                                    end: Alignment.bottomRight,
                                    colors: [Colors.yellowAccent, Colors.deepOrange],
                                  ),
                                  unselectedGradientColor: LinearGradient(
                                    begin: Alignment.topLeft,
                                    end: Alignment.bottomRight,
                                    colors: [Colors.black, Colors.blue],
                                  ),
                                ),


                              ),


                              SizedBox(height: 10,),

                              Column(
                                  children: [
                                    ContainerResponsive(
                                      height: 200, width: 700,
                                      heightResponsive: true,
                                      widthResponsive: true,
                                      color: Colors.white,
                                      alignment: Alignment.center,
                                      child:Padding(
                                          padding: EdgeInsetsResponsive.all(0),
                                          child: ListView.builder(
                                            physics: const NeverScrollableScrollPhysics(),
                                            itemCount: 3,
                                            scrollDirection: Axis.horizontal,
                                            itemBuilder: (context,index){

                                              if (index == 0){

                                                return ContainerResponsive(
                                                  margin: EdgeInsetsResponsive.fromLTRB(25, 10, 10, 10),
                                                  height: 200,
                                                  heightResponsive: true,
                                                  width: 280,
                                                  widthResponsive: true,
                                                  alignment: Alignment.center,
                                                  // color: index  == 0 ? Colors.orange : index == 1 ? Colors.red :
                                                  // index == 2 ? Colors.blue : index == 3 ? Colors.purple : Colors.purple,

                                                  // decoration: BoxDecoration(
                                                  //     color: index  == 0 ? Colors.orange : index == 1 ? Colors.red :
                                                  //     index == 2 ? Colors.blue : index == 3 ? Colors.purple : Colors.purple,
                                                  //     borderRadius: BorderRadius.all(Radius.circular(7))
                                                  // ),
                                                  child: Padding(padding: EdgeInsetsResponsive.only(top: 30, left: 0, right: 15),
                                                      child: GestureDetector(
                                                        onTap: (){
                                                          Navigator.pushAndRemoveUntil<dynamic>(
                                                            context,
                                                            MaterialPageRoute<dynamic>(
                                                              builder: (BuildContext context) => UserFilterDemo(),
                                                            ),
                                                                (route) => false,//if you want to disable back feature set to false
                                                          );
                                                        },
                                                        child: Container(child: Column(
                                                      children: [
                                                        Align(
                                                            alignment: Alignment.topLeft,
                                                            child: Text("${this.litems1[index]}",
                                                              style: TextStyle(
                                                                color: index  == 0 ? Colors.orange : index == 1 ? Colors.green :
                                                                index == 2? Colors.purple : index == 3 ? Colors.purple : Colors.purple,
                                                                // color: Colors.white,
                                                                fontWeight: FontWeight.bold,
                                                                fontSize: 14,
                                                              ),)),
                                                        Align(
                                                            alignment: Alignment.topLeft,
                                                            child: Text(unassigned.toString(),
                                                              style: TextStyle(
                                                                color: Colors.black,
                                                                fontWeight: FontWeight.w900,
                                                                fontSize: 14,
                                                              ),)),
                                                      ],
                                                    ),
                                                  ),
                                                )));
                                              }

                                              else if(index == 1){

                                                return ContainerResponsive(
                                                  margin: EdgeInsetsResponsive.fromLTRB(10, 20, 10, 10),
                                                  height: 150,
                                                  heightResponsive: true,
                                                  width: 250,
                                                  widthResponsive: true,
                                                  alignment: Alignment.center,
                                                  // color: index  == 0 ? Colors.orange : index == 1 ? Colors.red :
                                                  // index == 2 ? Colors.blue : index == 3 ? Colors.purple : Colors.purple,

                                                  // decoration: BoxDecoration(
                                                  //     color: index  == 0 ? Colors.orange : index == 1 ? Colors.red :
                                                  //     index == 2 ? Colors.blue : index == 3 ? Colors.purple : Colors.purple,
                                                  //     borderRadius: BorderRadius.all(Radius.circular(7))
                                                  // ),
                                                  child: Padding(padding:  EdgeInsetsResponsive.only(top: 15, left: 25, right: 15),
                                              child: GestureDetector(
                                              onTap: (){
                                              Navigator.pushAndRemoveUntil<dynamic>(
                                              context,
                                              MaterialPageRoute<dynamic>(
                                              builder: (BuildContext context) => UserFilterDemo(),
                                              ),
                                              (route) => false,//if you want to disable back feature set to false
                                              );
                                              },
                                              child: Container(   child: Column(
                                                      children: [
                                                        Align(
                                                            alignment: Alignment.topLeft,
                                                            child: Text("${this.litems1[index]}",

                                                              style: TextStyle(
                                                                color: index  == 0 ? Colors.orange : index == 1 ? Colors.green :
                                                                index == 2? Colors.purple : index == 3 ? Colors.purple : Colors.purple,
                                                                // color: Colors.white,
                                                                fontWeight: FontWeight.bold,
                                                                fontSize: 14,
                                                              ),)),
                                                        Align(
                                                            alignment: Alignment.topLeft,
                                                            child: Text((todayResolved).toString(),
                                                              style: TextStyle(
                                                                color: Colors.black,
                                                                fontWeight: FontWeight.w900,
                                                                fontSize: 14,
                                                              ),)),
                                                      ],
                                                    ),
                                                  ),
                                                )));
                                              }

                                              else if(index == 2){

                                                return ContainerResponsive(
                                                  margin: EdgeInsetsResponsive.fromLTRB(10, 20, 10, 10),
                                                  height: 150,
                                                  heightResponsive: true,
                                                  width: 250,
                                                  widthResponsive: true,
                                                  alignment: Alignment.center,
                                                  // color: index  == 0 ? Colors.orange : index == 1 ? Colors.red :
                                                  // index == 2 ? Colors.blue : index == 3 ? Colors.purple : Colors.purple,

                                                  // decoration: BoxDecoration(
                                                  //     color: index  == 0 ? Colors.orange : index == 1 ? Colors.red :
                                                  //     index == 2 ? Colors.blue : index == 3 ? Colors.purple : Colors.purple,
                                                  //     borderRadius: BorderRadius.all(Radius.circular(7))
                                                  // ),
                                                  child: Padding(padding:  EdgeInsetsResponsive.only(top: 15, left: 25, right: 15),
                                              child: GestureDetector(
                                              onTap: (){
                                              Navigator.pushAndRemoveUntil<dynamic>(
                                              context,
                                              MaterialPageRoute<dynamic>(
                                              builder: (BuildContext context) => UserFilterDemo(),
                                              ),
                                              (route) => false,//if you want to disable back feature set to false
                                              );
                                              },
                                              child: Container(
                                              child: Column(
                                                      children: [
                                                        Align(
                                                            alignment: Alignment.topLeft,
                                                            child: Text("${this.litems1[index]}",

                                                              style: TextStyle(
                                                                color: index  == 0 ? Colors.orange : index == 1 ? Colors.green :
                                                                index == 2? Colors.purple : index == 3 ? Colors.purple : Colors.purple,
                                                                // color: Colors.white,
                                                                fontWeight: FontWeight.bold,
                                                                fontSize: 14,
                                                              ),)),
                                                        Align(
                                                            alignment: Alignment.topLeft,
                                                            child: Text((workInProgress).toString(),
                                                              style: TextStyle(
                                                                color: Colors.black,
                                                                fontWeight: FontWeight.w900,
                                                                fontSize: 14,
                                                              ),)),
                                                      ],
                                                    ),
                                                  ),
                                                )));
                                              }


                                              // return Container(
                                              //   margin: EdgeInsets.fromLTRB(10, 10, 10, 10),
                                              //   height: 30,
                                              //   width: 100,
                                              //   alignment: Alignment.center,
                                              //   // color: index  == 0 ? Colors.orange : index == 1 ? Colors.red :
                                              //   // index == 2 ? Colors.blue : index == 3 ? Colors.purple : Colors.purple,
                                              //
                                              //   // decoration: BoxDecoration(
                                              //   //     color: index  == 0 ? Colors.orange : index == 1 ? Colors.red :
                                              //   //     index == 2 ? Colors.blue : index == 3 ? Colors.purple : Colors.purple,
                                              //   //     borderRadius: BorderRadius.all(Radius.circular(7))
                                              //   // ),
                                              //   child: Padding(padding: const EdgeInsets.only(top: 15, left: 15, right: 15),
                                              //     child: Column(
                                              //       children: [
                                              //         Align(
                                              //             alignment: Alignment.center,
                                              //             child: Text("${this.litems1[index]}",
                                              //
                                              //
                                              //               style: TextStyle(
                                              //                 color: index  == 0 ? Colors.orange : index == 1 ? Colors.green :
                                              //                 index == 2 ? Colors.purple : index == 3 ? Colors.purple : Colors.purple,
                                              //                 // color: Colors.white,
                                              //                 fontWeight: FontWeight.bold,
                                              //                 fontSize: 12,
                                              //               ),)),
                                              //         Align(
                                              //             alignment: Alignment.center,
                                              //             child: Text("${this.seconditems1[index]}",
                                              //               style: TextStyle(
                                              //                 color: Colors.black,
                                              //                 fontWeight: FontWeight.w900,
                                              //                 fontSize: 14,
                                              //               ),)),
                                              //       ],
                                              //     ),
                                              //   ),
                                              // );
                                            },
                                          )
                                      ),
                                    ),
                                  ]),

                              Column(
                                  children: [
                                    ContainerResponsive(
                                      height: 200, width: 700,
                                      heightResponsive: true,
                                      widthResponsive: true,
                                      color: Colors.white,
                                      child:Padding(
                                          padding: EdgeInsetsResponsive.all(0),
                                          child: ListView.builder(
                                            physics: const NeverScrollableScrollPhysics(),
                                            itemCount: 3,
                                            scrollDirection: Axis.horizontal,
                                            itemBuilder: (context,index){

                                              if (index == 0){

                                                return ContainerResponsive(
                                                  margin: EdgeInsetsResponsive.fromLTRB(25, 10, 10, 10),
                                                  height: 200,
                                                  heightResponsive: true,
                                                  width: 280,
                                                  widthResponsive: true,
                                                  alignment: Alignment.center,
                                                  // color: index  == 0 ? Colors.orange : index == 1 ? Colors.red :
                                                  // index == 2 ? Colors.blue : index == 3 ? Colors.purple : Colors.purple,

                                                  // decoration: BoxDecoration(
                                                  //     color: index  == 0 ? Colors.orange : index == 1 ? Colors.red :
                                                  //     index == 2 ? Colors.blue : index == 3 ? Colors.purple : Colors.purple,
                                                  //     borderRadius: BorderRadius.all(Radius.circular(7))
                                                  // ),
                                                  child: Padding(padding: EdgeInsetsResponsive.only(top: 30, left: 0, right: 15),
                                                      child: GestureDetector(
                                                        onTap: (){
                                                          Navigator.pushAndRemoveUntil<dynamic>(
                                                            context,
                                                            MaterialPageRoute<dynamic>(
                                                              builder: (BuildContext context) => UserFilterDemo(),
                                                            ),
                                                                (route) => false,//if you want to disable back feature set to false
                                                          );
                                                        },
                                                        child: Container(
                                                        child: Column(
                                                      children: [
                                                        Align(
                                                            alignment: Alignment.topLeft,
                                                            child: Text("${this.litems2[index]}",
                                                              style: TextStyle(
                                                                color: index  == 0 ? Colors.red : index == 1 ? Colors.green :
                                                                index == 2? Colors.purple : index == 3 ? Colors.purple : Colors.purple,
                                                                // color: Colors.white,
                                                                fontWeight: FontWeight.bold,
                                                                fontSize: 14,
                                                              ),)),
                                                        Align(
                                                            alignment: Alignment.topLeft,
                                                            child: Text(onHold.toString(),
                                                              style: TextStyle(
                                                                color: Colors.black,
                                                                fontWeight: FontWeight.w900,
                                                                fontSize: 14,
                                                              ),)),
                                                      ],
                                                    ),
                                                  ),
                                                )));
                                              }

                                              else if(index == 1){

                                                return ContainerResponsive(
                                                  margin: EdgeInsetsResponsive.fromLTRB(10, 20, 10, 10),
                                                  height: 150,
                                                  heightResponsive: true,
                                                  width: 250,
                                                  widthResponsive: true,
                                                  alignment: Alignment.center,
                                                  // color: index  == 0 ? Colors.orange : index == 1 ? Colors.red :
                                                  // index == 2 ? Colors.blue : index == 3 ? Colors.purple : Colors.purple,

                                                  // decoration: BoxDecoration(
                                                  //     color: index  == 0 ? Colors.orange : index == 1 ? Colors.red :
                                                  //     index == 2 ? Colors.blue : index == 3 ? Colors.purple : Colors.purple,
                                                  //     borderRadius: BorderRadius.all(Radius.circular(7))
                                                  // ),
                                                  child: Padding(padding:  EdgeInsetsResponsive.only(top: 15, left: 25, right: 15),
                                                   child: GestureDetector(
                                                      onTap: (){
                                                        Navigator.pushAndRemoveUntil<dynamic>(
                                                          context,
                                                          MaterialPageRoute<dynamic>(
                                                            builder: (BuildContext context) => UserFilterDemo(),
                                                          ),
                                                              (route) => false,//if you want to disable back feature set to false
                                                        );
                                                      },
                                                    child: Container(child: Column(

                                                      children: [

                                                        Align(
                                                            alignment: Alignment.topLeft,
                                                            child: Text("${this.litems2[index]}",

                                                              style: TextStyle(
                                                                color: index  == 0 ? Colors.orange : index == 1 ? Colors.yellow :
                                                                index == 2? Colors.purple : index == 3 ? Colors.purple : Colors.purple,
                                                                // color: Colors.white,
                                                                fontWeight: FontWeight.bold,
                                                                fontSize: 14,
                                                              ),)),
                                                        Align(
                                                            alignment: Alignment.topLeft,
                                                            child: Text((inJourney).toString(),
                                                              style: TextStyle(
                                                                color: Colors.black,
                                                                fontWeight: FontWeight.w900,
                                                                fontSize: 14,
                                                              ),)),
                                                      ],
                                                    ),
                                                    ),

                                                  ),
                                                ));
                                              }

                                              else if(index == 2){

                                                return ContainerResponsive(
                                                  margin: EdgeInsetsResponsive.fromLTRB(10, 20, 10, 10),
                                                  height: 150,
                                                  heightResponsive: true,
                                                  width: 250,
                                                  widthResponsive: true,
                                                  alignment: Alignment.center,
                                                  // color: index  == 0 ? Colors.orange : index == 1 ? Colors.red :
                                                  // index == 2 ? Colors.blue : index == 3 ? Colors.purple : Colors.purple,

                                                  // decoration: BoxDecoration(
                                                  //     color: index  == 0 ? Colors.orange : index == 1 ? Colors.red :
                                                  //     index == 2 ? Colors.blue : index == 3 ? Colors.purple : Colors.purple,
                                                  //     borderRadius: BorderRadius.all(Radius.circular(7))
                                                  // ),
                                                  child: Padding(padding:  EdgeInsetsResponsive.only(top: 15, left: 25, right: 15),
                                              child: GestureDetector(
                                              onTap: (){
                                              Navigator.pushAndRemoveUntil<dynamic>(
                                              context,
                                              MaterialPageRoute<dynamic>(
                                              builder: (BuildContext context) => UserFilterDemo(),
                                              ),
                                              (route) => false,//if you want to disable back feature set to false
                                              );
                                              },
                                              child: Container(   child: Column(
                                                      children: [
                                                        Align(
                                                            alignment: Alignment.topLeft,
                                                            child: Text("${""}",

                                                              style: TextStyle(
                                                                color: index  == 0 ? Colors.orange : index == 1 ? Colors.green :
                                                                index == 2? Colors.purple : index == 3 ? Colors.purple : Colors.purple,
                                                                // color: Colors.white,
                                                                fontWeight: FontWeight.bold,
                                                                fontSize: 14,
                                                              ),)),
                                                        Align(
                                                            alignment: Alignment.topLeft,
                                                            child: Text(("").toString(),
                                                              style: TextStyle(
                                                                color: Colors.black,
                                                                fontWeight: FontWeight.w900,
                                                                fontSize: 14,
                                                              ),)),
                                                      ],
                                                    ),
                                                  ),
                                                )));
                                              }


                                              // return Container(
                                              //   margin: EdgeInsets.fromLTRB(10, 10, 10, 10),
                                              //   height: 30,
                                              //   width: 100,
                                              //   alignment: Alignment.center,
                                              //   // color: index  == 0 ? Colors.orange : index == 1 ? Colors.red :
                                              //   // index == 2 ? Colors.blue : index == 3 ? Colors.purple : Colors.purple,
                                              //
                                              //   // decoration: BoxDecoration(
                                              //   //     color: index  == 0 ? Colors.orange : index == 1 ? Colors.red :
                                              //   //     index == 2 ? Colors.blue : index == 3 ? Colors.purple : Colors.purple,
                                              //   //     borderRadius: BorderRadius.all(Radius.circular(7))
                                              //   // ),
                                              //   child: Padding(padding: const EdgeInsets.only(top: 15, left: 15, right: 15),
                                              //     child: Column(
                                              //       children: [
                                              //         Align(
                                              //             alignment: Alignment.center,
                                              //             child: Text("${this.litems1[index]}",
                                              //
                                              //
                                              //               style: TextStyle(
                                              //                 color: index  == 0 ? Colors.orange : index == 1 ? Colors.green :
                                              //                 index == 2 ? Colors.purple : index == 3 ? Colors.purple : Colors.purple,
                                              //                 // color: Colors.white,
                                              //                 fontWeight: FontWeight.bold,
                                              //                 fontSize: 12,
                                              //               ),)),
                                              //         Align(
                                              //             alignment: Alignment.center,
                                              //             child: Text("${this.seconditems1[index]}",
                                              //               style: TextStyle(
                                              //                 color: Colors.black,
                                              //                 fontWeight: FontWeight.w900,
                                              //                 fontSize: 14,
                                              //               ),)),
                                              //       ],
                                              //     ),
                                              //   ),
                                              // );
                                            },
                                          )
                                      ),
                                    ),
                                  ]),

                            ],
                          ),
                        ),
                      ),
                    ),




                    Padding(
                      padding: EdgeInsets.all(10),
                      child: Column(
                        children: [
                          Align(
                              alignment: Alignment.centerLeft,
                              child: Text("Ticket Stats",
                                style: TextStyle(fontSize: 18.0, fontWeight: FontWeight.bold,
                                    color: Color(0xFF101010)),)),
                          SizedBox(
                            height: 5,
                          ),

                        ],
                      ),
                    ),

                    Column(
                      children: [
                        CarouselSlider(
                          options: CarouselOptions(
                            aspectRatio: 16/9,
                            height:130,
                            viewportFraction: 0.8,
                            initialPage: 0,
                            enableInfiniteScroll: true,
                            reverse: false,
                            autoPlay: true,
                            autoPlayInterval: Duration(seconds: 3),
                            autoPlayAnimationDuration: Duration(milliseconds: 800),
                            autoPlayCurve: Curves.fastOutSlowIn,
                            enlargeCenterPage: false,
                            scrollDirection: Axis.horizontal,

                          ),
                          items:[

                            Container(
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.all(Radius.circular(10)),
                                  color: Colors.orange
                              ),
                              width: 300,
                              // color: Colors.orange,
                              height: 1,
                              padding: EdgeInsets.fromLTRB(12,12,12,12),
                              margin: EdgeInsets.fromLTRB(10,10,10,10),

                              child: Column(
                                children: [
                                  Align(
                                      alignment: Alignment.topLeft,
                                      child: Text("Total Logged",

                                        style: TextStyle(
                                          color:Colors.white,
                                          // color: Colors.white,
                                          fontWeight: FontWeight.bold,
                                          fontSize: 16,
                                        ),)),
                                  Align(
                                      alignment: Alignment.topLeft,
                                      child: Text((count1).toString(),
                                        style: TextStyle(
                                          color: Colors.white,
                                          fontWeight: FontWeight.w900,
                                          fontSize: 16,
                                        ),)),
                                ],
                              ),),


                            Container(
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.all(Radius.circular(10)),
                                  color: Colors.red
                              ),

                              padding: EdgeInsets.fromLTRB(12,12,12,12),
                              margin: EdgeInsets.fromLTRB(10,10,10,10),
                              width: 300,
                              child: Column(
                                children: [
                                  Align(
                                      alignment: Alignment.topLeft,
                                      child: Text("Today Logged",

                                        style: TextStyle(
                                          color:Colors.white,
                                          // color: Colors.white,
                                          fontWeight: FontWeight.bold,
                                          fontSize: 16,
                                        ),)),
                                  Align(
                                      alignment: Alignment.topLeft,
                                      child: Text((todayCount).toString(),
                                        style: TextStyle(
                                          color: Colors.white,
                                          fontWeight: FontWeight.w900,
                                          fontSize: 16,
                                        ),)),
                                ],
                              ),),

                            Container(
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.all(Radius.circular(10)),
                                  color: Colors.blue
                              ),
                              padding: EdgeInsets.fromLTRB(12,12,12,12),
                              margin: EdgeInsets.fromLTRB(10,10,10,10),
                              width: 300,

                              child: Column(
                                children: [
                                  Align(
                                      alignment: Alignment.topLeft,
                                      child: Text("SLA",

                                        style: TextStyle(
                                          color:Colors.white,
                                          // color: Colors.white,
                                          fontWeight: FontWeight.bold,
                                          fontSize: 16,
                                        ),)),
                                  Align(
                                      alignment: Alignment.topLeft,
                                      child: Text((sla.toString()+"%").toString(),
                                        style: TextStyle(
                                          color: Colors.white,
                                          fontWeight: FontWeight.w900,
                                          fontSize: 16,
                                        ),)),
                                ],
                              ),),

                      ],),

                      SizedBox(
                          height: 10,
                        ),
                        Padding(
                          padding: EdgeInsets.all(15),
                          child: Align(
                              alignment: Alignment.centerLeft,
                              child: Text("Live Map",
                                style: TextStyle(fontSize: 21.0, fontWeight: FontWeight.bold,
                                    color: Color(0xFF101010)),)),
                        ),
                        SizedBox(
                          height: 10,
                          width: 10,
                        ),
                       Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[
                              SizedBox(
                                width: 20,
                              ),
                              Icon(Icons.location_on,color: Colors.deepPurple,),
                              Container(width: 150,
                              child: Text("New"),),
                              SizedBox(width: 10,),
                              Icon(Icons.location_on,color: Colors.orange,),
                              Container(
                                child: Text("Assign To Engineer"),)


                            ]
                        ),





                        SizedBox(width: 30,),



                        SizedBox(
                          height: 10,
                          width: 10,
                        ),
                        Row(
                            children: <Widget>[
                              SizedBox(
                                width: 20,
                              ),
                              Icon(Icons.location_on,color: Colors.yellow,),
                              Container(width: 150,
                                child: Text("Engineer On the way"),),


                              SizedBox(width: 10,),
                              Icon(Icons.location_on,color: Colors.purple,),

                              Text("WIP"),
                            ]
                        ),
                        SizedBox(
                          height: 10,
                          width: 10,
                        ),
                       Row(

                            children: <Widget>[
                              SizedBox(
                                width: 20,
                              ),
                              Icon(Icons.location_on,color: Colors.red,),
                              Container(width: 150,
                                child: Text("OnHold"),),



                              SizedBox(width: 10,),
                              Icon(Icons.location_on,color: Colors.purpleAccent,),
                              Text("UnderObservation"),



                            ]
                        ),



                        SizedBox(
                          height: 10,
                        ),
                        Container(
                          height: 450, width: 360,
                          color: Colors.white,

                          padding: EdgeInsets.all(2.0),



                          child: loading ? GoogleMap(
                            markers: Set.of(markers.values),
                            onMapCreated: _onMapCreated,
                            myLocationEnabled: true,
                            myLocationButtonEnabled: true,
                            mapType: MapType.normal,
                            scrollGesturesEnabled: true,
                            zoomGesturesEnabled: true,

                            gestureRecognizers: <Factory<OneSequenceGestureRecognizer>>[
                              new Factory<OneSequenceGestureRecognizer>(() => new EagerGestureRecognizer(),),
                            ].toSet(),
                            initialCameraPosition: CameraPosition(
                              target: currentPostion,
                              zoom: 12.0,
                            ),
                            //  myLocationEnabled: true,
                          ) : Container(),
                        )


                ],
              ),
            ],)),),
        ),


        ),
      // color: Color(0xFFF8FCFF),
    ));
  }





  Color _colorFromHex(String hexColor) {
    final hexCode = hexColor.replaceAll('#', '');
    return Color(int.parse('FF$hexCode', radix: 16));
  }

  List<String> litems = ["Total Logged","Today Logged","SLA Adherence MTD Closure"];
  List<String> litems1 = ["Unassigned","Today Resolved","WIP"];
  List<String> litems2 = ["OnHold","In Journey"];

  List<String> seconditems = ["34,799 (93.8%)","2,455 (6.1%)","1000"];
  List<String> seconditems1 = ["34,799 (93.8%)","2,455 (6.1%)","1000"];
  List<String> seconditems11 = ["34,799 (93.8%)","2,455 (6.1%)"];

  GoogleMapController myController;
  final LatLng _center = const LatLng(45.521563, -122.677433);
  var _controller = TextEditingController();
  var lat,lng;
  List<Coordinates> list = [];
  // Iterable markers = [];
  Future<void> _onMapCreated(GoogleMapController controller) async {
    myController = controller;
    if (this.mounted)
      setState(() {
        myController = controller;
      });
    // _location.onLocationChanged.listen((l) {
    //   myController.animateCamera(CameraUpdate.newCameraPosition(CameraPosition(target: LatLng(28.567310,77.188680),zoom: 15),),);
    // });
  }




  void printLocation() async {
    Position position = await Geolocator().getCurrentPosition(desiredAccuracy: LocationAccuracy.high);
    print("current position: "+position.latitude.toString()+""+position.longitude.toString());
    _originLatitude = position.latitude;
    _originLongitude =  position.longitude;
    setState(() {
      currentPostion =  LatLng(_originLatitude,_originLongitude);
      _addMarker(LatLng(_originLatitude, _originLongitude), "origin",
          BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueAzure));
      loading = true;
      print("checklfjdjfdjfjdd f d" + loading.toString());
    });


  }

  _addMarker(LatLng position, String id, BitmapDescriptor descriptor) {
    print("position " + position.toString());
    MarkerId markerId = MarkerId(id);
    Marker marker =
    Marker(markerId: markerId, icon: descriptor, position: position);
    markers[markerId] = marker;
  }




  getDetails() async {
    // EasyLoading.show(status: "Loading..");
    setState(() {
      isLoading = true;
      CircularProgressIndicator();
    });
    print("==getdetails");
    SharedPreferences sharedPreferences    = await SharedPreferences.getInstance();
    var id=sharedPreferences.getString("userId");
    name=sharedPreferences.getString('name').trim();
    sentence = toBeginningOfSentenceCase(name); // This is a string

    print("userId"+id.toString());
    var url=Constants.base_url+"api/cust/getTicketCount";

    var request =  http.MultipartRequest('POST',Uri.parse(url));
    request.fields['userId']=id;
    var response=await request.send();
    if(response.statusCode == 200) {
      // getUsers();
      getAllData();
      setState(() {
        isLoading =false;
      });
      var result = await http.Response.fromStream(response);
      final jsonResponse = jsonDecode(result.body);
      print("==jsonresponse tickets dashboard"+jsonResponse.toString());
       count1=jsonResponse[0]["totalCount"];
      totalOpen=jsonResponse[0]['totalOpen'];
      todayCount=jsonResponse[0]['todayCount'];
      todayResolved=jsonResponse[0]['todayResolved'];
      unassigned=jsonResponse[0]['unassigned'];
      inJourney=jsonResponse[0]['inJourney'];
      onHold=jsonResponse[0]['onHold'];
      workInProgress=jsonResponse[0]['workInProgress'];
       sla=jsonResponse[0]['sla'];

      //
      // if(count1 ==null){
      //      _controller.text="0";
      //    }
      //    else if(count1==0)
      //      {
      //        count=1;
      //        _controller.text=totalOpen.toString();
      //      }
      //    else{
      //      _controller.text=totalOpen.toString();
      //    }

      if(totalOpen==null){
        _controller.text="0";
      }
      else
      {
        _controller.text=totalOpen.toString();
      }

      if(count1==null){
        count1="0";
        count=1;
      }



      if(inJourney==null){
        seconditems11[1]='0';
      }
      else{
        seconditems11[1]=inJourney.toString();
      }
      if(onHold==null){
        seconditems11[0]='0';
      }
      else{
        seconditems11[0]=onHold.toString();
      }

      if(todayResolved==null){
        seconditems1[1]="0";
      }
      else{
        seconditems1[1]=todayResolved.toString();
      }

      if(workInProgress==null){
        seconditems1[2]="0";
      }
      else{
        seconditems1[2]=workInProgress.toString();
      }

      if(unassigned==null){
        seconditems1[0]="0";
      }
      else{
        seconditems1[0]=unassigned.toString();
      }
      if(totalOpen==null){
        seconditems[0]="0";
        seconditems[1]="0";
        seconditems[2]="0";
      }
      else{
        seconditems[0]=count1.toString();
        seconditems[1]=todayCount.toString();
        seconditems[2]=sla.toString()+"%".toString();

      }

      if(todayCount==null){
        seconditems[1]="0";

      }
      else if(todayCount==0){
        seconditems[1]="0";

      }
      else{
        seconditems[1]=todayCount.toString();
      }
    }

  }


//      getUsers() async {
//     String url = 'http://10.11.4.59:8080/api/cust/user/geTickets';
//
//       try {
//         SharedPreferences sharedPreferences    = await SharedPreferences.getInstance();
//         // final response = await http.get(url);
//         var request =  http.MultipartRequest('POST',Uri.parse(url));
// //     print("request"+request.toString());
//         request.fields['customerID']=sharedPreferences.getString("userId");
//
//         var response=await request.send();
//
//         if (response.statusCode == 200) {
//
//           var result1 = await http.Response.fromStream(response);
//           final jsonResponse = jsonDecode(result1.body);
//
//           var result=jsonResponse['result'] as List;
//           for(int i=0;i<result.length;i++){
//             var location=result[i]['location'];
//             var coordinates=location["coordinates"];
//              lat=coordinates[1];
//              lng=coordinates[0];
//             list.add(Coordinates(lat, lng));
//             setState(() {
//               markers = _markers;
//             });
//
//           _markers = Iterable.generate(list.length, (index) {
//               return Marker(
//                   markerId: MarkerId("1"),
//                   position: LatLng(
//                     lat,
//                    lng,
//                   ),
//                   infoWindow: InfoWindow(title: "hii")
//               );
//             });
//              print("location"+lat.toString()+lng.toString());
//           }
//
//         }
//         else {
//           EasyLoading.showError("Error");
//           throw Exception("Error");
//         }
//       }
//       catch (e) {
//         throw Exception(e.toString());
//       }
//     }




  List<TicketResponse> _AlluserDetails = [];
  Map<MarkerId, Marker> markers = <MarkerId, Marker>{};
  getAllData() async {
    List<TicketResponse> list;
    print("called get data");
    final sharedPreferences = await SharedPreferences.getInstance();
    var id = sharedPreferences.get("token");
    var idd = sharedPreferences.getString("token");

    print("==shared in notifo" + id + "hjhj" + idd);
    // EasyLoading.show(status :"Loading...");
    var url=Constants.base_url+"api/cust/user/geTickets";

    var request = http.MultipartRequest('POST', Uri.parse(url));
    print("request" + request.toString());
    request.fields['customerID'] = sharedPreferences.getString("userId");
    var response = await request.send();

    print(response.statusCode);

    // listen for response
    if (response.statusCode == 200) {
      var result = await http.Response.fromStream(response);
      var jsonResponse = json.decode(result.body);
      print(jsonResponse);


      var texts = jsonResponse["result"];

      print(texts);


      list = texts.map<TicketResponse>((json) => TicketResponse.fromJson(json))
          .toList();
      _AlluserDetails = list;

      for(int i =0;i< _AlluserDetails.length;i++){
        /*if(i==0){
          MarkerId markerId = MarkerId("markers"+i.toString());
          markers[markerId] = Marker(
            markerId: markerId,
            position: LatLng(_AlluserDetails[i].notification_details.cordinate.coordinates[1].toDouble(),_AlluserDetails[i].notification_details.cordinate.coordinates[0].toDouble()),
            infoWindow: InfoWindow(
              title: "bingo! This works",
            ),
            icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueRed),
          );
        }else if(i==1){
          MarkerId markerId = MarkerId("markers"+i.toString());
          markers[markerId] = Marker(
            markerId: markerId,
            position: LatLng(28.567818, 77.209562),
            infoWindow: InfoWindow(
              title: "bingo! This is works",
            ),
            icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueRed),
          );
        }else{
          MarkerId markerId = MarkerId("markers"+i.toString());
          markers[markerId] = Marker(
            markerId: markerId,
            position: LatLng(28.567262, 77.187369),
            infoWindow: InfoWindow(
              title: "bingo! This is also works",
            ),
            icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueRed),
          );*/
        if(_AlluserDetails[i].notification_details.status == "RIM_NEW")
        {
          MarkerId markerId = MarkerId("markers"+i.toString());
          markers[markerId] = Marker(
            markerId: markerId,
            position: LatLng(_AlluserDetails[i].notification_details.location?.coordinates[1]?.toDouble()?? 0.0,_AlluserDetails[i].notification_details.location?.coordinates[0]?.toDouble()??0.0),

            infoWindow: InfoWindow(title: "New",),
            icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueBlue),
          );
        }
        else if(_AlluserDetails[i].notification_details.status == "ENG_NEW"){
          MarkerId markerId = MarkerId("markers"+i.toString());
          markers[markerId] = Marker(
            markerId: markerId,
            position: LatLng(_AlluserDetails[i].notification_details.location?.coordinates[1]?.toDouble()??0.0,_AlluserDetails[i].notification_details.location?.coordinates[0]?.toDouble()??0.0),

            infoWindow: InfoWindow(title: "NEW",),
            icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueBlue),
          );
        }
        else if(_AlluserDetails[i].notification_details.status == "ENG_ACCEPT"){
          MarkerId markerId = MarkerId("markers"+i.toString());
          markers[markerId] = Marker(
            markerId: markerId,
            position: LatLng(_AlluserDetails[i].notification_details.location?.coordinates[1]?.toDouble()??0.0,_AlluserDetails[i].notification_details.location?.coordinates[0]?.toDouble()??0.0),

            infoWindow: InfoWindow(title: "Assigned to Engineer",),
            icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueOrange),
          );
        }

        else if(_AlluserDetails[i].notification_details.status == "ENG_REJECT"){
          MarkerId markerId = MarkerId("markers"+i.toString());
          markers[markerId] = Marker(
            markerId: markerId,
            position: LatLng(_AlluserDetails[i].notification_details.location?.coordinates[1]?.toDouble()??0.0,_AlluserDetails[i].notification_details.location?.coordinates[0]?.toDouble()??0.0),

            infoWindow: InfoWindow(title: "New",),
            icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueBlue),
          );
        }
        else if(_AlluserDetails[i].notification_details.status == "ENG_PFS"){
          MarkerId markerId = MarkerId("markers"+i.toString());
          markers[markerId] = Marker(
            markerId: markerId,
            position: LatLng(_AlluserDetails[i].notification_details.location?.coordinates[1]?.toDouble()??0.0,_AlluserDetails[i].notification_details.location?.coordinates[0]?.toDouble()??0.0),

            infoWindow: InfoWindow(title: "Engineer on the way",),
            icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueYellow),
          );
        }
        else if(_AlluserDetails[i].notification_details.status == "ENG_RAS"){
          MarkerId markerId = MarkerId("markers"+i.toString());
          markers[markerId] = Marker(
            markerId: markerId,
            position: LatLng(_AlluserDetails[i].notification_details.location?.coordinates[1]?.toDouble()??0.0,_AlluserDetails[i].notification_details.location?.coordinates[0]?.toDouble()??0.0),

            infoWindow: InfoWindow(title: "WIP",),
            icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueViolet),
          );
        } else if(_AlluserDetails[i].notification_details.status == "ENG_RESOLVED"){
          MarkerId markerId = MarkerId("markers"+i.toString());
          markers[markerId] = Marker(
            markerId: markerId,
            position: LatLng(_AlluserDetails[i].notification_details.location?.coordinates[1]?.toDouble()??0.0,_AlluserDetails[i].notification_details.location?.coordinates[0]?.toDouble()??0.0),

            infoWindow: InfoWindow(title: "RESOLVED",),
            icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueGreen),
          );
        }

        else if(_AlluserDetails[i].notification_details.status == "ENG_RESO"){
          MarkerId markerId = MarkerId("markers"+i.toString());
          markers[markerId] = Marker(
            markerId: markerId,
            position: LatLng(_AlluserDetails[i].notification_details.location?.coordinates[1]?.toDouble()??0.0,_AlluserDetails[i].notification_details.location?.coordinates[0]?.toDouble()??0.0),

            infoWindow: InfoWindow(title: "RESOLVED",),
            icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueGreen),
          );
        }


        else if(_AlluserDetails[i].notification_details.status == "ENG_UOB"){
          MarkerId markerId = MarkerId("markers"+i.toString());
          markers[markerId] = Marker(
            markerId: markerId,
            position: LatLng(_AlluserDetails[i].notification_details.location?.coordinates[1]?.toDouble()??0.0,_AlluserDetails[i].notification_details.location?.coordinates[0]?.toDouble()??0.0),

            infoWindow: InfoWindow(title: "Under Observation",),
            icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueMagenta),
          );
        }
        else if(_AlluserDetails[i].notification_details.status == "ENG_SPAREQ"){
          MarkerId markerId = MarkerId("markers"+i.toString());
          markers[markerId] = Marker(
            markerId: markerId,
            position: LatLng(_AlluserDetails[i].notification_details.location?.coordinates[1]?.toDouble()??0.0,_AlluserDetails[i].notification_details.location?.coordinates[0]?.toDouble()??0.0),

            infoWindow: InfoWindow(title: "WIP",),
            icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueViolet),
          );
        }
        else if(_AlluserDetails[i].notification_details.status == "ENG_WIP"){
          MarkerId markerId = MarkerId("markers"+i.toString());
          markers[markerId] = Marker(
            markerId: markerId,
            position: LatLng(_AlluserDetails[i].notification_details.location?.coordinates[1]?.toDouble()??0.0,_AlluserDetails[i].notification_details.location?.coordinates[0]?.toDouble()??0.0),

            infoWindow: InfoWindow(title: "WIP",),
            icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueViolet),
          );
        }
        else if(_AlluserDetails[i].notification_details.status == "ENG_STDBY_INSTL"){
          MarkerId markerId = MarkerId("markers"+i.toString());
          markers[markerId] = Marker(
            markerId: markerId,
            position: LatLng(_AlluserDetails[i].notification_details.location?.coordinates[1]?.toDouble()??0.0,_AlluserDetails[i].notification_details.location?.coordinates[0]?.toDouble()??0.0),

            infoWindow: InfoWindow(title: "Standby Provided",),
            icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueViolet),
          );
        }
        else if(_AlluserDetails[i].notification_details.status == "ENG_PDC"){
          MarkerId markerId = MarkerId("markers"+i.toString());
          markers[markerId] = Marker(
            markerId: markerId,
            position: LatLng(_AlluserDetails[i].notification_details.location?.coordinates[1]?.toDouble()??0.0,_AlluserDetails[i].notification_details.location?.coordinates[0]?.toDouble()??0.0),

            infoWindow: InfoWindow(title: "On Hold",),
            icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueRed),
          );
        }
        else if(_AlluserDetails[i].notification_details.status == "ENG_TRANSIM"){
          MarkerId markerId = MarkerId("markers"+i.toString());
          markers[markerId] = Marker(
            markerId: markerId,
            position: LatLng(_AlluserDetails[i].notification_details.location?.coordinates[1]?.toDouble()??0.0,_AlluserDetails[i].notification_details.location?.coordinates[0]?.toDouble()??0.0),

            infoWindow: InfoWindow(title: "WIP",),
            icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueViolet),
          );
        }

        else if(_AlluserDetails[i].notification_details.status == "ENG_TRNS_IM"){
          MarkerId markerId = MarkerId("markers"+i.toString());
          markers[markerId] = Marker(
            markerId: markerId,
            position: LatLng(_AlluserDetails[i].notification_details.location?.coordinates[1]?.toDouble()??0.0,_AlluserDetails[i].notification_details.location?.coordinates[0]?.toDouble()??0.0),

            infoWindow: InfoWindow(title: "WIP",),
            icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueViolet),
          );
        }
        else if(_AlluserDetails[i].notification_details.status == "ENG_WPS"){
          MarkerId markerId = MarkerId("markers"+i.toString());
          markers[markerId] = Marker(
            markerId: markerId,
            position: LatLng(_AlluserDetails[i].notification_details.location?.coordinates[1]?.toDouble()??0.0,_AlluserDetails[i].notification_details.location?.coordinates[0]?.toDouble()??0.0),

            infoWindow: InfoWindow(title: "WIP",),
            icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueViolet),
          );
        }



        else if(_AlluserDetails[i].notification_details.status == "ENG_SPR_REQ"){
          MarkerId markerId = MarkerId("markers"+i.toString());
          markers[markerId] = Marker(
            markerId: markerId,
            position: LatLng(_AlluserDetails[i].notification_details.location?.coordinates[1]?.toDouble()??0.0,_AlluserDetails[i].notification_details.location?.coordinates[0]?.toDouble()??0.0),

            infoWindow: InfoWindow(title: "WIP",),
            icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueViolet),
          );
        }

        else if(_AlluserDetails[i].notification_details.status == "Standby Delivered"){
          MarkerId markerId = MarkerId("markers"+i.toString());
          markers[markerId] = Marker(
            markerId: markerId,
            position: LatLng(_AlluserDetails[i].notification_details.location?.coordinates[1]?.toDouble()??0.0,_AlluserDetails[i].notification_details.location?.coordinates[0]?.toDouble()??0.0),

            infoWindow: InfoWindow(title: "Standby Given",),
            icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueCyan),
          );
        }

        else if(_AlluserDetails[i].notification_details.status == "Standby Delivered and Faulty Picked"){
          MarkerId markerId = MarkerId("markers"+i.toString());
          markers[markerId] = Marker(
            markerId: markerId,
            position: LatLng(_AlluserDetails[i].notification_details.location?.coordinates[1]?.toDouble()??0.0,_AlluserDetails[i].notification_details.location?.coordinates[0]?.toDouble()??0.0),

            infoWindow: InfoWindow(title: "Standby Given",),
            icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueCyan),
          );
        }



        else if(_AlluserDetails[i].notification_details.status == "PENG_NEW"){
          MarkerId markerId = MarkerId("markers"+i.toString());
          markers[markerId] = Marker(
            markerId: markerId,
            position: LatLng(_AlluserDetails[i].notification_details.location?.coordinates[1]?.toDouble()??0.0,_AlluserDetails[i].notification_details.location?.coordinates[0]?.toDouble()??0.0),

            infoWindow: InfoWindow(title: "NEW",),
            icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueBlue),
          );
        }
        else if(_AlluserDetails[i].notification_details.status == "PENG_ACCEPT"){
          MarkerId markerId = MarkerId("markers"+i.toString());
          markers[markerId] = Marker(
            markerId: markerId,
            position: LatLng(_AlluserDetails[i].notification_details.location?.coordinates[1]?.toDouble()??0.0,_AlluserDetails[i].notification_details.location?.coordinates[0]?.toDouble()??0.0),

            infoWindow: InfoWindow(title: "Assign to Engineer",),
            icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueOrange),
          );
        }

        else if(_AlluserDetails[i].notification_details.status == "PENG_REJECT"){
          MarkerId markerId = MarkerId("markers"+i.toString());
          markers[markerId] = Marker(
            markerId: markerId,
            position: LatLng(_AlluserDetails[i].notification_details.location?.coordinates[1]?.toDouble()??0.0,_AlluserDetails[i].notification_details.location?.coordinates[0]?.toDouble()??0.0),

            infoWindow: InfoWindow(title: "New",),
            icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueBlue),
          );
        }
        else if(_AlluserDetails[i].notification_details.status == "PENG_PFS"){
          MarkerId markerId = MarkerId("markers"+i.toString());
          markers[markerId] = Marker(
            markerId: markerId,
            position: LatLng(_AlluserDetails[i].notification_details.location?.coordinates[1]?.toDouble()??0.0,_AlluserDetails[i].notification_details.location?.coordinates[0]?.toDouble()??0.0),

            infoWindow: InfoWindow(title: "Engineer on the way",),
            icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueYellow),
          );
        }
        else if(_AlluserDetails[i].notification_details.status == "PENG_RAS"){
          MarkerId markerId = MarkerId("markers"+i.toString());
          markers[markerId] = Marker(
            markerId: markerId,
            position: LatLng(_AlluserDetails[i].notification_details.location?.coordinates[1]?.toDouble()??0.0,_AlluserDetails[i].notification_details.location?.coordinates[0]?.toDouble()??0.0),

            infoWindow: InfoWindow(title: "WIP",),
            icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueViolet),
          );
        } else if(_AlluserDetails[i].notification_details.status == "PENG_RESOLVED"){
          MarkerId markerId = MarkerId("markers"+i.toString());
          markers[markerId] = Marker(
            markerId: markerId,
            position: LatLng(_AlluserDetails[i].notification_details.location?.coordinates[1]?.toDouble()??0.0,_AlluserDetails[i].notification_details.location?.coordinates[0]?.toDouble()??0.0),

            infoWindow: InfoWindow(title: "RESOLVED",),
            icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueGreen),
          );
        }

        else if(_AlluserDetails[i].notification_details.status == "PENG_RESO"){
          MarkerId markerId = MarkerId("markers"+i.toString());
          markers[markerId] = Marker(
            markerId: markerId,
            position: LatLng(_AlluserDetails[i].notification_details.location?.coordinates[1]?.toDouble()??0.0,_AlluserDetails[i].notification_details.location?.coordinates[0]?.toDouble()??0.0),

            infoWindow: InfoWindow(title: "RESOLVED",),
            icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueGreen),
          );
        }


        else if(_AlluserDetails[i].notification_details.status == "PENG_UOB"){
          MarkerId markerId = MarkerId("markers"+i.toString());
          markers[markerId] = Marker(
            markerId: markerId,
            position: LatLng(_AlluserDetails[i].notification_details.location?.coordinates[1]?.toDouble()??0.0,_AlluserDetails[i].notification_details.location?.coordinates[0]?.toDouble()??0.0),

            infoWindow: InfoWindow(title: "Under Observation",),
            icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueMagenta),
          );
        }

        else if(_AlluserDetails[i].notification_details.status == "PENG_TRNS_PART"){
          MarkerId markerId = MarkerId("markers"+i.toString());
          markers[markerId] = Marker(
            markerId: markerId,
            position: LatLng(_AlluserDetails[i].notification_details.location?.coordinates[1]?.toDouble()??0.0,_AlluserDetails[i].notification_details.location?.coordinates[0]?.toDouble()??0.0),

            infoWindow: InfoWindow(title: "WIP",),
            icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueMagenta),
          );
        }

        else if(_AlluserDetails[i].notification_details.status == "PENG_SPAREQ"){
          MarkerId markerId = MarkerId("markers"+i.toString());
          markers[markerId] = Marker(
            markerId: markerId,
            position: LatLng(_AlluserDetails[i].notification_details.location?.coordinates[1]?.toDouble()??0.0,_AlluserDetails[i].notification_details.location?.coordinates[0]?.toDouble()??0.0),

            infoWindow: InfoWindow(title: "WIP",),
            icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueViolet),
          );
        }
        else if(_AlluserDetails[i].notification_details.status == "PENG_WIP"){
          MarkerId markerId = MarkerId("markers"+i.toString());
          markers[markerId] = Marker(
            markerId: markerId,
            position: LatLng(_AlluserDetails[i].notification_details.location?.coordinates[1]?.toDouble()??0.0,_AlluserDetails[i].notification_details.location?.coordinates[0]?.toDouble()??0.0),

            infoWindow: InfoWindow(title: "WIP",),
            icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueViolet),
          );
        }



        else if(_AlluserDetails[i].notification_details.status == "PENG_WPS"){
          MarkerId markerId = MarkerId("markers"+i.toString());
          markers[markerId] = Marker(
            markerId: markerId,
            position: LatLng(_AlluserDetails[i].notification_details.location?.coordinates[1]?.toDouble()??0.0,_AlluserDetails[i].notification_details.location?.coordinates[0]?.toDouble()??0.0),

            infoWindow: InfoWindow(title: "WIP",),
            icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueViolet),
          );
        }



        else if(_AlluserDetails[i].notification_details.status == "PENG_PDC"){
          MarkerId markerId = MarkerId("markers"+i.toString());
          markers[markerId] = Marker(
            markerId: markerId,
            position: LatLng(_AlluserDetails[i].notification_details.location?.coordinates[1]?.toDouble()??0.0,_AlluserDetails[i].notification_details.location?.coordinates[0]?.toDouble()??0.0),

            infoWindow: InfoWindow(title: "On Hold",),
            icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueRed),
          );
        }



        else if(_AlluserDetails[i].notification_details.status == "PENG_STDBY_INSTL"){
          MarkerId markerId = MarkerId("markers"+i.toString());
          markers[markerId] = Marker(
            markerId: markerId,
            position: LatLng(_AlluserDetails[i].notification_details.location?.coordinates[1]?.toDouble()??0.0,_AlluserDetails[i].notification_details.location?.coordinates[0]?.toDouble()??0.0),

            infoWindow: InfoWindow(title: "Standby Provided",),
            icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueRed),
          );
        }


        else if(_AlluserDetails[i].notification_details.status == "PENG_TRANSIM"){
          MarkerId markerId = MarkerId("markers"+i.toString());
          markers[markerId] = Marker(
            markerId: markerId,
            position: LatLng(_AlluserDetails[i].notification_details.location?.coordinates[1]?.toDouble()??0.0,_AlluserDetails[i].notification_details.location?.coordinates[0]?.toDouble()??0.0),

            infoWindow: InfoWindow(title: "WIP",),
            icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueViolet),
          );
        }

        else if(_AlluserDetails[i].notification_details.status == "PENG_TRNS_IM"){
          MarkerId markerId = MarkerId("markers"+i.toString());
          markers[markerId] = Marker(
            markerId: markerId,
            position: LatLng(_AlluserDetails[i].notification_details.location?.coordinates[1]?.toDouble()??0.0,_AlluserDetails[i].notification_details.location?.coordinates[0]?.toDouble()??0.0),

            infoWindow: InfoWindow(title: "WIP",),
            icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueViolet),
          );
        }

        else if(_AlluserDetails[i].notification_details.status == "PENG_SPR_REQ"){
          MarkerId markerId = MarkerId("markers"+i.toString());
          markers[markerId] = Marker(
            markerId: markerId,
            position: LatLng(_AlluserDetails[i].notification_details.location?.coordinates[1]?.toDouble()??0.0,_AlluserDetails[i].notification_details.location?.coordinates[0]?.toDouble()??0.0),

            infoWindow: InfoWindow(title: "WIP",),
            icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueViolet),
          );
        }

        else if(_AlluserDetails[i].notification_details.status == "RIM_NEW"){
          MarkerId markerId = MarkerId("markers"+i.toString());
          markers[markerId] = Marker(
            markerId: markerId,
            position: LatLng(_AlluserDetails[i].notification_details.location?.coordinates[1]?.toDouble()??0.0,_AlluserDetails[i].notification_details.location?.coordinates[0]?.toDouble()??0.0),

            infoWindow: InfoWindow(title: "NEW",),
            icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueBlue),
          );
        }

        else if(_AlluserDetails[i].notification_details.status == "RIM_ACCEPT"){
          MarkerId markerId = MarkerId("markers"+i.toString());
          markers[markerId] = Marker(
            markerId: markerId,
            position: LatLng(_AlluserDetails[i].notification_details.location?.coordinates[1]?.toDouble()??0.0,_AlluserDetails[i].notification_details.location?.coordinates[0]?.toDouble()??0.0),

            infoWindow: InfoWindow(title: "WIP",),
            icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueViolet),
          );
        }

        else if(_AlluserDetails[i].notification_details.status == "RIM_RESOLVE"){
          MarkerId markerId = MarkerId("markers"+i.toString());
          markers[markerId] = Marker(
            markerId: markerId,
            position: LatLng(_AlluserDetails[i].notification_details.location?.coordinates[1]?.toDouble()??0.0,_AlluserDetails[i].notification_details.location?.coordinates[0]?.toDouble()??0.0),

            infoWindow: InfoWindow(title: "RESOLVED",),
            icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueGreen),
          );
        }

        else if(_AlluserDetails[i].notification_details.status == "RIM_RESOLVED"){
          MarkerId markerId = MarkerId("markers"+i.toString());
          markers[markerId] = Marker(
            markerId: markerId,
            position: LatLng(_AlluserDetails[i].notification_details.location?.coordinates[1]?.toDouble()??0.0,_AlluserDetails[i].notification_details.location?.coordinates[0]?.toDouble()??0.0),

            infoWindow: InfoWindow(title: "RESOLVED",),
            icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueGreen),
          );
        }

        else if(_AlluserDetails[i].notification_details.status == "RIM_UOB"){
          MarkerId markerId = MarkerId("markers"+i.toString());
          markers[markerId] = Marker(
            markerId: markerId,
            position: LatLng(_AlluserDetails[i].notification_details.location?.coordinates[1]?.toDouble()??0.0,_AlluserDetails[i].notification_details.location?.coordinates[0]?.toDouble()??0.0),

            infoWindow: InfoWindow(title: "Under Observation",),
            icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueMagenta),
          );
        }


        else if(_AlluserDetails[i].notification_details.status == "RIM_UNDER_OBSERVATION"){
          MarkerId markerId = MarkerId("markers"+i.toString());
          markers[markerId] = Marker(
            markerId: markerId,
            position: LatLng(_AlluserDetails[i].notification_details.location?.coordinates[1]?.toDouble()??0.0,_AlluserDetails[i].notification_details.location?.coordinates[0]?.toDouble()??0.0),

            infoWindow: InfoWindow(title: "Under Observation",),
            icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueMagenta),
          );
        }



        else if(_AlluserDetails[i].notification_details.status == "RIM_PA"){
          MarkerId markerId = MarkerId("markers"+i.toString());
          markers[markerId] = Marker(
            markerId: markerId,
            position: LatLng(_AlluserDetails[i].notification_details.location?.coordinates[1]?.toDouble()??0.0,_AlluserDetails[i].notification_details.location?.coordinates[0]?.toDouble()??0.0),

            infoWindow: InfoWindow(title: "WIP",),
            icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueViolet),
          );
        }


        else if(_AlluserDetails[i].notification_details.status == "RIM_PART_AUTH"){
          MarkerId markerId = MarkerId("markers"+i.toString());
          markers[markerId] = Marker(
            markerId: markerId,
            position: LatLng(_AlluserDetails[i].notification_details.location?.coordinates[1]?.toDouble()??0.0,_AlluserDetails[i].notification_details.location?.coordinates[0]?.toDouble()??0.0),

            infoWindow: InfoWindow(title: "WIP",),
            icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueViolet),
          );
        }



        else if(_AlluserDetails[i].notification_details.status == "RIM_WIP"){
          MarkerId markerId = MarkerId("markers"+i.toString());
          markers[markerId] = Marker(
            markerId: markerId,
            position: LatLng(_AlluserDetails[i].notification_details.location?.coordinates[1]?.toDouble()??0.0,_AlluserDetails[i].notification_details.location?.coordinates[0]?.toDouble()??0.0),

            infoWindow: InfoWindow(title: "WIP",),
            icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueViolet),
          );
        }
        else if(_AlluserDetails[i].notification_details.status == "RIM_PDC"){
          MarkerId markerId = MarkerId("markers"+i.toString());
          markers[markerId] = Marker(
            markerId: markerId,
            position: LatLng(_AlluserDetails[i].notification_details.location?.coordinates[1]?.toDouble()??0.0,_AlluserDetails[i].notification_details.location?.coordinates[0]?.toDouble()??0.0),

            infoWindow: InfoWindow(title: "On HOLD",),
            icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueRed),
          );
        }
        else if(_AlluserDetails[i].notification_details.status == "RIM_TRNSIM"){
          MarkerId markerId = MarkerId("markers"+i.toString());
          markers[markerId] = Marker(
            markerId: markerId,
            position: LatLng(_AlluserDetails[i].notification_details.location?.coordinates[1]?.toDouble()??0.0,_AlluserDetails[i].notification_details.location?.coordinates[0]?.toDouble()??0.0),

            infoWindow: InfoWindow(title: "WIP",),
            icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueViolet),
          );
        }


        else if(_AlluserDetails[i].notification_details.status == "RIM_TIM"){
          MarkerId markerId = MarkerId("markers"+i.toString());
          markers[markerId] = Marker(
            markerId: markerId,
            position: LatLng(_AlluserDetails[i].notification_details.location?.coordinates[1]?.toDouble()??0.0,_AlluserDetails[i].notification_details.location?.coordinates[0]?.toDouble()??0.0),

            infoWindow: InfoWindow(title: "WIP",),
            icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueViolet),
          );
        }

        else if(_AlluserDetails[i].notification_details.status == "RIM_RTN_ENG"){
          MarkerId markerId = MarkerId("markers"+i.toString());
          markers[markerId] = Marker(
            markerId: markerId,
            position: LatLng(_AlluserDetails[i].notification_details.location?.coordinates[1]?.toDouble()??0.0,_AlluserDetails[i].notification_details.location?.coordinates[0]?.toDouble()??0.0),

            infoWindow: InfoWindow(title: "WIP",),
            icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueViolet),
          );
        }



        else if(_AlluserDetails[i].notification_details.status == "RIM_TOE"){
          MarkerId markerId = MarkerId("markers"+i.toString());
          markers[markerId] = Marker(
            markerId: markerId,
            position: LatLng(_AlluserDetails[i].notification_details.location?.coordinates[1]?.toDouble()??0.0,_AlluserDetails[i].notification_details.location?.coordinates[0]?.toDouble()??0.0),

            infoWindow: InfoWindow(title: "WIP",),
            icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueViolet),
          );
        }


        else if(_AlluserDetails[i].notification_details.status == "RIM_TENG"){
          MarkerId markerId = MarkerId("markers"+i.toString());
          markers[markerId] = Marker(
            markerId: markerId,
            position: LatLng(_AlluserDetails[i].notification_details.location?.coordinates[1]?.toDouble()??0.0,_AlluserDetails[i].notification_details.location?.coordinates[0]?.toDouble()??0.0),

            infoWindow: InfoWindow(title: "WIP",),
            icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueViolet),
          );
        }


        else if(_AlluserDetails[i].notification_details.status == "RIM_FAULTPR"){
          MarkerId markerId = MarkerId("markers"+i.toString());
          markers[markerId] = Marker(
            markerId: markerId,
            position: LatLng(_AlluserDetails[i].notification_details.location?.coordinates[1]?.toDouble()??0.0,_AlluserDetails[i].notification_details.location?.coordinates[0]?.toDouble()??0.0),

            infoWindow: InfoWindow(title: "WIP",),
            icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueViolet),
          );
        }

        else if(_AlluserDetails[i].notification_details.status == "RIM_FPR"){
          MarkerId markerId = MarkerId("markers"+i.toString());
          markers[markerId] = Marker(
            markerId: markerId,
            position: LatLng(_AlluserDetails[i].notification_details.location?.coordinates[1]?.toDouble()??0.0,_AlluserDetails[i].notification_details.location?.coordinates[0]?.toDouble()??0.0),

            infoWindow: InfoWindow(title: "WIP",),
            icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueViolet),
          );
        }
        else if(_AlluserDetails[i].notification_details.status == "RIM_STD_REQ"){
          MarkerId markerId = MarkerId("markers"+i.toString());
          markers[markerId] = Marker(
            markerId: markerId,
            position: LatLng(_AlluserDetails[i].notification_details.location?.coordinates[1]?.toDouble()??0.0,_AlluserDetails[i].notification_details.location?.coordinates[0]?.toDouble()??0.0),

            infoWindow: InfoWindow(title: "WIP",),
            icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueViolet),
          );
        }
        else if(_AlluserDetails[i].notification_details.status == "RIM_FPR_STD_REQ"){
          MarkerId markerId = MarkerId("markers"+i.toString());
          markers[markerId] = Marker(
            markerId: markerId,
            position: LatLng(_AlluserDetails[i].notification_details.location?.coordinates[1]?.toDouble()??0.0,_AlluserDetails[i].notification_details.location?.coordinates[0]?.toDouble()??0.0),

            infoWindow: InfoWindow(title: "WIP",),
            icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueViolet),
          );
        }

        else if(_AlluserDetails[i].notification_details.status == "RIM_FAULTPRST"){
          MarkerId markerId = MarkerId("markers"+i.toString());
          markers[markerId] = Marker(
            markerId: markerId,
            position: LatLng(_AlluserDetails[i].notification_details.location?.coordinates[1]?.toDouble()??0.0,_AlluserDetails[i].notification_details.location?.coordinates[0]?.toDouble()??0.0),

            infoWindow: InfoWindow(title: "WIP",),
            icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueViolet),
          );
        }


        else if(_AlluserDetails[i].notification_details.status == "RIM_FPR_SR"){
          MarkerId markerId = MarkerId("markers"+i.toString());
          markers[markerId] = Marker(
            markerId: markerId,
            position: LatLng(_AlluserDetails[i].notification_details.location?.coordinates[1]?.toDouble()??0.0,_AlluserDetails[i].notification_details.location?.coordinates[0]?.toDouble()??0.0),

            infoWindow: InfoWindow(title: "WIP",),
            icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueViolet),
          );
        }



        else if(_AlluserDetails[i].notification_details.status == "STR_NEW"){
          MarkerId markerId = MarkerId("markers"+i.toString());
          markers[markerId] = Marker(
            markerId: markerId,
            position: LatLng(_AlluserDetails[i].notification_details.location?.coordinates[1]?.toDouble()??0.0,_AlluserDetails[i].notification_details.location?.coordinates[0]?.toDouble()??0.0),

            infoWindow: InfoWindow(title: "WIP",),
            icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueViolet),
          );
        }


        else if(_AlluserDetails[i].notification_details.status == "STR_WIP"){
          MarkerId markerId = MarkerId("markers"+i.toString());
          markers[markerId] = Marker(
            markerId: markerId,
            position: LatLng(_AlluserDetails[i].notification_details.location?.coordinates[1]?.toDouble()??0.0,_AlluserDetails[i].notification_details.location?.coordinates[0]?.toDouble()??0.0),

            infoWindow: InfoWindow(title: "WIP",),
            icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueViolet),
          );
        }

        else if(_AlluserDetails[i].notification_details.status == "STR_UP"){
          MarkerId markerId = MarkerId("markers"+i.toString());
          markers[markerId] = Marker(
            markerId: markerId,
            position: LatLng(_AlluserDetails[i].notification_details.location?.coordinates[1]?.toDouble()??0.0,_AlluserDetails[i].notification_details.location?.coordinates[0]?.toDouble()??0.0),

            infoWindow: InfoWindow(title: "WIP",),
            icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueViolet),
          );
        }

        else if(_AlluserDetails[i].notification_details.status == "STR_TBD"){
          MarkerId markerId = MarkerId("markers"+i.toString());
          markers[markerId] = Marker(
            markerId: markerId,
            position: LatLng(_AlluserDetails[i].notification_details.location?.coordinates[1]?.toDouble()??0.0,_AlluserDetails[i].notification_details.location?.coordinates[0]?.toDouble()??0.0),

            infoWindow: InfoWindow(title: "WIP",),
            icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueViolet),
          );
        }

        else if(_AlluserDetails[i].notification_details.status == "STR_PDC"){
          MarkerId markerId = MarkerId("markers"+i.toString());
          markers[markerId] = Marker(
            markerId: markerId,
            position: LatLng(_AlluserDetails[i].notification_details.location?.coordinates[1]?.toDouble()??0.0,_AlluserDetails[i].notification_details.location?.coordinates[0]?.toDouble()??0.0),

            infoWindow: InfoWindow(title: "On Hold",),
            icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueViolet),
          );
        }

        else if(_AlluserDetails[i].notification_details.status == "STR_DEL_ENR"){
          MarkerId markerId = MarkerId("markers"+i.toString());
          markers[markerId] = Marker(
            markerId: markerId,
            position: LatLng(_AlluserDetails[i].notification_details.location?.coordinates[1]?.toDouble()??0.0,_AlluserDetails[i].notification_details.location?.coordinates[0]?.toDouble()??0.0),

            infoWindow: InfoWindow(title: "Resolved",),
            icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueViolet),
          );
        }

        else if(_AlluserDetails[i].notification_details.status == "STR_DEL_ER"){
          MarkerId markerId = MarkerId("markers"+i.toString());
          markers[markerId] = Marker(
            markerId: markerId,
            position: LatLng(_AlluserDetails[i].notification_details.location?.coordinates[1]?.toDouble()??0.0,_AlluserDetails[i].notification_details.location?.coordinates[0]?.toDouble()??0.0),

            infoWindow: InfoWindow(title: "WIP",),
            icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueViolet),
          );
        }

        else if(_AlluserDetails[i].notification_details.status == "STR_RESOLVED"){
          MarkerId markerId = MarkerId("markers"+i.toString());
          markers[markerId] = Marker(
            markerId: markerId,
            position: LatLng(_AlluserDetails[i].notification_details.location?.coordinates[1]?.toDouble()??0.0,_AlluserDetails[i].notification_details.location?.coordinates[0]?.toDouble()??0.0),

            infoWindow: InfoWindow(title: "Resolved",),
            icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueViolet),
          );
        }

        else if(_AlluserDetails[i].notification_details.status == "STR_HOVR_TRC_RPR"){
          MarkerId markerId = MarkerId("markers"+i.toString());
          markers[markerId] = Marker(
            markerId: markerId,
            position: LatLng(_AlluserDetails[i].notification_details.location?.coordinates[1]?.toDouble()??0.0,_AlluserDetails[i].notification_details.location?.coordinates[0]?.toDouble()??0.0),

            infoWindow: InfoWindow(title: "WIP",),
            icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueViolet),
          );
        }

        else if(_AlluserDetails[i].notification_details.status == "STR_HOVR_PTR_RPR"){
          MarkerId markerId = MarkerId("markers"+i.toString());
          markers[markerId] = Marker(
            markerId: markerId,
            position: LatLng(_AlluserDetails[i].notification_details.location?.coordinates[1]?.toDouble()??0.0,_AlluserDetails[i].notification_details.location?.coordinates[0]?.toDouble()??0.0),

            infoWindow: InfoWindow(title: "WIP",),
            icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueViolet),
          );
        }

        else if(_AlluserDetails[i].notification_details.status == "STR_PKUP"){
          MarkerId markerId = MarkerId("markers"+i.toString());
          markers[markerId] = Marker(
            markerId: markerId,
            position: LatLng(_AlluserDetails[i].notification_details.location?.coordinates[1]?.toDouble()??0.0,_AlluserDetails[i].notification_details.location?.coordinates[0]?.toDouble()??0.0),

            infoWindow: InfoWindow(title: "WIP",),
            icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueViolet),
          );
        }

        else if(_AlluserDetails[i].notification_details.status == "STR_SPR_BP"){
          MarkerId markerId = MarkerId("markers"+i.toString());
          markers[markerId] = Marker(
            markerId: markerId,
            position: LatLng(_AlluserDetails[i].notification_details.location?.coordinates[1]?.toDouble()??0.0,_AlluserDetails[i].notification_details.location?.coordinates[0]?.toDouble()??0.0),

            infoWindow: InfoWindow(title: "WIP",),
            icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueViolet),
          );
        }

        else if(_AlluserDetails[i].notification_details.status == "STR_SPR_DSPTH"){
          MarkerId markerId = MarkerId("markers"+i.toString());
          markers[markerId] = Marker(
            markerId: markerId,
            position: LatLng(_AlluserDetails[i].notification_details.location?.coordinates[1]?.toDouble()??0.0,_AlluserDetails[i].notification_details.location?.coordinates[0]?.toDouble()??0.0),

            infoWindow: InfoWindow(title: "WIP",),
            icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueViolet),
          );
        }
        else if(_AlluserDetails[i].notification_details.status == "STR_SPR_DLVR-R"){
          MarkerId markerId = MarkerId("markers"+i.toString());
          markers[markerId] = Marker(
            markerId: markerId,
            position: LatLng(_AlluserDetails[i].notification_details.location?.coordinates[1]?.toDouble()??0.0,_AlluserDetails[i].notification_details.location?.coordinates[0]?.toDouble()??0.0),

            infoWindow: InfoWindow(title: "RESOLVED",),
            icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueGreen),
          );
        }
        else if(_AlluserDetails[i].notification_details.status == "STR_SPR_DSPTH-ER"){
          MarkerId markerId = MarkerId("markers"+i.toString());
          markers[markerId] = Marker(
            markerId: markerId,
            position: LatLng(_AlluserDetails[i].notification_details.location?.coordinates[1]?.toDouble()??0.0,_AlluserDetails[i].notification_details.location?.coordinates[0]?.toDouble()??0.0),

            infoWindow: InfoWindow(title: "WIP",),
            icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueViolet),
          );
        }
        else if(_AlluserDetails[i].notification_details.status == "STR_STDBY_DSPTH"){
          MarkerId markerId = MarkerId("markers"+i.toString());
          markers[markerId] = Marker(
            markerId: markerId,
            position: LatLng(_AlluserDetails[i].notification_details.location?.coordinates[1]?.toDouble()??0.0,_AlluserDetails[i].notification_details.location?.coordinates[0]?.toDouble()??0.0),

            infoWindow: InfoWindow(title: "WIP",),
            icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueViolet),
          );
        }
        else if(_AlluserDetails[i].notification_details.status == "STR_STDBY_DLVR"){
          MarkerId markerId = MarkerId("markers"+i.toString());
          markers[markerId] = Marker(
            markerId: markerId,
            position: LatLng(_AlluserDetails[i].notification_details.location?.coordinates[1]?.toDouble()??0.0,_AlluserDetails[i].notification_details.location?.coordinates[0]?.toDouble()??0.0),

            infoWindow: InfoWindow(title: "Standby Given",),
            icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueCyan),
          );
        }

        else if(_AlluserDetails[i].notification_details.status == "STR_STDBY_PICK"){
          MarkerId markerId = MarkerId("markers"+i.toString());
          markers[markerId] = Marker(
            markerId: markerId,
            position: LatLng(_AlluserDetails[i].notification_details.location?.coordinates[1]?.toDouble()??0.0,_AlluserDetails[i].notification_details.location?.coordinates[0]?.toDouble()??0.0),

            infoWindow: InfoWindow(title: "RESOLVED",),
            icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueGreen),
          );
        }

        else if(_AlluserDetails[i].notification_details.status == "STR_FLTY_PFR"){
          MarkerId markerId = MarkerId("markers"+i.toString());
          markers[markerId] = Marker(
            markerId: markerId,
            position: LatLng(_AlluserDetails[i].notification_details.location?.coordinates[1]?.toDouble()??0.0,_AlluserDetails[i].notification_details.location?.coordinates[0]?.toDouble()??0.0),

            infoWindow: InfoWindow(title: "WIP",),
            icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueViolet),
          );
        }

        else if(_AlluserDetails[i].notification_details.status == "STR_STDBY-DLVR_FP"){
          MarkerId markerId = MarkerId("markers"+i.toString());
          markers[markerId] = Marker(
            markerId: markerId,
            position: LatLng(_AlluserDetails[i].notification_details.location?.coordinates[1]?.toDouble()??0.0,_AlluserDetails[i].notification_details.location?.coordinates[0]?.toDouble()??0.0),

            infoWindow: InfoWindow(title: "Standby Given",),
            icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueCyan),
          );
        }

        else if(_AlluserDetails[i].notification_details.status == "STR_REP"){
          MarkerId markerId = MarkerId("markers"+i.toString());
          markers[markerId] = Marker(
            markerId: markerId,
            position: LatLng(_AlluserDetails[i].notification_details.location?.coordinates[1]?.toDouble()??0.0,_AlluserDetails[i].notification_details.location?.coordinates[0]?.toDouble()??0.0),

            infoWindow: InfoWindow(title: "WIP",),
            icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueCyan),
          );
        }
        else if(_AlluserDetails[i].notification_details.status == "STR_UREP"){
          MarkerId markerId = MarkerId("markers"+i.toString());
          markers[markerId] = Marker(
            markerId: markerId,
            position: LatLng(_AlluserDetails[i].notification_details.location?.coordinates[1]?.toDouble()??0.0,_AlluserDetails[i].notification_details.location?.coordinates[0]?.toDouble()??0.0),

            infoWindow: InfoWindow(title: "WIP",),
            icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueCyan),
          );
        }


        else if(_AlluserDetails[i].notification_details.status == "STR_REP_DLVRD_FP"){
          MarkerId markerId = MarkerId("markers"+i.toString());
          markers[markerId] = Marker(
            markerId: markerId,
            position: LatLng(_AlluserDetails[i].notification_details.location?.coordinates[1]?.toDouble()??0.0,_AlluserDetails[i].notification_details.location?.coordinates[0]?.toDouble()??0.0),

            infoWindow: InfoWindow(title: "RESOLVED",),
            icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueGreen),
          );
        }


        else if(_AlluserDetails[i].notification_details.status == "STR_REPD_NTBP"){
          MarkerId markerId = MarkerId("markers"+i.toString());
          markers[markerId] = Marker(
            markerId: markerId,
            position: LatLng(_AlluserDetails[i].notification_details.location?.coordinates[1]?.toDouble()??0.0,_AlluserDetails[i].notification_details.location?.coordinates[0]?.toDouble()??0.0),

            infoWindow: InfoWindow(title: "RESOLVED",),
            icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueGreen),
          );
        }

        else if(_AlluserDetails[i].notification_details.status == "STR_TRNS_IM"){
          MarkerId markerId = MarkerId("markers"+i.toString());
          markers[markerId] = Marker(
            markerId: markerId,
            position: LatLng(_AlluserDetails[i].notification_details.location?.coordinates[1]?.toDouble()??0.0,_AlluserDetails[i].notification_details.location?.coordinates[0]?.toDouble()??0.0),

            infoWindow: InfoWindow(title: "WIP",),
            icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueViolet),
          );
        }

        else if(_AlluserDetails[i].notification_details.status == "STR_TRNS_TRC_RPR"){
          MarkerId markerId = MarkerId("markers"+i.toString());
          markers[markerId] = Marker(
            markerId: markerId,
            position: LatLng(_AlluserDetails[i].notification_details.location?.coordinates[1]?.toDouble()??0.0,_AlluserDetails[i].notification_details.location?.coordinates[0]?.toDouble()??0.0),

            infoWindow: InfoWindow(title: "WIP",),
            icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueViolet),
          );
        }


        else if(_AlluserDetails[i].notification_details.status == "STR_TRNS_PRTNR_RPR"){
          MarkerId markerId = MarkerId("markers"+i.toString());
          markers[markerId] = Marker(
            markerId: markerId,
            position: LatLng(_AlluserDetails[i].notification_details.location?.coordinates[1]?.toDouble()??0.0,_AlluserDetails[i].notification_details.location?.coordinates[0]?.toDouble()??0.0),

            infoWindow: InfoWindow(title: "WIP",),
            icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueViolet),
          );
        }

        else if(_AlluserDetails[i].notification_details.status == "STR_FLTY_NTBP"){
          MarkerId markerId = MarkerId("markers"+i.toString());
          markers[markerId] = Marker(
            markerId: markerId,
            position: LatLng(_AlluserDetails[i].notification_details.location?.coordinates[1]?.toDouble()??0.0,_AlluserDetails[i].notification_details.location?.coordinates[0]?.toDouble()??0.0),

            infoWindow: InfoWindow(title: "WIP",),
            icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueViolet),
          );
        }


        else if(_AlluserDetails[i].notification_details.status == "IM_NEW"){
          MarkerId markerId = MarkerId("markers"+i.toString());
          markers[markerId] = Marker(
            markerId: markerId,
            position: LatLng(_AlluserDetails[i].notification_details.location?.coordinates[1]?.toDouble()??0.0,_AlluserDetails[i].notification_details.location?.coordinates[0]?.toDouble()??0.0),
            infoWindow: InfoWindow(title: "WIP",),
            icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueViolet),
          );
        }


        else if(_AlluserDetails[i].notification_details.status == "IM_WIP"){
          MarkerId markerId = MarkerId("markers"+i.toString());
          markers[markerId] = Marker(
            markerId: markerId,
            position: LatLng(_AlluserDetails[i].notification_details.location?.coordinates[1]?.toDouble()??0.0,_AlluserDetails[i].notification_details.location?.coordinates[0]?.toDouble()??0.0),

            infoWindow: InfoWindow(title: "WIP",),
            icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueViolet),
          );
        }
        else if(_AlluserDetails[i].notification_details.status == "IM_SPART_REQ"){
          MarkerId markerId = MarkerId("markers"+i.toString());
          markers[markerId] = Marker(
            markerId: markerId,
            position: LatLng(_AlluserDetails[i].notification_details.location?.coordinates[1]?.toDouble()??0.0,_AlluserDetails[i].notification_details.location?.coordinates[0]?.toDouble()??0.0),

            infoWindow: InfoWindow(title: "WIP",),
            icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueViolet),
          );
        }
        else if(_AlluserDetails[i].notification_details.status == "IM_TRANSFER_RIM"){
          MarkerId markerId = MarkerId("markers"+i.toString());
          markers[markerId] = Marker(
            markerId: markerId,
            position: LatLng(_AlluserDetails[i].notification_details.location?.coordinates[1]?.toDouble()??0.0,_AlluserDetails[i].notification_details.location?.coordinates[0]?.toDouble()??0.0),

            infoWindow: InfoWindow(title: "WIP",),
            icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueViolet),
          );
        }

        else if(_AlluserDetails[i].notification_details.status == "IM_ASGND_PRTNR"){
          MarkerId markerId = MarkerId("markers"+i.toString());
          markers[markerId] = Marker(
            markerId: markerId,
            position: LatLng(_AlluserDetails[i].notification_details.location?.coordinates[1]?.toDouble()??0.0,_AlluserDetails[i].notification_details.location?.coordinates[0]?.toDouble()??0.0),

            infoWindow: InfoWindow(title: "WIP",),
            icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueViolet),
          );
        }

        else if(_AlluserDetails[i].notification_details.status == "IM_ASGND_ENGNR"){
          MarkerId markerId = MarkerId("markers"+i.toString());
          markers[markerId] = Marker(
            markerId: markerId,
            position: LatLng(_AlluserDetails[i].notification_details.location?.coordinates[1]?.toDouble()??0.0,_AlluserDetails[i].notification_details.location?.coordinates[0]?.toDouble()??0.0),

            infoWindow: InfoWindow(title: "WIP",),
            icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueViolet),
          );
        }


        else if(_AlluserDetails[i].notification_details.status == "IM_PDC"){
          MarkerId markerId = MarkerId("markers"+i.toString());
          markers[markerId] = Marker(
            markerId: markerId,
            position: LatLng(_AlluserDetails[i].notification_details.location?.coordinates[1]?.toDouble()??0.0,_AlluserDetails[i].notification_details.location?.coordinates[0]?.toDouble()??0.0),

            infoWindow: InfoWindow(title: "On Hold",),
            icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueRed),
          );
        }


        else if(_AlluserDetails[i].notification_details.status == "IM_OBS"){
          MarkerId markerId = MarkerId("markers"+i.toString());
          markers[markerId] = Marker(
            markerId: markerId,
            position: LatLng(_AlluserDetails[i].notification_details.location?.coordinates[1]?.toDouble()??0.0,_AlluserDetails[i].notification_details.location?.coordinates[0]?.toDouble()??0.0),

            infoWindow: InfoWindow(title: "Under Observation",),
            icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueRed),
          );
        }


        else if(_AlluserDetails[i].notification_details.status == "IM_RESOLVED"){
          MarkerId markerId = MarkerId("markers"+i.toString());
          markers[markerId] = Marker(
            markerId: markerId,
            position: LatLng(_AlluserDetails[i].notification_details.location?.coordinates[1]?.toDouble()??0.0,_AlluserDetails[i].notification_details.location?.coordinates[0]?.toDouble()??0.0),

            infoWindow: InfoWindow(title: "RESOLVED",),
            icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueGreen),
          );
        }

        else if(_AlluserDetails[i].notification_details.status == "IM_WPS"){
          MarkerId markerId = MarkerId("markers"+i.toString());
          markers[markerId] = Marker(
            markerId: markerId,
            position: LatLng(_AlluserDetails[i].notification_details.location?.coordinates[1]?.toDouble()??0.0,_AlluserDetails[i].notification_details.location?.coordinates[0]?.toDouble()??0.0),

            infoWindow: InfoWindow(title: "WIP",),
            icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueGreen),
          );
        }






        else if(_AlluserDetails[i].notification_details.status == "PART_NEW"){
          MarkerId markerId = MarkerId("markers"+i.toString());
          markers[markerId] = Marker(
            markerId: markerId,
            position: LatLng(_AlluserDetails[i].notification_details.location?.coordinates[1]?.toDouble()??0.0,_AlluserDetails[i].notification_details.location?.coordinates[0]?.toDouble()??0.0),

            infoWindow: InfoWindow(title: "WIP",),
            icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueViolet),
          );
        }
        else if(_AlluserDetails[i].notification_details.status == "PART_ACCEPT"){
          MarkerId markerId = MarkerId("markers"+i.toString());
          markers[markerId] = Marker(
            markerId: markerId,
            position: LatLng(_AlluserDetails[i].notification_details.location?.coordinates[1]?.toDouble()??0.0,_AlluserDetails[i].notification_details.location?.coordinates[0]?.toDouble()??0.0),

            infoWindow: InfoWindow(title: "WIP",),
            icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueViolet),
          );
        }

        else if(_AlluserDetails[i].notification_details.status == "PART_REJECT"){
          MarkerId markerId = MarkerId("markers"+i.toString());
          markers[markerId] = Marker(
            markerId: markerId,
            position: LatLng(_AlluserDetails[i].notification_details.location?.coordinates[1]?.toDouble()??0.0,_AlluserDetails[i].notification_details.location?.coordinates[0]?.toDouble()??0.0),

            infoWindow: InfoWindow(title: "WIP",),
            icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueViolet),
          );
        }

        else if(_AlluserDetails[i].notification_details.status == "PART_WIP"){
          MarkerId markerId = MarkerId("markers"+i.toString());
          markers[markerId] = Marker(
            markerId: markerId,
            position: LatLng(_AlluserDetails[i].notification_details.location?.coordinates[1]?.toDouble()??0.0,_AlluserDetails[i].notification_details.location?.coordinates[0]?.toDouble()??0.0),

            infoWindow: InfoWindow(title: "WIP",),
            icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueViolet),
          );
        }
        else if(_AlluserDetails[i].notification_details.status == "PART_SPR_REQ"){
          MarkerId markerId = MarkerId("markers"+i.toString());
          markers[markerId] = Marker(
            markerId: markerId,
            position: LatLng(_AlluserDetails[i].notification_details.location?.coordinates[1]?.toDouble()??0.0,_AlluserDetails[i].notification_details.location?.coordinates[0]?.toDouble()??0.0),

            infoWindow: InfoWindow(title: "WIP",),
            icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueViolet),
          );
        }

        else if(_AlluserDetails[i].notification_details.status == "PART_ASGN_PENGNR"){
          MarkerId markerId = MarkerId("markers"+i.toString());
          markers[markerId] = Marker(
            markerId: markerId,
            position: LatLng(_AlluserDetails[i].notification_details.location?.coordinates[1]?.toDouble()??0.0,_AlluserDetails[i].notification_details.location?.coordinates[0]?.toDouble()??0.0),

            infoWindow: InfoWindow(title: "WIP",),
            icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueViolet),
          );
        }
        else if(_AlluserDetails[i].notification_details.status == "PART_ASGN_PRTNR"){
          MarkerId markerId = MarkerId("markers"+i.toString());
          markers[markerId] = Marker(
            markerId: markerId,
            position: LatLng(_AlluserDetails[i].notification_details.location?.coordinates[1]?.toDouble()??0.0,_AlluserDetails[i].notification_details.location?.coordinates[0]?.toDouble()??0.0),

            infoWindow: InfoWindow(title: "WIP",),
            icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueViolet),
          );
        }

        else if(_AlluserDetails[i].notification_details.status == "PART_ASGN_ENGNR"){
          MarkerId markerId = MarkerId("markers"+i.toString());
          markers[markerId] = Marker(
            markerId: markerId,
            position: LatLng(_AlluserDetails[i].notification_details.location?.coordinates[1]?.toDouble()??0.0,_AlluserDetails[i].notification_details.location?.coordinates[0]?.toDouble()??0.0),

            infoWindow: InfoWindow(title: "WIP",),
            icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueViolet),
          );
        }


        else if(_AlluserDetails[i].notification_details.status == "PART_PDC"){
          MarkerId markerId = MarkerId("markers"+i.toString());
          markers[markerId] = Marker(
            markerId: markerId,
            position: LatLng(_AlluserDetails[i].notification_details.location?.coordinates[1]?.toDouble()??0.0,_AlluserDetails[i].notification_details.location?.coordinates[0]?.toDouble()??0.0),

            infoWindow: InfoWindow(title: "On Hold",),
            icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueRed),
          );
        }

        else if(_AlluserDetails[i].notification_details.status == "PART_RESOLVED"){
          MarkerId markerId = MarkerId("markers"+i.toString());
          markers[markerId] = Marker(
            markerId: markerId,
            position: LatLng(_AlluserDetails[i].notification_details.location?.coordinates[1]?.toDouble()??0.0,_AlluserDetails[i].notification_details.location?.coordinates[0]?.toDouble()??0.0),

            infoWindow: InfoWindow(title: "RESOLVED",),
            icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueGreen),
          );
        }

               else if(_AlluserDetails[i].notification_details.status == "PART_WPS"){
          MarkerId markerId = MarkerId("markers"+i.toString());
          markers[markerId] = Marker(
            markerId: markerId,
            position: LatLng(_AlluserDetails[i].notification_details.location?.coordinates[1]?.toDouble()??0.0,_AlluserDetails[i].notification_details.location?.coordinates[0]?.toDouble()??0.0),

            infoWindow: InfoWindow(title: "WIP",),
            icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueGreen),
          );
        }


        else if(_AlluserDetails[i].notification_details.status == "PART_UOB"){
          MarkerId markerId = MarkerId("markers"+i.toString());
          markers[markerId] = Marker(
            markerId: markerId,
            position: LatLng(_AlluserDetails[i].notification_details.location?.coordinates[1]?.toDouble()??0.0,_AlluserDetails[i].notification_details.location?.coordinates[0]?.toDouble()??0.0),

            infoWindow: InfoWindow(title: "Under Observation",),
            icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueGreen),
          );
        }



        else if(_AlluserDetails[i].notification_details.status == "TRC_NEW"){
          MarkerId markerId = MarkerId("markers"+i.toString());
          markers[markerId] = Marker(
            markerId: markerId,
            position: LatLng(_AlluserDetails[i].notification_details?.location?.coordinates[1].toDouble()??0.0,_AlluserDetails[i].notification_details.location?.coordinates[0]?.toDouble()??0.0),

            infoWindow: InfoWindow(title: "WIP",),
            icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueViolet),
          );
        }

        else if(_AlluserDetails[i].notification_details.status == "TRC_WIP"){
          MarkerId markerId = MarkerId("markers"+i.toString());
          markers[markerId] = Marker(
            markerId: markerId,
            position: LatLng(_AlluserDetails[i].notification_details.location?.coordinates[1]?.toDouble()??0.0,_AlluserDetails[i].notification_details.location?.coordinates[0]?.toDouble()??0.0),

            infoWindow: InfoWindow(title: "WIP",),
            icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueViolet),
          );
        }


        else if(_AlluserDetails[i].notification_details.status == "TRC_REPAIRED"){
          MarkerId markerId = MarkerId("markers"+i.toString());
          markers[markerId] = Marker(
            markerId: markerId,
            position: LatLng(_AlluserDetails[i].notification_details.location?.coordinates[1]?.toDouble()??0.0,_AlluserDetails[i].notification_details.location?.coordinates[0]?.toDouble()??0.0),

            infoWindow: InfoWindow(title: "WIP",),
            icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueViolet),
          );
        }

        else if(_AlluserDetails[i].notification_details.status == "TRC_NOT_REPAIRABLE"){
          MarkerId markerId = MarkerId("markers"+i.toString());
          markers[markerId] = Marker(
            markerId: markerId,
            position: LatLng(_AlluserDetails[i].notification_details.location?.coordinates[1]?.toDouble()??0.0,_AlluserDetails[i].notification_details.location?.coordinates[0]?.toDouble()??0.0),

            infoWindow: InfoWindow(title: "WIP",),
            icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueViolet),
          );
        }


      }


      return list;

    } else {
      // print("== ff" + ModelAll.toString());
      print("==response error" + response.toString());
    }
    setState(() {

    });

  }





}


