 class User{
  final int id;
  String name;
  String email;
  String phoneNumber;

  User({this.id, this.name, this.email,this.phoneNumber});

  @override
  String toString() {
    return 'User{id: $id, name: $name, email: $email, phoneNumber: $phoneNumber}';
  }

  factory User.fromJson(Map<String, dynamic> json) {
    return User(
        id: json['_id'],
        name: json['name'].toString(),
        email: json['email'].toString(),
      phoneNumber: json['phoneNumber'].toString()
    );
  }
}

