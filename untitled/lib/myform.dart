import 'dart:convert';
import 'dart:io';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:customer/Constant.dart';
import 'package:customer/CustomerInfo.dart';
import 'package:customer/Dashboard.dart';
import 'package:customer/DomainSelection.dart';
import 'package:customer/ForgotPassword.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:geolocator/geolocator.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'DomainSelectionLogin.dart';
import 'User.dart';
import 'form_model.dart';
import 'package:intl/intl.dart';
import 'my_form_text_field.dart';

  Position position;
  class MyForm extends StatefulWidget {

  @override
  State<StatefulWidget> createState() {
    return MyFormState();
  }

}
class MyFormState extends State<MyForm> {
  var isLogged=false;
  String token1;
  String _message = '';
  // email RegExp
  final _emailRegExp = RegExp(
      r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+");
  // uniquely identifies a Form
  final _formKey = GlobalKey<FormState>();
  // holds the form data for access
  final model = FormModel();



    String validatePassword(String value) {
    Pattern pattern =
        r'^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[!@#\$&*~]).{8,}$';
    RegExp regex = new RegExp(pattern);
    bool passValid = RegExp("^(?=.{8,32}\$)(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])(?=.*[!@#\$%^&*(),.?:{}|<>]).*").hasMatch(value);
    print(value);
    if (value.isEmpty) {
      return 'Please enter password';
    } else {
      if (!regex.hasMatch(value))
        return 'Enter valid password';
      else
        return null;
    }
  }

    @override
    Widget build(BuildContext context) {
    Color color1 = _colorFromHex("#00ABC5");
    return FlutterEasyLoading(
      child:MaterialApp(
      home: SingleChildScrollView(
          padding: EdgeInsets.all(20.0),

          child: Container(


            child: Column(

              children: <Widget>[
                Form(
                  key: _formKey,
                  child: Column(
                    children: <Widget>[
                      TextFormField(

                        decoration: InputDecoration(
                          labelText: "EmailAddress",
                          hintText: "me@abc.com",
                        ),
                        maxLength: 80,
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'Please enter an email address';
                          } else if (!_emailRegExp.hasMatch(value)) {
                            return 'Invalid email address!';
                          }
                          return null;
                        },
                        onSaved: (value) {
                          model.emailAddress = value;
                        },
                      ),
                      MyFormTextField(
                        isObscure: true,
                        decoration: InputDecoration(
                        labelText: "Password", hintText: "my password"),
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'Please enter a password';
                          }

                          // else if(passValid){
                          //
                          // }
                          return null;
                        },
                        onSaved: (value) {
                          model.password = value;
                        },
                      ),
                      Container(
                          child: Row(
                            children: <Widget>[

                              FlatButton(
                                textColor: color1,
                                child: Text(
                                  'Forgot Password',
                                ),
                                onPressed: () {
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(builder: (context) => ForgotPassword()),
                                  );
                                  //signup screen
                                },
                              )
                            ],
                            mainAxisAlignment: MainAxisAlignment.center,
                          )),
                      // FormSubmitButton(
                  Container(
                  height: 50,
                  width: 500,
                 padding: EdgeInsets.fromLTRB(10, 10, 10, 0),
                 margin: EdgeInsets.fromLTRB(10, 10, 10, 0),
                 child: RaisedButton(
                 textColor: Colors.white,
                 color: color1,
                 child: Text('SignIn'),
                onPressed: () async {
                if (_formKey.currentState.validate()) {
                _formKey.currentState.save();

                  signIn(model.emailAddress.toString(), model.password.toString(),"cgXF8E7XTlG7bOEkuwBAsE:APA91bGSKk8ywczXx1GYVfYgX1Ijyk5pOJF6vEeDuJ374QX...",context);




                   }
             },
         )),

                    ],
                  ),
                )
              ],
            ),
          )),
    ),);
  }

    @override
    void initState() {
    super.initState();
    EasyLoading.dismiss(animation: false);
    final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
    _firebaseMessaging.getToken().then((token) async {
      token1 = token.toString();
      print("token1"+ token1);
      getMessage();
      // do whatever you want with the token here
    });
  }

    void getMessage(){
    final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
    _firebaseMessaging.configure(
        onMessage: (Map<String, dynamic> message) async {
          print('==on message $message');
           setState(() => _message = message["notification"]["title"]);
        },
        // onBackgroundMessage: myBackgroundMessageHandler,
        onResume: (Map<String, dynamic> message) async {
          print('==on resume $message');
          // setState(() => _message = message["notification"]["title"]);
        }, onLaunch: (Map<String, dynamic> message) async {
         print('==on launch $message');
      // setState(() => _message = message["notification"]["title"]);
    });
  }

    SharedPreferences sharedPreferences ;
    var nameList;
    var name1;
    Future<User> signIn(String email, pass,token,BuildContext context) async {

    EasyLoading.show(status:"Loading");
    print("==email :"+email);
    print("==password : "+pass);
    // print("==token"+token);
    position = await Geolocator().getCurrentPosition(desiredAccuracy: LocationAccuracy.high);
    double lat=position.latitude;
    double lng=position.longitude;


    var parts = email.split('@');
    var prefix = parts[1].trim();
    print("==prefix"+prefix);


    HttpClient client = new HttpClient();
    client.badCertificateCallback = ((X509Certificate cert, String host, int port) => true);

    // http://192.168.49.1:8080/api/ticket/index
      var url=Constants.base_url+"api/cust/user/login";
      var request =  http.MultipartRequest('POST',Uri.parse(url));
      print("request"+request.toString());
      request.fields['email']=email;
      request.fields['password']=pass;
      request.fields['firebase_token']=token;
      request.fields['domain_name']=prefix;
    // request.fields['curr_latitude']=lat.toString();
    //   request.fields['curr_longitude']=lng.toString/();
      var response=await request.send();
      SnackBar(content: Text('Processing Data'));
      if(response.statusCode == 200) {
        EasyLoading.dismiss(animation: false);
        EasyLoading.showToast("Success");
        var result = await http.Response.fromStream(response);
        var  jsonResponse = json.decode(result.body);
        sharedPreferences= await SharedPreferences.getInstance();
        sharedPreferences.setString("token", jsonResponse['token']);
        var user=jsonResponse['user'];
        var name=user['name'];
        var sentence = toBeginningOfSentenceCase(name); // This is a string
        var email=user['email'];
        var id=user['_id'];
        var phoneNumber=user['phoneNumber'];
        sharedPreferences.setString("name", sentence);
        sharedPreferences.setString("email", email);
        sharedPreferences.setString("phone", phoneNumber.toString());
        sharedPreferences.setString("userId",id.toString());
        sharedPreferences.setBool('first_time', false);
        sharedPreferences.setString('domain_name', prefix);
        print("==user"+user.toString());
        print("==name"+name.toString());
        print("==result"+result.body);
        Navigator.pushAndRemoveUntil<dynamic>(context, MaterialPageRoute<dynamic>(builder: (BuildContext context) => Dashboard(),), (route) => false,
          //if you want to disable back feature set to false
);
        // Navigator.push(context,MaterialPageRoute(builder: (context) => Dashboard()),);
      }
      else if(response.statusCode == 400) {
        print("==comming");
        var result = await http.Response.fromStream(response);
        var  jsonResponse = json.decode(result.body);
        var message=jsonResponse['message'];
        var error=jsonResponse['error'];
        var userId=jsonResponse['userId'];
        var user=jsonResponse['user'];
        if(message=="Incorrect Password"){
          EasyLoading.showError(message);
          // EasyLoading.showError(user.toString());
        }
        else if(error=="Incorrect Credentials"){
          EasyLoading.showError(error);
        }

        // EasyLoading.showToast("Email or password is Incorrect");
         if(message=="Domain not registered")
          // {
          //   var comapny=jsonResponse['domain_list'];
          //   var customerarray=comapny['customerarray']  as List;
          //   var list=customerarray.map<CustomerInfo>((json) => CustomerInfo.fromJson(json)).toList();
          //   for(int i=0;i<list.length;i++)
          //   {
          //     name1=list[i].name.toString();
          //     // print("name"+ name);
          //     nameList=List<String>();
          //     nameList.add(name1);
          //   }
          //   if(user!=null){
          //
          //     var name=user['name'];
          //     var sentence = toBeginningOfSentenceCase(name); // This is a string
          //     var email=user['email'];
          //     var id=user['_id'];
          //     var phoneNumber=user['phoneNumber'];
          //     var firebaseToken=user['firebase_token'];
          //     var sharedPreferences    = await SharedPreferences.getInstance();
          //     sharedPreferences.setString("name", sentence);
          //     sharedPreferences.setString("email", email);
          //     sharedPreferences.setString("phone", phoneNumber.toString());
          //     sharedPreferences.setString("userid", id);
          //     sharedPreferences.setString("userId",id.toString());
          //     sharedPreferences.setBool('first_time', false);
          //     sharedPreferences.setString("response",result.body.toString());
          //     sharedPreferences.setString("token", firebaseToken);
          //
          //     Navigator.pushAndRemoveUntil<dynamic>(
          //       context,
          //       MaterialPageRoute<dynamic>(
          //         builder: (BuildContext context) => DomainSelectionLogin(),
          //       ),
          //           (route) => false,//if you want to disable back feature set to false
          //     );
          //     EasyLoading.dismiss(animation: false);
          //   }
          // }
         if(message=="Incorrect Password"){
          EasyLoading.showError(message);
          // EasyLoading.showError(user.toString());
        }
        else if (message=="Kindly complete the authentication process."){
          EasyLoading.showError(message);
        }
        else if(message=="User not Verified"){
          EasyLoading.showError(message);
        }
        else if(error=="Incorrect Credentials")
          {
            EasyLoading.showError(error);
          }
        print("==response error"+jsonResponse.toString());
      }
  }

    void getLocation() async {
    position = await Geolocator().getCurrentPosition(desiredAccuracy: LocationAccuracy.high);
         print("==location"+position.toString());
    }

    Color _colorFromHex(String hexColor) {
    final hexCode = hexColor.replaceAll('#', '');
    return Color(int.parse('FF$hexCode', radix: 16));
  }

}


