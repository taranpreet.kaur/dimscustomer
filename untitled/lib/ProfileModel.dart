 import 'User.dart';

class ProfileModel{
  User user;


  ProfileModel({this.user});

  @override
  String toString() {
    return 'ProfileModel{user: $user}';
  }

  factory ProfileModel.fromJson(Map<String, dynamic> json) {
    return ProfileModel(
        user: json['user'],

    );
  }
}

