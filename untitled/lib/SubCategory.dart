class SubCategory{

  String id;
  String CategoryName;
   String SubCategoryName;
  String TypeOfRequest;
  String Department;

  SubCategory({this.id,this.CategoryName,this.SubCategoryName,this.TypeOfRequest,this.Department});




  factory SubCategory.fromJson(Map<String, dynamic> json) {
    return SubCategory(
      id: json["_id"] as String,
      CategoryName: json["CategoryName"] as String,
      SubCategoryName: json["SubCategoryName"] as String,
      TypeOfRequest: json["TypeOfRequest"] as String,
      Department: json["Department"] as String,
    );
  }

  static Map<String, dynamic> toMap(SubCategory music) =>
      {
        '_id': music.id,
        'CategoryName':music.CategoryName,
         'SubCategoryName': music.SubCategoryName,
        'TypeOfRequest': music.TypeOfRequest,
        'Department': music.Department
      };

}
