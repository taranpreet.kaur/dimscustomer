class FormModel {
  String emailAddress;
  String password;
  String name;
  String phoneNumber;
  String companyName;
  String confirmPassword;
  FormModel({this.emailAddress, this.password,this.name,this.phoneNumber,this.companyName,this.confirmPassword});
}
