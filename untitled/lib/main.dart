import 'dart:async';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:customer/Dashboard.dart';
import 'package:customer/LoginTab.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'DomainSelectionLogin.dart';
class MyHttpOverrides extends HttpOverrides{
  @override
  HttpClient createHttpClient(SecurityContext context){
    return super.createHttpClient(context)
      ..badCertificateCallback = (X509Certificate cert, String host, int port)=> true;
  }
}
void main() {
  HttpOverrides.global = new MyHttpOverrides();


  runApp(new MaterialApp(
    home: new SplashScreen(),
    routes: <String, WidgetBuilder>{
      '/HomePage': (BuildContext context) => new Dashboard(),
      '/WelcomePage': (BuildContext context) => new LoginTab()
    },
  ));
}
class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => new _SplashScreenState();
}
class _SplashScreenState extends State<SplashScreen> {
  startTime() async {
    setState(() {
      main();
    });
    SharedPreferences prefs = await SharedPreferences.getInstance();
    bool firstTime = prefs.getBool('first_time');
    bool domain=prefs.getBool("domain");
    print("==dimain"+domain.toString());

    var _duration = new Duration(seconds: 10);
print("vinayCheck" + domain.toString());
    if(domain==true){
      return new Timer(_duration, navigationPageHome);
    }
    else if(domain==false){
      Navigator.pushAndRemoveUntil<dynamic>(
        context,
        MaterialPageRoute<dynamic>(
          builder: (BuildContext context) => DomainSelectionLogin(),
        ),
            (route) => false,//if you want to disable back feature set to false
      );
    }
    if (firstTime != null && !firstTime ) {// Not first time
      return new Timer(_duration, navigationPageHome);
    }
    else {// First time
      // prefs.setBool('first_time', false);
      return new Timer(_duration, navigationPageWel);
    }
  }

  void navigationPageHome() {
    Navigator.of(context).pushReplacementNamed('/HomePage');
  }

  void navigationPageWel() {
    Navigator.of(context).pushReplacementNamed('/WelcomePage');
  }

  @override
  void initState() {
    super.initState();
    startTime();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
          body:Container(
              height: double.infinity,
              width: double.infinity,
              child: Image.asset("assets/app.gif",
                  gaplessPlayback: true,
                  fit: BoxFit.fill
              )
          )




      ),
    );

  }
}