class IssueCategory {
String id;
  String issueCategory;

  IssueCategory({
    this.id,
    this.issueCategory,

  });

  factory IssueCategory.fromJson(Map<String, dynamic> json) {
    return IssueCategory(
      id: json["_id"] as String,
      issueCategory: json["issueCategory"] as String,

    );
  }

  static Map<String, dynamic> toMap(IssueCategory music) =>
      {
         '_id': music.id,
        'issueCategory': music.issueCategory,

      };
}