import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:customer/Constant.dart';
import 'package:customer/CustomerInfo.dart';
import 'package:customer/Dashboard.dart';
import 'package:customer/LoginTab.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';

   var list;
   var name1;
   List<String> nameList = [];
   List<String> shared_list;
   String dropdownValue;
   Future<SharedPreferences> prefs = SharedPreferences.getInstance();
   String id;
   class DomainSelection extends StatelessWidget {
    DomainSelection();
    @override
    Widget build(BuildContext context)  {
    Color color1 = _colorFromHex("#00ABC5");
    return Scaffold(
        appBar: AppBar(
          title: Text("Domain Selection",),  flexibleSpace: Image(
          image: AssetImage('assets/appbar_background.png'),
          fit: BoxFit.cover,
        ),
          backgroundColor: Colors.transparent,),
        backgroundColor: Colors.white,
        resizeToAvoidBottomInset: true,
             body:  DropdownScreen()
    );
 }
}
   var sharedPreferences;

  class DropdownScreen extends StatefulWidget {

  State createState() =>  DropdownScreenState();
  }

  class DropdownScreenState extends State<DropdownScreen> {
    @override
    Widget build(BuildContext context) {
      print("namelist in drop down"+nameList.toString());
      return  Scaffold(body: SingleChildScrollView(child: Container(
                padding: EdgeInsets.fromLTRB(20, 20, 20, 20),
                child: Column(
                  children: <Widget>[Container(

                  //
                      child: DropdownButton<String>(
                    hint: Text("Select Building type"),
                    value: dropdownValue,
                    onChanged: (String newValue) {
                      setState(() {
                        dropdownValue = newValue;
                      });
                    },
                    items: nameList.map((String value) {
                      print("namelist1313"+nameList.toString());
                      return DropdownMenuItem<String>(
                        value: value,
                        child: Row(
                          children: <Widget>[
                            SizedBox(
                              width: 10,
                            ),
                            Text(value, style: TextStyle(color: Colors.black),),
                          ],
                        ),
                      );
                    }).toList(),
                  ),

                  ),
                    Container(child: RaisedButton(
                    textColor: Colors.white,
                    color: Colors.lightBlueAccent,
                    child: Text('Next'),
                    onPressed: () async {
                       print("dropdown" + dropdownValue);
                       SharedPreferences prefs = await SharedPreferences.getInstance() ;
                       prefs.setString("companyName", dropdownValue);
                       customerUpdate(id, dropdownValue,  context);
                    },
                  )),
                  ],)

            ),),);
          }
          // print("shared_listttt"+shared_list.toString());
    @override
    void initState() {
      // TODO: implement initState
      super.initState();
      _loadCounter();
    }
      _loadCounter()  async {
      nameList.clear();
      SharedPreferences prefs = await SharedPreferences.getInstance();

      setState(() {
        list = prefs.getString('response');
        var list1=jsonDecode(list);
        var comapny=list1['companies'];
        var customerarray=comapny['customerarray']  as List;
        var list2=customerarray.map<CustomerInfo>((json) => CustomerInfo.fromJson(json)).toList();
        for(int i=0;i<list2.length;i++)
        {
          name1=list2[i].name.toString();
          var assetId1 = toBeginningOfSentenceCase(name1);

          print("name: "+ assetId1);
          nameList.add(assetId1);
          nameList.sort();

        }
        print("==namelist12"+nameList.toString());
        prefs.setStringList("list_customer", nameList);
        dropdownValue=nameList[0];
        // shared_list = prefs.getStringList('list_customer');
        // dropdownValue=shared_list[0];
      });
     id =prefs.getString("userid");
    }
    // loadCounter()  {
    //   SharedPreferences prefs =  SharedPreferences.getInstance() as SharedPreferences;
    //    setState(() {
    //     shared_list = prefs.getStringList('list_customer');
    //     print("==2: sharedpref"+shared_list.toString());
    //     dropdownValue=shared_list[0];
    //    });
    // }

  }
SharedPreferences sharedPreferences1;

  customerUpdate(String id, comapnyname,BuildContext context) async {
    sharedPreferences1= await SharedPreferences.getInstance();

    var url=Constants.base_url+"api/cust/user/setName";
    var request =  http.MultipartRequest('POST',Uri.parse(url));
    print("request"+request.toString());
    request.fields['customer_id']=id;
    request.fields['customer_name']=comapnyname;

  var response=await request.send();
  if(response.statusCode == 200) {
    var result = await http.Response.fromStream(response);
    var  jsonResponse = json.decode(result.body);
    print("==response"+jsonResponse.toString());

    // Navigator.push(context,MaterialPageRoute(builder: (context) => LoginTab()),);
    Navigator.pushAndRemoveUntil<dynamic>(
      context,
      MaterialPageRoute<dynamic>(
        builder: (BuildContext context) => LoginTab(),
      ),
          (route) => false,//if you want to disable back feature set to false
    );



  }
  else {
    print("==response error"+response.toString());
  }

  }

       Color _colorFromHex(String hexColor) {
       final hexCode = hexColor.replaceAll('#', '');
       return Color(int.parse('FF$hexCode', radix: 16));
     }