import 'package:customer/CustomerInfo.dart';

class CompanyInfo{
  final List<CustomerInfo> photos;

  CompanyInfo(
      {
        this.photos

      });


  // factory CompanyInfo.fromJson(Map<String, dynamic> parsedJson) {
  //
  //   return new CompanyInfo(
  //
  //     streets: parsedJson['customerarray'],
  //   );
  // }

  factory CompanyInfo.fromJson(List<dynamic> parsedJson) {

    List<CustomerInfo> photos = new List<CustomerInfo>();
      photos = parsedJson.map((i)=>CustomerInfo.fromJson(i)).toList();


    return new CompanyInfo(
      photos: photos,);
  }

}