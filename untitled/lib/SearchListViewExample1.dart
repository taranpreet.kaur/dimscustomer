import 'dart:async';
import 'package:connectivity/connectivity.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:customer/TicketDetails.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'Article.dart';
import 'Services.dart';
import 'package:scroll_to_index/scroll_to_index.dart';


class UserFilterDemo1 extends StatefulWidget {
  UserFilterDemo1() : super();
  final String title = "Filter List Demo";
  @override
  UserFilterDemoState createState() => UserFilterDemoState();
}

class Debouncer {
  final int milliseconds;
  VoidCallback action;
  Timer _timer;
  Debouncer({this.milliseconds});
  run(VoidCallback action) {
    if (null != _timer) {
      _timer.cancel();
    }
    _timer = Timer(Duration(milliseconds: milliseconds), action);
  }
}

class UserFilterDemoState extends State<UserFilterDemo1> {
  //
  final _debouncer = Debouncer(milliseconds: 500);
  List<Article> users = List();
  List<Article> filteredUsers = List();
  final newList = [];
  ConnectionState connectionState;
  var isLoading=false;
  SharedPreferences sharedPreferences;
  @override
  Future<void> initState() async {
    super.initState();

    controller = AutoScrollController(
        viewportBoundaryGetter: () =>
            Rect.fromLTRB(0, 0, 0, MediaQuery.of(context).padding.bottom),
        axis: scrollDirection);    setState(() {
      isLoading=true;
      CircularProgressIndicator();
    });

    Services.getUsers().then((usersFromServer) {
      setState(() {
        isLoading=false;
        // if(connectionState==ConnectionState.waiting)
        // {
        //   loadingView();
        // }
        // else if(connectionState==ConnectionState.active){
        //
        // }
        users = usersFromServer.cast<Article>();
        filteredUsers = users;
      });
    });
  }
  final scrollDirection = Axis.vertical;

  AutoScrollController controller;
  @override
  Widget build(BuildContext context) {
    Color color1 = _colorFromHex("#00ABC5");
    return  new MaterialApp(
      home: Scaffold(
        // resizeToAvoidBottomPadding: false,
        appBar: AppBar(centerTitle: false,
          title: Text("My Tickets Listing"),
          actions: <Widget>[
            FlatButton(
              textColor: Colors.white,
              onPressed: () {
                showDialog(
                  context: context,
                  builder: (context) {
                    return Dialog(
                      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
                      elevation: 16,
                      child: Container(
                        width: 300,
                        height: 350,
                        padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
                        alignment: Alignment.centerLeft,
                        child: Column(
                          children: [
                            Align(
                                alignment: Alignment.center,
                                child: Text("Filter",
                                  style: TextStyle(fontSize: 17.0, fontWeight: FontWeight.bold,
                                      color: Color(0xFF919191)
                                  ),
                                )
                            ),
                            new Container(
                              // child: Row(
                              //   children: <Widget>[
                              child: Column(children: [
                                Container(child: InkWell(
                                  onTap: () {
                                    print("RiM new click");
                                    setState(() {
                                      filteredUsers = users
                                          .where((u) => (u.status.toLowerCase().contains("RIM_NEW".toLowerCase())) ||u.status.toLowerCase().contains("ENG_NEW".toLowerCase())|| u.status.toLowerCase().contains("ENG_REJECT".toLowerCase()) )
                                          .toList();
                                      Navigator.pop(context);
                                    });
                                    print("new"+filteredUsers.length.toString());
                                  }, // Handle your callback
                                  child: Text('NEW'),
                                ),
                                  padding: EdgeInsets.only(top: 10.0),) ,

                                Container(child:InkWell(
                                  onTap: () {
                                    print("eng accept new click");
                                    print("accept"+filteredUsers.length.toString());

                                    setState(() {
                                      filteredUsers = users
                                          .where((u) => (u.status.toLowerCase().contains("ENG_ACCEPT".toLowerCase()) || u.status.toLowerCase().contains("IM_ASGND_ENGNR".toLowerCase()) ||  u.status.toLowerCase().contains("IM_ASGND_PRTNR".toLowerCase()) ))
                                          .toList();
                                      Navigator.pop(context);
                                    });
                                    print("accept"+filteredUsers.length.toString());

                                  }, // Handle your callback
                                  child:  Text('Assign to Agent'),
                                ),  padding: EdgeInsets.only(top: 10.0),),



                                Container(child:InkWell(
                                  onTap: () {
                                    print("eng accept new click");
                                    setState(() {
                                      filteredUsers = users
                                          .where((u) => (u.status.toLowerCase().contains("ENG_PFS".toLowerCase())))
                                          .toList();
                                      Navigator.pop(context);
                                    });
                                    print("assign"+filteredUsers.length.toString());

                                  }, // Handle your callback
                                  child:  Text('Aggent on the way'),
                                ),  padding: EdgeInsets.only(top: 10.0),),




                                Container(child:InkWell(
                                  onTap: () {
                                    print("undefined new click");
                                    setState(() {
                                      filteredUsers = users
                                          .where((u) => (u.status.toLowerCase().contains("ENG_WIP".toLowerCase()) || u.status.toLowerCase().contains("ENG_RAS".toLowerCase()) ||  u.status.toLowerCase().contains("ENG_SPAREQ".toLowerCase()) || u.status.toLowerCase().contains("ENG_WIP".toLowerCase()) || u.status.toLowerCase().contains("ENG_TRANSIM".toLowerCase()) || u.status.toLowerCase().contains("RIM_ACCEPT".toLowerCase()) ||

                                          u.status.toLowerCase().contains("RIM_PA".toLowerCase())||  u.status.toLowerCase().contains("RIM_WIP".toLowerCase()) ||  u.status.toLowerCase().contains("RIM_TRNSIM".toLowerCase()) ||
                                          u.status.toLowerCase().contains("RIM_TOE".toLowerCase()) ||   u.status.toLowerCase().contains("RIM_FAULTPR".toLowerCase())  ||   u.status.toLowerCase().contains("RIM_FAULTPRST".toLowerCase()) ||
                                          u.status.toLowerCase().contains("STR_NEW".toLowerCase())  ||   u.status.toLowerCase().contains("STR_WIP".toLowerCase())  ||    u.status.toLowerCase().contains("STR_SPR_BP".toLowerCase())  ||
                                          u.status.toLowerCase().contains("STR_SPR_DSPTH".toLowerCase()) ||     u.status.toLowerCase().contains("STR_SPR_DSPTH-ER".toLowerCase()) ||
                                          u.status.toLowerCase().contains("STR_FLTY_PFR".toLowerCase())   ||   u.status.toLowerCase().contains("STR_TRNS_IM".toLowerCase())  ||   u.status.toLowerCase().contains("STR_TRNS_TRC_RPR".toLowerCase()) ||
                                          u.status.toLowerCase().contains("STR_TRNS_PRTNR_RPR".toLowerCase()) ||  u.status.toLowerCase().contains("STR_FLTY_NTBP".toLowerCase()) ||
                                          u.status.toLowerCase().contains("IM_NEW".toLowerCase()) || u.status.toLowerCase().contains("IM_WIP".toLowerCase()) ||  u.status.toLowerCase().contains("PART_NEW".toLowerCase()) ||
                                          u.status.toLowerCase().contains("PART_WIP".toLowerCase()) ||   u.status.toLowerCase().contains("PART_ASGN_PRTNR".toLowerCase()) ||  u.status.toLowerCase().contains("PART_ASGN_ENGNR".toLowerCase()) ||
                                          u.status.toLowerCase().contains("TRC_NEW".toLowerCase()) ||   u.status.toLowerCase().contains("TRC_WIP".toLowerCase()) ||   u.status.toLowerCase().contains("TRC_REPAIRED".toLowerCase()) ||  u.status.toLowerCase().contains("TRC_NOT_REPAIRABLE".toLowerCase())
                                      ))
                                          .toList();
                                      Navigator.pop(context);
                                    });
                                    print("Aggent"+filteredUsers.length.toString());

                                  }, // Handle your callback
                                  child:  Text('WIP'),
                                ),  padding: EdgeInsets.only(top: 10.0),),



                                Container(child: InkWell(
                                  onTap: () {
                                    print("undefined new click");
                                    setState(() {
                                      filteredUsers = users
                                          .where((u) => (u.status.toLowerCase().contains("ENG_UOB".toLowerCase()) || u.status.toLowerCase().contains("RIM_UOB".toLowerCase()) ))
                                          .toList();
                                      Navigator.pop(context);
                                    });
                                  }, // Handle your callback
                                  child:  Text('UnderObservation'),
                                ),

                                  padding: EdgeInsets.only(top: 10.0),),

                                Container( child: InkWell(
                                  onTap: () {
                                    print("undefined new click");
                                    setState(() {
                                      filteredUsers = users
                                          .where((u) => (u.status.toLowerCase().contains("ENG_RESOLVED".toLowerCase()) || u.status.toLowerCase().contains("RIM_RESOLVE".toLowerCase()) || u.status.toLowerCase().contains("STR_SPR_DLVR-R".toLowerCase()) || u.status.toLowerCase().contains("STR_STDBY_PICK".toLowerCase()) || u.status.toLowerCase().contains("STR_REPD_NTBP".toLowerCase()) || u.status.toLowerCase().contains("IM_RESOLVED".toLowerCase()) ))
                                          .toList();
                                      Navigator.pop(context);
                                    });
                                    print("resolved "+filteredUsers.length.toString());

                                  }, // Handle your callback
                                  child:  Text('Resolved'),
                                ),  padding: EdgeInsets.only(top: 10.0),),

                                Container( child: InkWell(
                                  onTap: () {
                                    print("undefined new click");
                                    setState(() {
                                      filteredUsers = users
                                          .where((u) => (u.status.toLowerCase().contains("ENG_PDC".toLowerCase()) || u.status.toLowerCase().contains("RIM_PDC".toLowerCase()) || u.status.toLowerCase().contains("IM_PDC".toLowerCase()) || u.status.toLowerCase().contains("PART_PDC".toLowerCase()) ))
                                          .toList();
                                      Navigator.pop(context);
                                    });
                                    print("hold "+filteredUsers.length.toString());

                                  }, // Handle your callback
                                  child:  Text('Hold'),
                                ),  padding: EdgeInsets.only(top: 10.0),),


                                Container( child: InkWell(
                                  onTap: () {
                                    print("undefined new click");
                                    setState(() {
                                      filteredUsers = users
                                          .where((u) => (u.status.toLowerCase().contains("Standby Delivered".toLowerCase()) || u.status.toLowerCase().contains("Standby Delivered and Faulty Picked".toLowerCase()) || u.status.toLowerCase().contains("STR_STDBY_DLVR".toLowerCase()) || u.status.toLowerCase().contains("STR_STDBY-DLVR_FP".toLowerCase()) ))
                                          .toList();
                                      Navigator.pop(context);
                                    });
                                    print("stand "+filteredUsers.length.toString());

                                  }, // Handle your callback
                                  child:  Text('Stand By Given'),
                                ),  padding: EdgeInsets.only(top: 10.0),),







                                Container(child:InkWell(
                                  onTap: () {
                                    print("undefined new click");
                                    Services.getUsers().then((usersFromServer) {
                                      setState(() {
                                        users = usersFromServer.cast<Article>();
                                        filteredUsers = users;
                                        Navigator.pop(context);
                                      });
                                    });
                                    print("all "+filteredUsers.length.toString());

                                  }, // Handle your callback
                                  child:  Text('ALL'
                                  ),
                                ),  padding: EdgeInsets.only(top: 10.0),),
                              ],),
                              //   ],
                              // ),
                            ),
                          ],
                        ),
                      ),
                    );
                  },
                );
              },
              child: Image(image: AssetImage('assets/filter_icon.png'),) ,
              shape: CircleBorder(side: BorderSide(color: Colors.transparent)),
            ),
          ],
          flexibleSpace: Image(
            image: AssetImage('assets/appbar_background.png'),
            fit: BoxFit.cover,
          ),
          backgroundColor: Colors.transparent,),
        body:   WillPopScope(
          onWillPop: _onBackPressed,


          child: Column(
              children: <Widget>[
                // Row(children: [
                //   TextField(
                //
                //     textAlign: TextAlign.left,
                //     decoration: InputDecoration(labelText: "TicketId",
                //
                //         labelStyle:TextStyle(color: Colors.black)),),
                //   TextField(
                //     textAlign: TextAlign.center,
                //     decoration: InputDecoration(labelText: "Asset Type",
                //
                //         labelStyle:TextStyle(color: Colors.black)),),
                //   TextField(
                //     textAlign: TextAlign.right,
                //     decoration: InputDecoration(labelText: "Status",
                //
                //         labelStyle:TextStyle(color: Colors.black)),),
                // ],),
                TextField(
                  decoration: InputDecoration(
                    contentPadding: EdgeInsets.all(15.0),
                    hintText: 'Filter by asset type,ticket id or status',),
                  onChanged: (string) {
                    _debouncer.run(() {
                      setState(() {
                        filteredUsers = users
                            .where((u) => (u.assetType
                            .toLowerCase()
                            .contains(string.toLowerCase()) ||
                            u.id.toLowerCase().contains(string.toLowerCase())))
                            .toList();
                      });
                    });
                  },
                ),
                //
                // Row(  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                //   children:<Widget>[ TextField(
                //
                //     textAlign: TextAlign.left,
                //     decoration: InputDecoration(labelText: "TicketId",
                //
                //         labelStyle:TextStyle(color: Colors.black)),) ] ,),



                Expanded(

                  child: isLoading
                      ? Center(
                    child: CircularProgressIndicator(),
                  )
                      :
                  ListView.builder(
                      scrollDirection: scrollDirection,
                      controller: controller,
                      itemCount: filteredUsers.length,
                      itemBuilder: (BuildContext context, int index) {
                        return Card(
                          child: Padding(
                            padding: EdgeInsets.all(0.0),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children:<Widget> [
                                Container(
                                  color: Color((0xFFF8FCFF)),
                                  padding: EdgeInsets.fromLTRB(5, 5, 5, 200),
                                  height: MediaQuery.of(context).size.height ,
                                  width: MediaQuery.of(context).size.width ,
                                  child: ListView.separated(
                                    //padding: EdgeInsets.fromLTRB(20, 20, 20, 20),
                                    separatorBuilder: (context, index) => Divider(
                                      color: Colors.transparent,
                                    ),
                                    itemCount: filteredUsers.length,
                                    itemBuilder: (context, index) => Padding(
                                      padding: EdgeInsets.all(2.0),
                                      child: InkWell(
                                        onTap: () async {
                                          // print("ticketid"+ticketId[index]);
                                          //   Article aa=new Article();
                                          // var id=aa.id;
                                          // print("id search page"+id);

                                          SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
                                          sharedPreferences.setString("ticket_id", filteredUsers[index].id);
                                          // if(aa.assetType==true)
                                          //   {
                                          //
                                          //    CupertinoColors.extraLightBackgroundGray;
                                          //   }
                                          Navigator.push(context, MaterialPageRoute(builder: (context) => TicketDetails(filteredUsers[index].ticketSerialNo)),);
                                        },
                                        child: Container(
                                          height: 40.0,

                                          padding: EdgeInsets.fromLTRB(5, 5, 5, 5),
                                          child: Column( children: <Widget> [
                                            Row(
                                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                              children: [
                                                Expanded(
                                                    flex: 0,
                                                    child: Padding(
                                                      padding: EdgeInsets.fromLTRB(1, 1, 1, 1),
                                                      child: Text("${this. filteredUsers[index].id}",
                                                        textAlign: TextAlign.left,
                                                        // textAlign: TextAlign.right,
                                                        style: TextStyle(
                                                          fontSize: 12.0,
                                                          // color:  "${this.dogsBreedList[index]}" == "New" ? Colors.orange : "${this.dogsBreedList[index]}" == "On Hold" ?
                                                          // Colors.red :
                                                          // "${this.dogsBreedList[index]}" == "Except" ? Colors.green : ""
                                                          //     "${this.dogsBreedList[index]}" == "Resolve" ? Colors.yellow :
                                                          // "${this.dogsBreedList[index]}" == "Hold On" ? Colors.grey : Colors.grey
                                                          //(1==1)?Colors.blue:Colors.orange
                                                        ),
                                                      ),

                                                      // this doesn't work for top and bottom
                                                    )
                                                ),
                                                Expanded(
                                                  flex: 0,
                                                  child: Text("${this.filteredUsers[index].assetType}",
                                                    textAlign: TextAlign.center,
                                                    style: TextStyle(
                                                      fontSize: 12.0,
                                                      // color:  "${this.dogsBreedList[index]}" == "New" ? Colors.orange : "${this.dogsBreedList[index]}" == "On Hold" ?
                                                      // Colors.red :
                                                      // "${this.dogsBreedList[index]}" == "Except" ? Colors.green : ""
                                                      //     "${this.dogsBreedList[index]}" == "Resolve" ? Colors.yellow :
                                                      // "${this.dogsBreedList[index]}" == "Hold On" ? Colors.grey : Colors.grey
                                                      //(1==1)?Colors.blue:Colors.orange
                                                    ),
                                                  ),
                                                ),
                                                Expanded(
                                                  flex: 0,

                                                  child:
                                                  Text("${
                                                      _status(this.filteredUsers[index].status)}",
                                                    textAlign: TextAlign.right,
                                                    style: TextStyle(fontSize: 12.0),

                                                  ),
                                                ),
                                                SizedBox(
                                                  height: 10,
                                                ),
                                                Container(
                                                  padding: EdgeInsets.fromLTRB(1, 1, 1, 1),
                                                  height: 0.1,
                                                  color: Colors.black,
                                                )
                                              ],
                                            ),
                                          ]),
                                          // ),
                                        ),
                                      ),),),),],),),);
                      }
                  ),),]),),),);
  }


   _status(status) {
    if (status == "RIM_NEW") {
      return 'New'.toString();
    } else if(status == "RIM_ACCEPT") {
      return 'WIP';
    }
    else if(status=="RIM_WIP"){
      return "WIP";
    }
    else if(status=="RIM_UOB"){
      return "Under Observation";
    }
    else if(status=="RIM_PA"){
      return "WIP";
    }
    else if(status=="RIM_PDC"){
      return "HOLD";
    }
    else if(status=="RIM_FAULTPRST"){
      return "WIP";
    }
    else if(status=="RIM_RESOLVED"){
      return "RESOLVED";
    }
    else if(status=="RIM_TRNSIM"){
      return "WIP";
    }
    else if(status=="RIM_TOE"){
      return "WIP";
    }
    else if(status=="RIM_FAULTPR"){
      return "WIP";
    }
   else if (status == "ENG_NEW") {
      return 'New';
    } else if(status == "ENG_ACCEPT") {
      return 'Assign to Aggent';
    }
    else if(status=="ENG_REJECT"){
      return "New";
    }
    else if(status=="ENG_PFS"){
      return "Aggent on the way";
    }

    else if(status=="ENG_RAS"){
      return "WIP";
    }
    else if(status=="ENG_RESOLVED"){
      return "RESOLVED";
    }
    else if(status=="ENG_UOB"){
      return "Under Observation";
    }
    else if(status=="ENG_SPAREQ"){
      return "WIP";
    }
    else if(status=="ENG_WIP"){
      return "WIP";
    }
    else if(status=="ENG_SPR_REQ"){
      return "WIP";
    }
    else if(status=="ENG_PDC"){
      return "Hold";
    }
    else if(status=="ENG_RESO"){
      return "RESOLVED";
    }

    else if(status=="ENG_TRANSIM"){
      return "WIP";
    }
    else if(status=="Standby Delivered"){
      return "Standby Given";
    }
    else if(status=="Standby Delivered and Faulty Picked"){
      return "Standby Given";
    }



    else if(status=="IM_NEW"){
      return "WIP";
    }
    else if(status=="IM_WIP"){
      return "WIP";
    }
    else if(status=="IM_ASGND_PRTNR"){
      return "Assiged to Agent";
    }
    else if(status=="IM_ASGND_ENGNR"){
      return "Assiged to Agent";
    }
    else if(status=="IM_PDC"){
      return "Hold";
    }
    else if(status=="IM_RESOLVED"){
      return "RESOLVED";
    }
    else if (status == "STR_NEW") {
      return 'WIP';
    } else if(status == "STR_WIP") {
      return 'WIP';
    }
    else if(status=="STR_SPR_BP"){
      return "WIP";
    }
    else if(status=="STR_SPR_DSPTH"){
      return "WIP";
    }

    else if(status=="STR_SPR_DLVR-R"){
      return "RESOLVED";
    }
    else if(status=="STR_SPR_DSPTH-ER"){
      return "WIP";
    }
    else if(status=="STR_STDBY_DSPTH"){
      return "WIP";
    }
    else if(status=="STR_STDBY_DLVR"){
      return "Standby Given";
    }
    else if(status=="STR_STDBY_PICK"){
      return "RESOLVED";
    }
    else if(status=="STR_FLTY_PFR"){
      return "WIP";
    }
    else if(status=="STR_STDBY-DLVR_FP"){
      return "Standby Given";
    }

    else if(status=="STR_REP_DLVRD_FP"){
      return "RESOLVED";
    }
    else if(status=="STR_REPD_NTBP"){
      return "RESOLVED";
    }
    else if(status=="STR_TRNS_IM"){
      return "WIP";
    }

    else if(status=="STR_TRNS_TRC_RPR"){
      return "WIP";
    }
    else if(status=="STR_TRNS_PRTNR_RPR"){
      return "WIP";
    }
    else if(status=="STR_FLTY_NTBP"){
      return "WIP";
    }
    else if (status == "PART_NEW") {
      return 'WIP';
    } else if(status == "PART_WIP") {
      return 'WIP';
    }
    else if(status=="PART_ASGN_PRTNR"){
      return "WIP";
    }
    else if(status=="PART_ASGN_ENGNR"){
      return "WIP";
    }

    else if(status=="PART_PDC"){
      return "Hold";
    }
    else if(status=="PART_RESOLVED"){
      return "RESOLVED";
    }


    else if(status=="TRC_NEW"){
      return "WIP";
    }
    else if(status=="TRC_WIP"){
      return "WIP";
    }
    else if(status=="TRC_REPAIRED"){
      return "WIP";
    }
    else if(status=="TRC_NOT_REPAIRABLE"){
      return "WIP";
    }


   }

  Future<bool> _onBackPressed() {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text('Are you sure?'),
            content: Text('Do you want to exit from App!!'),
            actions: <Widget>[

              FlatButton(
                child: Text('YES'),
                onPressed: () {
                  Navigator.of(context).pop(true);
                },
              ),
              FlatButton(
                child: Text('NO'),
                onPressed: () {
                  Navigator.of(context).pop(false);
                },
              ),
            ],
          );
        });
  }





}

Color _colorFromHex(String hexColor) {
  final hexCode = hexColor.replaceAll('#', '');
  return Color(int.parse('FF$hexCode', radix: 16));
}

  Future<bool> isConnected() async {
  var connectivityResult = await (Connectivity().checkConnectivity());
  if (connectivityResult == ConnectivityResult.mobile) {
    return true;
  } else if (connectivityResult == ConnectivityResult.wifi) {
    return true;
  }
  return false;
}


  Color checkStatus(String st){
  Color a;
  if(st=="OK")
    a=Colors.red;}



   Widget loadingView() => Center(
   child: CircularProgressIndicator(
    backgroundColor: Colors.red,
  ),
);

// class _SearchListViewExampleState extends State<SearchListViewExample> {
//   var text=TextEditingController();
//   List<String> dogsBreedList = List<String>();
//   List<String> dogsBreedList1 = List<String>();
//
//   List<String> tempList = List<String>();
//   List<String> ticketId=List<String>();
//   List<String> status=List<String>();
//   List<Article> aa1=List<Article>();
//   bool isLoading = false;
//   var id;
//  List<Article> people=List<Article>();
//   @override
//   void initState(){
//     // TODO: implement initState
//     super.initState();
//
//     _fetchDogsBreed();
//
//     // EasyLoading.show(status:"Loading");
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     Color color1 = _colorFromHex("#00ABC5");
//     // return FlutterEasyLoading(
//      return MaterialApp(
//         title: 'Ticket Listing',
//         theme: ThemeData(
//           backgroundColor: color1,
//         ),
//         debugShowCheckedModeBanner: false,
//
//         home: Scaffold(
//         appBar: AppBar(centerTitle: false,
//           //leading: Icon(Icons.arrow_back),
//           leading: FlatButton(
//
//             onPressed: (){
//
//               Navigator.pop(context);
//             },
//             child:Image(
//               image: AssetImage('assets/appbar_background.png'),
//               fit: BoxFit.cover,
//             ),
//           ),
//
//
//           title: Text("My Tickets Listing"),
//           actions: <Widget>[
//             FlatButton(
//               textColor: Colors.white,
//               onPressed: () {
//
//                 showDialog(
//                   context: context,
//                   builder: (context) {
//                     return Dialog(
//                       shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
//                       elevation: 16,
//
//                       child: Container(
//
//
//                         width: 300,
//                         height: 300,
//                         padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
//                         alignment: Alignment.centerLeft,
//                         child: Column(
//                           children: [
//                             Align(
//                                 alignment: Alignment.center,
//                                 child: Text("Filter",
//                                   style: TextStyle(fontSize: 17.0, fontWeight: FontWeight.bold,
//                                       color: Color(0xFF919191)
//                                   ),
//                                 )
//                             ),
//                             new Container(
//                               child: Row(
//                                 children: <Widget>[
//
//                                   Checkbox(
//                                       value: false,
//                                       activeColor: Colors.green,
//                                       onChanged:(bool newValue){
//                                         // setState(() {
//                                         var checkBoxValue = newValue;
//                                         print(checkBoxValue);
//                                         // });
//                                       }
//                                   ),
//
//                                  InkWell(
//                                      child: Text("RIM_NEW"),
//                                   onTap: () {
//                                     print("hello");
//                                     _filterDogList("ALL UPS HARDWARE");
//
//                                     Navigator.pop(context);
//                                     // handle your onTap here
//                                     }
//                               ),
//                                 ],
//                               ),
//                             ),
//                           ],
//                         ),
//                       ),
//                     );
//                   },
//                 );
//               },
//               child: Icon(Icons.filter),
//               shape: CircleBorder(side: BorderSide(color: Colors.transparent)),
//             ),
//           ],
//           flexibleSpace: Image(
//             image: AssetImage('assets/appbar_background.png'),
//             fit: BoxFit.cover,
//           ),
//           backgroundColor: Colors.transparent,),
//       body:
//       Padding(
//         padding: EdgeInsets.all(0.0),
//         child: Container(
//           child: Column(
//             crossAxisAlignment: CrossAxisAlignment.start,
//             children: <Widget>[
//               _searchBar(),
//               Container(
//                 child: Padding(
//                   padding: EdgeInsets.all(15.0),
//                   child: Row(
//                     children: [
//                       Expanded(
//                         flex: 1,
//                         child: Text("Ticket Id"),
//                       ),
//                       Expanded(
//                         flex: 1,
//                         child: Text("Asset Type"),
//                       ),
//
//                       Expanded(
//                         flex: 1,
//                         child: Text("Status"),
//                       ),
//                     ],
//                   ),
//                 ),
//               ),
//               Container(
//                 height: 0.1,
//                 color: Colors.black,
//               ),
//               Expanded(
//                 flex: 1,
//                 // child: _mainData(context),
//                 child: Column(
//                   children:<Widget> [
//                     Container(
//                       color: Color((0xFFF8FCFF)),
//                       padding: EdgeInsets.all(0),
//                       height: MediaQuery.of(context).size.height ,
//                       width: MediaQuery.of(context).size.width ,
//                       child: ListView.separated(
//                         //padding: EdgeInsets.fromLTRB(20, 20, 20, 20),
//
//                         separatorBuilder: (context, index) => Divider(
//                           color: Colors.transparent,
//                         ),
//                         itemCount: dogsBreedList.length,
//                         itemBuilder: (context, index) => Padding(
//                           padding: EdgeInsets.all(2.0),
//                           child: InkWell(
//                             onTap: () async {
//                               print("ticketid"+ticketId[index]);
//
//
//
//
//                               //   Article aa=new Article();
//                               // var id=aa.id;
//                               // print("id search page"+id);
//
//                               SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
//                               sharedPreferences.setString("ticket_id", ticketId[index]);
//                               // if(aa.assetType==true)
//                               //   {
//                               //
//                               //    CupertinoColors.extraLightBackgroundGray;
//                               //   }
//                               Navigator.push(context, MaterialPageRoute(builder: (context) => TicketDetails(ticketId[index])),);
//
//                             },
//                             child: Container(
//                               height: 50.0,
//
//                               padding: EdgeInsets.fromLTRB(5, 5, 5, 5),
//                               child: Column( children: <Widget> [
//                                 Row(
//                                   mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                                   children: [
//                                    Expanded(
//                                         flex: 0,
//
//                                         child: Padding(
//                                           padding: EdgeInsets.fromLTRB(1, 1, 1, 1),
//
//                                           child: Text("${this.ticketId[index]}",
//                                             textAlign: TextAlign.left,
//                                             // textAlign: TextAlign.right,
//                                             style: TextStyle(
//                                             fontSize: 12.0,
//                                                 // color:  "${this.dogsBreedList[index]}" == "New" ? Colors.orange : "${this.dogsBreedList[index]}" == "On Hold" ?
//                                                 // Colors.red :
//                                                 // "${this.dogsBreedList[index]}" == "Except" ? Colors.green : ""
//                                                 //     "${this.dogsBreedList[index]}" == "Resolve" ? Colors.yellow :
//                                                 // "${this.dogsBreedList[index]}" == "Hold On" ? Colors.grey : Colors.grey
//                                               //(1==1)?Colors.blue:Colors.orange
//                                             ),
//                                           ),
//
//                                           // this doesn't work for top and bottom
//                                         )
//
//
//
//                                     ),
//                                     Expanded(
//                                       flex: 0,
//                                       child: Text("${this.dogsBreedList[index]}",
//                                         textAlign: TextAlign.center,
//
//                                         style: TextStyle(
//
//                                           fontSize: 12.0,
//
//
//                                           // color:  "${this.dogsBreedList[index]}" == "New" ? Colors.orange : "${this.dogsBreedList[index]}" == "On Hold" ?
//                                           // Colors.red :
//                                           // "${this.dogsBreedList[index]}" == "Except" ? Colors.green : ""
//                                           //     "${this.dogsBreedList[index]}" == "Resolve" ? Colors.yellow :
//                                           // "${this.dogsBreedList[index]}" == "Hold On" ? Colors.grey : Colors.grey
//                                           //(1==1)?Colors.blue:Colors.orange
//                                         ),
//
//                                       ),
//
//                                     ),
//                                     Expanded(
//                                       flex: 0,
//                                       child: Text("${this.dogsBreedList1[index]}",
//                                         textAlign: TextAlign.right,
//                                         style: TextStyle(fontSize: 12.0),
//                                     ),
//
//                                 ),
//                                 SizedBox(
//                                   height: 10,
//                                 ),
//                                 Container(
//                                   padding: EdgeInsets.fromLTRB(1, 1, 1, 1),
//                                   height: 0.1,
//                                   color: Colors.black,
//
//                                 )
//
//
//                               ],
//
//                               ),
//                             ]),
//                           ),
//                         ),
//                       ),
//
//                     ),
//
//
//
//
//                     // Expanded(
//                     //
//                     //     child:
//                     //
//                     // ),
//                     )],
//
//                 ),
//               )
//             ],
//           ),
//         )
//     )));
//   }
//
//   Widget _searchBar(){
//     return Container(
//       padding: EdgeInsets.only(bottom: 16.0),
//       child: TextField(
//         decoration: InputDecoration(
//           hintText: "Search Tickets Here...",
//           prefixIcon: Icon(Icons.search),
//
//         ),
//         controller: text,
//         onChanged: (text){
//           if(text!=null){
//             showSearch(
//               context: context,
//               delegate: SearchPage<Article>(
//                 onQueryUpdate: (s) => print(s),
//                 items: people,
//                 searchLabel: 'Search people',
//                 suggestion: Center(
//                   child: Text('Filter people by name, surname or age'),
//                 ),
//                 failure: Center(
//                   child: Text('No person found :('),
//                 ),
//                 filter: (person) => [
//                   person.assetType,
//                   person.assetId,
//                   person.id,
//                   person.status
//                 ],
//                 builder: (person) => ListTile(
//                   title: Text(person.assetId),
//                   subtitle: Text(person.assetType),
//                   trailing: Text(person.status)
//                 ),
//               ),
//             );
//             // _filterDogList(text);
//           }
//           // else
//           //   {
//           //     _fetchDogsBreed();
//           //   }
//           },
//       ),
//     );
//   }
//
//   // Widget _mainData(BuildContext context){
//   //   return Center(
//   //     child: isLoading?
//   //     CircularProgressIndicator():
//   //     ListView.builder(
//   //         itemCount: dogsBreedList.length,
//   //         itemBuilder: (context,index){
//   //           return ListTile(
//   //             title: Text(dogsBreedList[index],),
//   //                 onTap: () async {
//   //               print("ticketid"+ticketId[index]);
//   //
//   //              //   Article aa=new Article();
//   //              // var id=aa.id;
//   //              // print("id search page"+id);
//   //
//   //               SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
//   //               sharedPreferences.setString("ticket_id", ticketId[index]);
//   //               // if(aa.assetType==true)
//   //               //   {
//   //               //
//   //               //    CupertinoColors.extraLightBackgroundGray;
//   //               //   }
//   //               Navigator.push(context, MaterialPageRoute(builder: (context) => TicketDetails(ticketId[index])),);
//   //
//   //                 },
//   //           );
//   //         }),
//   //   );
//   // }
//
//   _filterDogList(String text) {
//     if(text.isNotEmpty){
//       setState(() {
//         dogsBreedList = tempList;
//         final List<String> filteredBreeds = List<String>();
//         tempList.map((assetType){
//           if(assetType.contains(text.toString().toUpperCase())){
//             filteredBreeds.add(assetType);
//           }
//         }).toList();
//         setState(() {
//           dogsBreedList.clear();
//           dogsBreedList.addAll(filteredBreeds);
//         });
//       });
//     }
//     else{
//       final List<String> filteredBreeds = List<String>();
//       tempList.map((breed){
//         if(breed.contains(text.toString().toUpperCase())){
//           filteredBreeds.add(breed);
//         }
//       }).toList();
//       setState(() {
//         dogsBreedList.clear();
//         dogsBreedList.addAll(filteredBreeds);
//       });
//     }
//   }
//
//
//
//
//   _fetchDogsBreed() async{
//     SharedPreferences sharedPreferences    = await SharedPreferences.getInstance();
//     String token=  sharedPreferences.getString("token");
//     setState(() {
//       isLoading = true;
//     });
//     tempList = List<String>();
//     String link ="http://10.11.4.59:8080/api/cust/user/geTickets";
//     var request =  http.MultipartRequest('POST',Uri.parse(link));
//     print("request"+request.toString());
//     request.fields['customerID']=sharedPreferences.getString("userId");
//     var response=await request.send();
//
//     if(response.statusCode == 200){
//       var result1 = await http.Response.fromStream(response);
//       final jsonResponse = jsonDecode(result1.body);
//       print("==jsonresponse list "+jsonResponse.toString());
//
//       var result=jsonResponse['result'] as List;
//       print("==result "+result.toString());
//       for (var i = 0; i < result.length; i++){
//         Article aa=Article.fromJson(result[i]);
//         var assetType=aa.assetType.toString();
//         var id=aa.id.toString();
//         print('asssetType'+assetType);
//         tempList.add(assetType.toString().toUpperCase());
//         ticketId.add(id);
//         status.add(aa.status.toString());
//         List <Article> people=  List<Article>();
//         var uu=new Article();
//         uu.assetType=aa.assetType;
//         uu.assetId=aa.assetId;
//         uu.id=aa.id;
//         uu.status=aa.status;
//         people.add( uu);
//
//       }
//
//     }
//     else{
//       throw Exception("Failed to load Dogs Breeds.");
//     }
//     setState(() {
//       dogsBreedList = tempList;
//       dogsBreedList1=status;
//       isLoading = false;
//     });
//   }
//
//
// }